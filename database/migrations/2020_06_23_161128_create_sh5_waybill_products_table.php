<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSh5WaybillProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sh5_waybill_products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('waybill_id');
            $table->integer('product_id');
            $table->string('name')->nullable();
            $table->string('guid', 40)->index()->nullable();
            $table->integer('price');
            $table->integer('amount');
            $table->float('quantity');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sh5_waybill_products');
    }
}
