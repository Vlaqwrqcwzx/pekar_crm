<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameColumnShiftProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shift_products', function (Blueprint $table) {
            $table->renameColumn('reject', 'reject1');
            $table->renameColumn('loss', 'reject2');
        });

        Schema::table('shift_product_histories', function (Blueprint $table) {
            $table->renameColumn('reject', 'reject1');
            $table->renameColumn('loss', 'reject2');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
