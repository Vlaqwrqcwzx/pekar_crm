<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Sh5WaybillProductsRid extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sh5_waybill_products', function (Blueprint $table) {
            $table->integer('rid')->index()->nullable();
            $table->integer('base_unit')->index()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sh5_waybill_products', function (Blueprint $table) {
            // 
        });
    }
}
