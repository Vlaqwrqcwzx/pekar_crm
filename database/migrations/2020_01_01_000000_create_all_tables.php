<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAllTables extends Migration
{
    public function up()
    {
        Schema::create('devices', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('uuid', 40)->unique()->index();
            $table->integer('export_id')->unique()->index();
            $table->integer('import_id')->index();
            $table->integer('shop_id')->index();
            $table->integer('setting_group_id')->index()->nullable();
            $table->string('name', 100);
            $table->string('model_code', 100);
            $table->string('model_name', 100);
        });

        Schema::create('inventories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('shift_id')->index();
            $table->integer('shop_id')->index();
            $table->integer('user_id')->index();
            $table->string('type', 50);
            $table->string('code', 100);
            $table->timestamps();
        });

        Schema::create('inventory_products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('inventory_id')->index();
            $table->integer('product_id')->index();
            $table->integer('shop_id')->index();
            $table->decimal('before', 10, 3);
            $table->decimal('quantity', 10, 3);
            $table->timestamps();
        });

        Schema::create('movements', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('shift_id')->index();
            $table->integer('from_user_id')->index();
            $table->integer('to_user_id')->index();
            $table->integer('from_shop_id')->index();
            $table->integer('to_shop_id')->index();
            $table->string('code', 100);
            $table->timestamps();
        });

        Schema::create('movement_products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('movement_id')->index();
            $table->integer('product_id')->index();
            $table->integer('from_shop_id')->index();
            $table->integer('to_shop_id')->index();
            $table->decimal('complete', 10, 3);
            $table->decimal('quantity', 10, 3);
            $table->timestamps();
        });

        Schema::create('password_resets', function (Blueprint $table) {
            $table->string('email')->index();
            $table->string('token');
            $table->timestamp('created_at')->nullable();
        });

        Schema::create('plans', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('shop_id')->index();
            $table->string('name', 50);
            $table->string('type', 50);
            $table->integer('Mon');
            $table->integer('Tue');
            $table->integer('Wed');
            $table->integer('Thu');
            $table->integer('Fri');
            $table->integer('Sat');
            $table->integer('Sun');
            $table->timestamps();
        });

        Schema::create('plan_products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('plan_id')->index();
            $table->integer('product_id')->index();
            $table->integer('hour');
            $table->decimal('quantity', 10, 3);
            $table->timestamps();
        });

        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('parent_id')->nullable();
            $table->integer('import_id');
            $table->integer('device_category_id')->nullable();
            $table->integer('category_id')->nullable();
            $table->string('uuid', 40)->unique()->index();
            $table->string('name', 200);
            $table->string('type', 100);
            $table->float('quantity');
            $table->integer('price')->nullable();
            $table->integer('time')->nullable();
            $table->integer('top')->nullable();
            $table->integer('new')->nullable();
            $table->dateTime('date')->nullable();
            $table->integer('hide')->nullable();
            $table->integer('hide_shop');
            $table->integer('rid')->index()->nullable();
            $table->integer('base_unit')->index()->nullable();
            $table->string('sh5_guid', 40)->unique()->index()->nullable();
            $table->timestamps();
        });

        Schema::create('product_device_prices', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('product_id');
            $table->integer('device_id');
            $table->integer('price');
            $table->unique(['product_id', 'device_id']);
            $table->timestamps();
        });

        Schema::create('product_shop_orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('device_id');
            $table->integer('shop_id');
            $table->integer('product_id');
            $table->integer('position');
            $table->integer('price');
            $table->decimal('quantity', 10, 3);
            $table->integer('amount');
            $table->integer('discount')->nullable();
            $table->string('payment', 40);
            $table->dateTime('date');
        });

        Schema::create('product_shop_prices', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('product_id')->index();
            $table->integer('shop_id')->index();
            $table->integer('price');
            $table->date('date');
        });

        Schema::create('product_shop_quantities', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('product_id')->index();
            $table->integer('shop_id')->index();
            $table->date('date')->index();
            $table->decimal('quantity');
            $table->timestamps();
        });

        Schema::create('product_shop_quantity_histories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('product_id')->index();
            $table->integer('shop_id')->index();
            $table->integer('user_id');
            $table->float('quantity');
            $table->float('before')->nullable();
            $table->float('after')->nullable();
            $table->string('entity');
            $table->integer('entity_id');
            $table->timestamps();
        });

        Schema::create('rejects', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('shift_id');
            $table->integer('shop_id')->index();
            $table->integer('user_id')->index();
            $table->string('type', 50);
            $table->string('place', 10);
            $table->string('code', 100);
            $table->timestamps();
        });

        Schema::create('reject_products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('reject_id')->index();
            $table->integer('product_id')->index();
            $table->integer('shift_product_id');
            $table->integer('shop_id')->index();
            $table->string('place', 10);
            $table->decimal('quantity', 10, 3);
            $table->timestamps();
        });

        Schema::create('shifts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('shop_id')->index();
            $table->string('type', 50);
            $table->integer('user_id_start')->index();
            $table->integer('user_id_end')->index();
            $table->dateTime('date_start')->nullable();
            $table->dateTime('date_end')->nullable();
        });

        Schema::create('shift_products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('shift_id')->index();
            $table->integer('plan_id')->index();
            $table->integer('product_id')->index();
            $table->integer('shop_id')->index();
            $table->integer('hour');
            $table->decimal('quantity', 10, 3);
            $table->decimal('complete', 10, 3);
            $table->decimal('accept', 10, 3);
            $table->decimal('reject', 10, 3);
            $table->decimal('loss', 10, 3);
            $table->decimal('wrong', 10, 3);
            $table->integer('status');
            $table->string('code', 20);
            $table->timestamps();
        });

        Schema::create('shift_product_histories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('shift_product_id')->index();
            $table->integer('user_id')->index();
            $table->integer('status');
            $table->decimal('complete', 10, 3);
            $table->decimal('accept', 10, 3);
            $table->decimal('reject', 10, 3);
            $table->decimal('loss', 10, 3);
            $table->decimal('wrong', 10, 3);
            $table->timestamps();
        });

        Schema::create('shops', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('franchise_id')->index();
            $table->integer('setting_id')->index()->nullable();
            $table->string('name', 100);
            $table->string('corr_guid_cashless', 40)->index()->nullable();
            $table->string('corr_guid_cash', 40)->index()->nullable();
            $table->string('depart_guid', 40)->index()->nullable();
            $table->string('marriage_corr_guid', 40)->index()->nullable();
            $table->string('brokerage_corr_guid', 40)->index()->nullable();
            $table->string('write_off_corr_guid', 40)->index()->nullable();
            $table->string('corr_guid_1', 40)->index()->nullable();
            $table->string('corr_guid_2', 40)->index()->nullable();
            $table->string('corr_guid_3', 40)->index()->nullable();
            $table->string('corr_guid_4', 40)->index()->nullable();
            $table->string('corr_guid_5', 40)->index()->nullable();

            $table->timestamps();
        });

        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('group_id')->index();
            $table->string('name');
            $table->string('email')->unique();
            $table->string('code', 100);
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });

        Schema::create('user_groups', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 100);
            $table->text('menu')->nullable();
            $table->string('type', 50)->nullable();
            $table->timestamps();
        });

        Schema::create('user_shifts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('shift_id')->index();
            $table->integer('shop_id')->index();
            $table->integer('user_id')->index();
            $table->dateTime('date_start')->nullable();
            $table->dateTime('date_end')->nullable();
        });

        Schema::create('user_shop', function (Blueprint $table) {
            $table->integer('user_id')->index();
            $table->integer('shop_id')->index();
            $table->string('type', 50)->nullable();
            $table->integer('time')->nullable();
            $table->unique(['user_id', 'shop_id', 'type']);
        });

        Schema::create('sh5_departs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 100);
            $table->integer('rid')->index()->nullable();
            $table->string('guid', 40)->unique()->index()->nullable();
            $table->timestamps();
        });

        Schema::create('sh5_corrs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 100);
            $table->integer('rid')->index()->nullable();
            $table->string('guid', 40)->unique()->index()->nullable();
            $table->timestamps();
        });

        Schema::create('sh5_waybills', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('type', 100)->index()->nullable();
            $table->string('number', 100)->index()->nullable();
            $table->string('guid', 40)->unique()->index()->nullable();
            $table->integer('reject_id')->index();
            $table->integer('shop_id')->index();
            $table->string('payment', 40)->index()->nullable();
            $table->integer('count');
            $table->string('depart_guid', 40)->index()->nullable();
            $table->string('corr_guid', 40)->index()->nullable();
            $table->integer('shift_id')->index();
            $table->dateTime('date_start');
            $table->dateTime('date_end');
            $table->timestamps();
        });

        Schema::create('sh5_settings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('import_id')->index()->nullable();
            $table->string('name');
            $table->string('ip');
            $table->string('port')->unique();
            $table->string('username');
            $table->string('password');
            $table->timestamps();
        });

        Schema::create('sh5_setting_groups', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('setting_id')->index()->nullable();
            $table->string('name', 100);
            $table->integer('rid')->index()->nullable();
            $table->string('guid', 40)->unique()->index()->nullable();
            $table->timestamps();
        });

        Schema::create('franchises', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 100);
            $table->timestamps();
        });

        Schema::create('imports', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('setting_group_id')->index()->nullable();
            $table->integer('setting_id')->index()->nullable();
            $table->string('name', 100);
            $table->string('token');
            $table->string('type');
            $table->integer('status');
            $table->timestamps();
        });

        Schema::create('device_categories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('import_id');
            $table->integer('device_category_id')->nullable();
            $table->integer('category_id')->nullable();
            $table->string('name', 200);
            $table->timestamps();
        });

        Schema::create('categories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 200);
            $table->string('color', 10)->nullable();
            $table->integer('sort');
            $table->timestamps();
        });

        Schema::create('settings', function (Blueprint $table) {
            $table->string('name');
            $table->text('value')->nullable();
        });

        Schema::create('report_settings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id')->index();
            $table->string('report', 50);
            $table->text('settings');
            $table->timestamps();
        });
    }
}
