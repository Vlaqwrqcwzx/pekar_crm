<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Sh5Changes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sh5_corrs', function (Blueprint $table) {
            $table->integer('setting_id')->index()->nullable();
        });

        Schema::table('sh5_departs', function (Blueprint $table) {
            $table->integer('setting_id')->index()->nullable();
        });

        Schema::table('devices', function (Blueprint $table) {
            $table->dropColumn('setting_group_id');
        });

        Schema::table('sh5_settings', function (Blueprint $table) {
            $table->dropColumn('import_id');
        });

        Schema::table('shops', function (Blueprint $table) {
            $table->dropColumn(['setting_id', 'corr_guid_cashless',
            'corr_guid_cash', 'depart_guid', 'marriage_corr_guid', 'brokerage_corr_guid', 'write_off_corr_guid',
            'corr_guid_1', 'corr_guid_2', 'corr_guid_3', 'corr_guid_4', 'corr_guid_5' ]);
            $table->integer('import_id');
            $table->integer('depart_rid_1')->index()->nullable();
            $table->integer('depart_rid_2')->index()->nullable();
            $table->integer('cashless_corr_rid')->index()->nullable();
            $table->integer('cash_corr_rid')->index()->nullable();
            $table->integer('marriage_corr_rid')->index()->nullable();
            $table->integer('brokerage_corr_rid')->index()->nullable();
            $table->integer('write_off_corr_rid')->index()->nullable();
        });

        Schema::table('sh5_waybills', function (Blueprint $table) {
            $table->dropColumn(['depart_guid','corr_guid' ]);
            $table->string('proc_name');
            $table->integer('shift_products_id')->index()->nullable();
            $table->integer('movement_id')->index()->nullable();
            $table->integer('setting_id')->index()->nullable();
            $table->integer('depart_rid')->index()->nullable();
            $table->integer('corr_rid')->index()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
