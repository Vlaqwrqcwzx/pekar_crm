<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPlaceToMovement extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('movements', function (Blueprint $table) {
            $table->string('place', 10)->after('to_shop_id');
        });

        Schema::table('movement_products', function (Blueprint $table) {
            $table->integer('shift_product_id')->index()->nullable()->after('product_id');
        });

        Schema::table('shift_products', function (Blueprint $table) {
            $table->decimal('move', 10, 3)->after('wrong');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('movement', function (Blueprint $table) {
            //
        });
    }
}
