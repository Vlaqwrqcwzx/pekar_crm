<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductShopPrice extends Model
{
    public $timestamps = false;

    protected $guarded = [];
}
