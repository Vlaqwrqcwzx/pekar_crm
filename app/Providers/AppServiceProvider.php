<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Blade;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        Blade::directive('format', function ($amount) {
            return "<?php echo sprintf('%s ₽', number_format($amount / 100, 0, 0, 0)); ?>";
        });

        Blade::directive('moneyFormat', function ($amount) {
            return "<?php echo sprintf('%s ₽', number_format($amount, 2, '.', ' ')); ?>";
        });
    }
}
