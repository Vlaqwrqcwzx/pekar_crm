<?php

namespace App\Providers;

use App\Plan;
use App\Shop;
use App\Policies\PlanPolicy;
use App\Policies\ShopPolicy;
use App\Policies\ShiftPolicy;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Plan::class => PlanPolicy::class,
        Shop::class => ShopPolicy::class,
        Shift::class => ShiftPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}
