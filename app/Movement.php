<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Movement extends Model
{
    protected $guarded = [];

    public function getName()
    {
        return sprintf('%s -> %s', $this->fromShop ? $this->fromShop->name : '', $this->toShop ? $this->toShop->name : '-');
    }

    public function movementProducts()
    {
        return $this->hasMany('App\MovementProduct');
    }

    public function fromUser()
    {
        return $this->belongsTo('App\User', 'from_user_id');
    }

    public function toUser()
    {
        return $this->belongsTo('App\User', 'to_user_id');
    }

    public function fromShop()
    {
        return $this->belongsTo('App\Shop', 'from_shop_id');
    }

    public function toShop()
    {
        return $this->belongsTo('App\Shop', 'to_shop_id');
    }
}
