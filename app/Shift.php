<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Shift extends Model
{
    public $timestamps = false;
    public $dates = ['date_start', 'date_end'];

    public const STATUSES = [
        0 => 'Начало',
        1 => 'В работе',
        2 => 'Выпущен',
        3 => 'Откланен',
        4 => 'Принят'
    ];

    public const TYPES = [
        'baker'        => 'Пекарь',
        'confectioner' => 'Кондитер',
        'cash'         => 'Кассир'
    ];

    protected $guarded = [];

    public function getName()
    {
        return sprintf('Смена #%s - %s (%s - %s)', $this->id, $this->getType(), $this->date_start, $this->date_end);
    }

    public function getType()
    {
        return isset(self::TYPES[$this->type]) ? self::TYPES[$this->type] : '';
    }

    /**
     * Получить открытые смены
     */
    public function scopeOpen($query, $shop_id = false, $type = false)
    {
        $query = $query->whereNull('date_end')
            ->orderBy('date_start', 'desc');;

        if ($shop_id) {
            $query->where('shop_id', $shop_id);
        }

        if ($type) {
            $query->where('type', $type);
        }

        return $query;
    }

    public function shiftProducts()
    {
        return $this->hasMany('App\ShiftProduct');
    }

    public function rejects()
    {
        return $this->hasMany('App\Reject');
    }

    public function shop()
    {
        return $this->belongsTo('App\Shop');
    }

    public function userShifts()
    {
        return $this->hasMany('App\UserShift');
    }

    public function inventory()
    {
        return $this->hasMany('App\Inventory');
    }

    /**
     * Группа кассы?
     */
    public function getIsCashAttribute()
    {
        return in_array($this->type, ['cash']);
    }

    /**
     * Группа пекаря?
     */
    public function getIsBakerAttribute()
    {
        return in_array($this->type, ['baker']);
    }

    public function getIsConfectionerAttribute()
    {
        return in_array($this->type, ['confectioner']);
    }

    public function getDateEnd()
    {
        return $this->date_end ? $this->date_end : Carbon::tomorrow();
    }
}
