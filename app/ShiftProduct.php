<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShiftProduct extends Model
{
    protected $guarded = [];

    public function product()
    {
        return $this->belongsTo('App\Product');
    }

    public function plan()
    {
        return $this->belongsTo('App\Plan');
    }

    public function shift()
    {
        return $this->belongsTo('App\Shift');
    }

    public function movementProduct()
    {
        return $this->hasOne('App\MovementProduct');
    }

    public function histories()
    {
        return $this->hasMany('App\ShiftProductHistory');
    }

    public function isMoveAccepted()
    {
        return $this->movementProduct ? $this->movementProduct->isAccepted : false;
    }

    /*
     * Кол-ва товара которые еще висят на приемку
     * Берем сколько товаров выпущено и минусуем от него брак, бракераж, перемещение и принятие
     *
     */

    public function toAccept()
    {
        return ($this->getIsCompletedAttribute() || $this->getIsAcceptedAttribute())
            ? round($this->complete - $this->accept - $this->reject1 - $this->reject2 - $this->wrong - $this->move, 3)
            : 0;
    }

    /*
     * Принят кассиром?
     */
    public function getIsAcceptedAttribute()
    {
        return in_array($this->status, [3]);
    }

    /*
     * Если все принято
     */
    public function getIsAllAcceptedAttribute()
    {
        return $this->toAccept() == $this->accept;
    }

    /*
     * Выпущен кассиром?
     */
    public function getIsCompletedAttribute()
    {
        return in_array($this->status, [2]);
    }

    /*
     * Когда перемещение из выпуска приняли на другом магазине
     */
    public function getIsMovementAcceptedAttribute()
    {
        return $this->movementProduct ? $this->movementProduct->isAccepted : false;
    }
}
