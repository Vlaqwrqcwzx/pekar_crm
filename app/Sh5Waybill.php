<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sh5Waybill extends Model
{
    protected $fillable = [
        'type',
        'number',
        'guid',
        'shop_id',
        'payment',
        'count',
        'worker',
        'depart_rid',
        'corr_rid',
        'shift_id',
        'setting_id',
        'reject_id',
        'movement_id',
        'to_shop_id',
        'inventory_id',
        'shift_products_id',
        'date_start',
        'date_end',
        'proc_name'
    ];

    public function waybillProducts()
    {
        return $this->hasMany('App\Sh5WaybillProduct', 'waybill_id');
    }

}
