<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MovementProduct extends Model
{
    protected $guarded = [];

    public function product()
    {
        return $this->belongsTo('App\Product');
    }

    public function movement()
    {
        return $this->belongsTo('App\Movement');
    }

    public function getIsAcceptedAttribute()
    {
        return $this->created_at != $this->updated_at;
    }

    public function getIsKitchenAttribute()
    {
        return $this->shift_product_id > 0;
    }

    public function getIsCashAttribute()
    {
        return $this->shift_product_id === 0;
    }
}
