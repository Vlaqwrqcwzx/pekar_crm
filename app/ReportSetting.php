<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReportSetting extends Model
{
    protected $guarded = [];
    protected $casts = [
        'settings' => 'array'
    ];
}
