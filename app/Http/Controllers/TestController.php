<?php

namespace App\Http\Controllers;

use App\Orders;
use App\Http\Requests\RequestTest;

class TestController extends Controller
{
    public function index()
    {
        return view('test');
    }

    public function data()
    {
        return response()->json([
            'name'  => 'Abigail!!!!',
            'state' => 'CA',
            'orders' => Orders::all()->toArray()
        ]);
    }

    public function insert(RequestTest $request)
    {
        Orders::create($request->all());
    }
}
