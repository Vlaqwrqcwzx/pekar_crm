<?php

namespace App\Http\Controllers\System;

use App\Http\Controllers\Controller;
use App\Inventory;

class InventoryController extends Controller
{
    public function index(Inventory $inventory)
    {
        $inventory->load([
            'shop',
            'user',
            'inventoryProducts'
        ]);

        return view('system.inventory.index', compact('inventory'));
    }

    public function update()
    {


        return redirect()->back()->with('success', 'Смена изменена');
    }
}
