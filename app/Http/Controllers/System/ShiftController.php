<?php

namespace App\Http\Controllers\System;

use App\Http\Controllers\ReportController;
use App\Shop;
use App\UserShift;

class ShiftController extends ReportController
{
    public function index()
    {
        if (request()->has('user_shift_id')) {
            UserShift::destroy(request()->get('user_shift_id'));

            return redirect()->back()->with('success', 'Смена пользователя удалена');
        }

        $shops = Shop::all();

        $this->data = array_merge($this->data, [
            'products' => [],
            'shops'    => $shops
        ]);

        $this->filters['filter_date'] = request()->get('filter_date', date('Y-m-d'));

        if ($this->filters['filter_shop']) {
            $shop = $shops->first(function ($item) {
                return $item->id == $this->filters['filter_shop'];
            });
        }

        if (empty($shop)) {
            $shop = $shops->first();
        }

        if ($shop) {
            $this->filters['filter_shop'] = $shop->id;

            $shop->load(['shifts' => function ($query) {
                $query->whereDate('date_start', '<=', $this->filters['filter_date'])
                    ->where(function ($query) {
                        $query->where('date_end', '>=', $this->filters['filter_date'])->orWhereNull('date_end');
                    })->with('userShifts.user.group');
            }]);

            $this->data['shifts'] = $shop->shifts;
        }

        return view('system.shift.index', $this->data, $this->filters);
    }

    public function update()
    {
        if (request()->has('date_start')) {
            foreach (request()->date_start as $id => $date) {
                UserShift::find($id)->update([
                    'date_start' => $date,
                    'date_end'   => request()->date_end[$id]
                ]);
            }
        }

        return redirect()->back()->with('success', 'Смена изменена');
    }
}
