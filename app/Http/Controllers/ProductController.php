<?php

namespace App\Http\Controllers;

use App\Shop;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Category;
use App\Product;
use App\Import;

use Illuminate\Support\Facades\Validator;

use App\Http\Controllers\StoreHouse5\InsGoodController;

class ProductController extends Controller
{
    public function index()
    {
        return view('product.index', [
            // 'products' => Product::all(),
            'imports' => Import::all()
        ]);
    }

    public function import(Import $import) {

        $products = Product::where('import_id', '=', $import->id)->get();
        return view('product.import', [
            'products' => $products,
            'import_id' => $import->id,
        ]);
    }

    public function edit(Product $product)
    {
        return view('product.form', [
            'product'  => $product,
            'import_id' => $product->import_id,
            'products' => Product::where('id', '!=', $product->id)->get(),
            'action'   => route('product.update', ['id' => $product->id])
        ]);
    }

    public function update(Request $request, Product $product)
    {
        Validator::make($request->all(), [
            'name' => [
                'nullable',
                'max:255'
            ]
        ])->validate();

        $product->update([
            'name'      => $request->post('name'),
            'time'      => $request->post('time'),
            'parent_id' => $request->post('parent_id')
        ]);

        return redirect()
            ->route('product.import', $request->post('import_id'))
            ->with('success', 'Товар успешно обновлен');
    }

    public function json()
    {
        $json = [
            'products'   => [],
            'categories' => []
        ];

        $categoryProducts = Product::with('category');

        if (request()->has('shop_id')) {
            $categoryProducts->with(['shopPrice' => function($query) {
                $query->where('shop_id', request()->shop_id)->whereDate('date', date('Y-m-d'));
            }]);

            $categoryProducts->with(['shopQuantity' => function($query) {
                $query->where('shop_id', request()->shop_id)
                    ->whereDate('date', Carbon::today()->toDateString());

                // Были какие то движухи по товары
                if (request()->has('live')) {
                    $query->whereColumn('created_at', '!=', 'updated_at');
                }
            }]);

            $shop = Shop::find(request()->shop_id);

            if ($shop) {
                $categoryProducts->where('import_id', $shop->import_id);
            }
        }

        $categoryProducts = $categoryProducts
            ->orderBy('name')
            ->get()
            ->where('category.kitchen', 1)
            ->groupBy('category.id')
            ->sortBy(function ($item) {
                return $item->first()->getSort();
            });

        foreach ($categoryProducts as $products) {
            foreach ($products as $product) {
                if (request()->has('live') && !$product->shopQuantity) {
                    continue;
                }

                $json['products'][] = [
                    'id'             => $product->id,
                    'name'           => $product->getName(),
                    'time'           => $product->time,
                    'category'       => $product->category_id,
                    'category_color' => $product->getColor(),
                    'price'          => $product->getPrice(),
                    'quantity'       => $product->getQuantity(),
                    'live'           => $product->shopQuantity ? ($product->shopQuantity->updated_at->toDateString() == Carbon::now()->toDateString()) : false
                ];

                if ($product->category) {
                    $json['categories'][$product->category->id] = $product->category;
                }
            }
        }

        $json['categories'] = collect($json['categories'])->sortBy('sort')->values();

        return response()->json($json);
    }
}
