<?php

namespace App\Http\Controllers\Api;

use App\Movement;
use App\MovementProduct;
use App\ProductShopQuantity;
use App\Shift;
use App\Http\Controllers\Controller;
use App\ShiftProduct;
use Illuminate\Support\Facades\Auth;


class MovementController extends Controller
{
    public function addMovement(Shift $shift)
    {
        $movement = Movement::create([
            'shift_id'     => $shift->id,
            'from_user_id' => Auth::user()->id,
            'from_shop_id' => $shift->shop_id,
            'to_shop_id'   => request()->get('shop_id'),
            'place'        => Auth::user()->group->isKitchen ? 'kitchen' : 'shop'
        ]);

        $movement->code = sprintf('%s-%s', mb_substr($movement->id . date('His'), 0, 6), rand(111111, 999999));
        $movement->save();

        foreach (request()->post('products') as $product) {
            // Если есть этот параметр, то значит перемещение с какого то выпуска
            if (isset($product['id'])) {
                $shift_product_id = $product['id'];

                // Записываем перемещение
                $shiftProduct = ShiftProduct::findOrFail($shift_product_id);
                $shiftProduct->update([
                    'move' => $shiftProduct->move + $product['quantity']
                ]);
            } else {
                $shift_product_id = 0;
            }

            MovementProduct::create([
                'movement_id'      => $movement->id,
                'product_id'       => $product['product_id'],
                'shift_product_id' => $shift_product_id,
                'from_shop_id'     => $shift->shop_id,
                'to_shop_id'       => request()->get('shop_id'),
                'quantity'         => $product['quantity'],
                'complete'         => 0,
            ]);
        }
    }

    public function acceptMovement(Shift $shift)
    {
        Movement::where('id', request()->movement['id'])->update(['to_user_id' => Auth::user()->id]);

        foreach (request()->movement['products'] as $product) {
            MovementProduct::where('id', $product['id'])->update(['complete' => $product['complete']]);

            if ($product['shift_product_id']) {
                $shiftProduct = ShiftProduct::findOrFail($product['shift_product_id']);
                $shiftProduct->update([
                    'move' => $shiftProduct->move - $product['quantity'] + $product['complete']
                ]);
            }
        }
    }
}
