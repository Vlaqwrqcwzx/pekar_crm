<?php

namespace App\Http\Controllers\Api\StoreHouse5;

use App\Shop;
use App\Shift;
use App\Import;
use App\Reject;
use App\Product;
use App\Sh5Corr;
use App\Movement;
use App\Sh5Depart;
use App\Inventory;
use App\Sh5Setting;
use App\Sh5Waybill;
use App\ShiftProduct;
use App\RejectProduct;
use App\MovementProduct;
use App\InventoryProduct;
use App\Sh5WaybillProduct;

use Carbon\Carbon;

use App\Http\Controllers\StoreHouse5\StoreHouse5Controller;      

class ApiInvoicesController extends StoreHouse5Controller
{

    // Метод создания накладной
    public function create_invoices($options) 
    {
        
        $StoreHouse5 = new StoreHouse5Controller();
        
        $array = $StoreHouse5->SH5_CONNECT($options["setting"]->ip.":".$options["setting"]->port."/api/sh5exec", $options["setting"]->username, $options["setting"]->password, $options["proc_name"], 
            array(
                array(
                    "head" => "111",
                    "original" => array(
                        // "33" Опции заголовка накладной (см. Опции накладных)
                        "33",
                        // "32" RID соседней накладной
                        "32",
                        // "31" Дата накладной
                        "31",
                        // "34" Курс валюты (единица базовой)
                        "34",
                        // "35" к единице валюты накладной)
                        "35",
                        // "100\\1" Rid валюты
                        "100\\1",
                        // "105\\1" RID поставщика (Пример: 8388609) - Склад Кухня
                        "105\\1",
                        // "105#1\\1" RID получателя (Пример: 19)
                        "105#1\\1",
                        // "3" Номер накладной
                        "3"
                    ), 
                    "values" => array(
                        // "33" Опции заголовка накладной (см. Опции накладных)
                        array(0),
                        // "32" RID соседней накладной
                        array(0),
                        // "31" Дата накладной
                        array($options["created_at"]->format("Y-m-d H:i:s")),
                        // "34" Курс валюты (единица базовой)
                        array(1),
                        // "35" к единице валюты накладной
                        array(1),
                        // "100\\1" Rid валюты
                        array(0),
                        // "105\\1" RID поставщика (Пример: 8388609) Склад Кухня - 8388609 Склад крепкий алкоголь - "{C5E94B31-8FB3-8B30-193A-6B05C3A687D6}"
                        array($options["depart_rid"]),
                        // "105#1\\1" RID получателя (Пример: 19)
                        array($options["corr_rid"]),
                        // "3" Номер накладной
                        array($options["number"])
                    ),
                ),
                array(
                    "head" => "112",
                    "original" => array(
                        // "210\\1" Rid товара
                        "210\\1",
                        // "210\\206\\1" RID ед.изм
                        "210\\206\\1",
                        // "212#1\\9" Ставка НДС
                        "212#1\\9",
                        // "213#1\\9" Ставка НСП
                        "213#1\\9",
                        // "31" Количество
                        "31",
                        // "32" Опции спецификаций накладных
                        "32",
                        // "45" Отпускная сумма
                        "45",
                        // "46" Отпускная сумма НДС
                        "46",
                        // "47" Отпускная сумма НСП
                        "47"
                    ), 
                    "values" => array(
                        // "210\\1" Rid товара
                        $options["products"]['rid'],
                        // "210\\206\\1" RID ед.изм
                        $options["products"]['base_unit'],
                        // "212#1\\9" Ставка НДС
                        $options["products"]['rate_nds'],
                        // "213#1\\9" Ставка НСП
                        $options["products"]['rate_nsp'],
                        // "31" Количество
                        $options["products"]["quantity"],
                        // "32" Опции спецификаций накладных - ВРЕМЕННО ['rate_nds']
                        $options["products"]['specification'], 
                        // "45" Отпускная сумма
                        $options["type"] === "р/н" ? $options["products"]["amount"] : 0,
                        // "46" Отпускная сумма НДС
                        $options["products"]['v_a_nds'],
                        // "47" Отпускная сумма НСП
                        $options["products"]['v_a_nsp'],
                    ),
                )
            )
        );
        
        if($array["errorCode"] === 0) {
            
            $waybill = Sh5Waybill::create(
                [
                    'type' => $options["type"],
                    'number' => $options["number"],
                    'guid' => $array['shTable'][0]['values'][0][0],
                    'shop_id' => $options["shop_id"],
                    'worker'   =>$options["worker"],
                    'shift_id' => $options["shift_id"] == NULL ? 0 : $options["shift_id"],
                    'reject_id' => $options["reject_id"],
                    'movement_id' => $options["movement_id"],
                    'to_shop_id' => $options["to_shop_id"],
                    'inventory_id' => $options["inventory_id"],
                    'shift_products_id' => $options["shift_products_id"],
                    'payment' => $options["payment"],
                    'count' => $options["count"],
                    'depart_rid' => $options["depart_rid"],
                    'corr_rid' => $options["corr_rid"],
                    'setting_id' => $options["setting"]->id,
                    'date_start' => $options["date_interval"][0],
                    'date_end' => $options["date_interval"][1],
                    'proc_name' => $options["proc_name"],
                ]
            );

            // Сохраняем список товаров
            foreach($options["products"]["rid"] as $key => $product) {

                $data[$key]["rid"] = $options["products"]["rid"][$key];
                $data[$key]["base_unit"] = $options["products"]["base_unit"][$key];
                $data[$key]["name"] = $options["products"]["name"][$key];
                $data[$key]["guid"] = $options["products"]["guid"][$key];
                $data[$key]["price"] = $options["products"]["price"][$key];
                $data[$key]["amount"] = $options["products"]["amount"][$key];
                $data[$key]["quantity"] = $options["products"]["quantity"][$key];
                $data[$key]["product_id"] = $options["products"]["product_id"][$key];
                $data[$key]["waybill_id"] = $waybill->id;
                $data[$key]["created_at"] = $options["created_at"];
                $data[$key]["updated_at"] = $options["created_at"];

            }

            Sh5WaybillProduct::insert($data);

            return $array["errMessage"];

        } else {

            $error_file = 'StoreHouse5.logs.txt';
            $error = fopen($error_file, 'a+');
            fwrite($error, "Ошибка выгрузки накладной в StoreHouse5 errMessage: ".$array["errMessage"].' - '.$options["number"].PHP_EOL);
            fclose($error);

        }

    }

    //  Метод выгрузки по сменам
    public function computed_shifts($params)
    {
        
        // Получаем смены по дате
        if($params["shift_id"] === 0) {
            $shifts = Shift::where('type', '=', $params["worker"])
                ->whereBetween("date_end", $params["date_interval"])
                ->whereBetween("date_start", $params["date_interval"])
            ->get();
        } else {
            // Для ручной выгрузки из консоли
            $shifts = Shift::where('id', '=', $params["shift_id"])->get();

        }

        foreach($shifts as $shift) {

            if($shift->id != 0) {

                $params["date_interval"] = [$shift->date_start, $shift->date_end];

            }
            
            $shop = Shop::find($shift->shop_id);

            // Определяем дату создания накладной, и счётчик накладной по порядку
            $count_and_date = $this->getCountAndDate($params["date_interval"][0], $params["harakter"]);

            $params["count"] = $count_and_date["count"];

            $params["created_at"] = $count_and_date["created_at"];

            // Берём конкретную настройку StoreHouse5
            $params["setting"] = $this->getSettingSH5($shop);

            if ($params["setting"] == false) {

                $error_file = 'StoreHouse5.logs.txt';
                $error = fopen($error_file, 'a+');
                fwrite($error, "Ошибка выгрузки накладной в StoreHouse5 errMessage: Отсутствуют настройки StoreHouse5 в магазине ".$shop->name.PHP_EOL);
                fclose($error);

                continue;

            }

            // Определяем тип выгрузки
            $type_of_discharge = $this->typeOfDischarge($params, $shop, $shift->id);
            
            $params["corr_rid"] = $type_of_discharge["corr_rid"];

            $params["depart_rid"] = $type_of_discharge["depart_rid"];
            
            $params["number"] = $type_of_discharge["number"];

            // Запрашиваем проданные продукты за интервал времени
            $date_interval = [$shift->date_start, $shift->date_end];

            $params["date_interval"] = [$shift->date_start, $shift->date_end];

            $payment = $params["payment"];

            $shop_id = $shop->id;

            $params["shop_id"] = $shop->id;
            
            $params["shift_id"] = $shift->id;

            $product_shop_orders = Product::join('product_shop_orders', function($join) use($payment, $date_interval, $shop_id) {
                $join->on('product_shop_orders.product_id', '=', 'products.id')
                ->whereBetween('product_shop_orders.date', $date_interval)
                ->where("payment", "=", "".$payment."")
                ->where("shop_id", "=", "".$shop_id."");
            })->get();

            $options = $this->getOptions($params, $product_shop_orders);

            if (!empty($options["products"]["rid"]) && $options["corr_rid"] != NULL && $options["depart_rid"] != NULL) {
            
                $this->create_invoices($options);

            }

        }

    }

    //  Метод выгрузки за сутки вне смен
    public function computed_day($params)
    {   
        
        // Получаем смены по дате
        $shifts = Shift::where('type', '=', $params["worker"])
            ->whereBetween("date_end", $params["date_interval"])
            ->whereBetween("date_start", $params["date_interval"])
        ->get();

        $shops = $params["shop_id"] != 0 ? Shop::where('id', '=', $params["shop_id"])->get() : Shop::with('devices')->get();

        foreach($shops as $shop) {

            // Определяем дату создания накладной, и счётчик накладной по порядку
            $count_and_date = $this->getCountAndDate($params["date_interval"][0], $params["harakter"]);

            $params["count"] = $count_and_date["count"];

            $params["created_at"] = $count_and_date["created_at"];

            // Берём конкретную настройку StoreHouse5
            $params["setting"] = $this->getSettingSH5($shop);

            if ($params["setting"] == false) {

                $error_file = 'StoreHouse5.logs.txt';
                $error = fopen($error_file, 'a+');
                fwrite($error, "Ошибка выгрузки накладной в StoreHouse5 errMessage: Отсутствуют настройки StoreHouse5 в магазине ".$shop->name.PHP_EOL);
                fclose($error);
                
                continue;

            }

            // Определяем тип выгрузки
            $type_of_discharge = $this->typeOfDischarge($params, $shop, 0);
            
            $params["corr_rid"] = $type_of_discharge["corr_rid"];

            $params["depart_rid"] = $type_of_discharge["depart_rid"];

            $params["number"] = $type_of_discharge["number"];

            $params["shop_id"] = $shop->id;

            $shop_id = $shop->id;

            $payment = $params["payment"];

            $date_interval = $params["date_interval"];

            $sql = Product::join('product_shop_orders', function($join) use($payment, $date_interval, $shop_id) {
                    $join->on('product_shop_orders.product_id', '=', 'products.id')
                    ->where("payment", "=", "".$payment."")
                    ->where("shop_id", "=", "".$shop_id."")
                    ->whereBetween('product_shop_orders.date', $date_interval);
                });
                    
            foreach($shifts as $shift) {

                $date_interval = [$shift->date_start, $shift->date_end];
                $sql = $sql->whereNotBetween('product_shop_orders.date', $date_interval);

            }

            $options = $this->getOptions($params, $sql->get());
            
            if (!empty($options["products"]["rid"]) && $options["corr_rid"] != NULL && $options["depart_rid"] != NULL) {
                
                $this->create_invoices($options);

            }

        }

    }

    // Перемещение из точки в точку
    public function computed_movements_other_shop($params)
    {

        // Получаем смены по дате
        $type = $params["type"];

        $date_interval = $params["date_interval"];

        $reject_id = $params["reject_id"];
        
        if ($params["worker"] == "cash" && $params["shift_id"] === 0) {
            $shifts = Shift::where('type', '=', "cash")
                ->whereBetween("date_end", $params["date_interval"])
                ->whereBetween("date_start", $params["date_interval"])
            ->get();
        } else if ($params["shift_id"] === 0) {
            $shifts = Shift::where('type', '!=', "cash")
                ->whereBetween("date_end", $params["date_interval"])
                ->whereBetween("date_start", $params["date_interval"])
            ->get();
        } else {
            // Для ручной выгрузки из консоли
            $shifts = Shift::where('id', '=', $params["shift_id"])->get();

        }
        
        foreach($shifts as $shift) {

            if($shift->id != 0) {

                $params["date_interval"] = [$shift->date_start, $shift->date_end];

            }
            
            $params["shift_id"] = $shift->id;
            
            $shift_id = $params["shift_id"];
            
            $shop = Shop::find($shift->shop_id);

            $movements = Movement::where('movements.from_shop_id', '=', $shop->id)
                ->where('movements.shift_id', '=', $shift_id)->get();
            
            if (!empty($movements[0])) {

                foreach($movements as $movement) {

                    // Определяем дату создания накладной, и счётчик накладной по порядку
                    $count_and_date = $this->getCountAndDate($params["date_interval"][0], $params["harakter"]);

                    $params["count"] = $count_and_date["count"];

                    $params["created_at"] = $count_and_date["created_at"];

                    // Берём конкретную настройку StoreHouse5
                    $params["setting"] = $this->getSettingSH5($shop);

                    // Пропускаем цикл, если у магазина нет настроек StoreHouse5
                    if ($params["setting"] == false) {

                        $error_file = 'StoreHouse5.logs.txt';
                        $error = fopen($error_file, 'a+');
                        fwrite($error, "Ошибка выгрузки накладной в StoreHouse5 errMessage: Отсутствуют настройки StoreHouse5 в магазине ".$shop->name.PHP_EOL);
                        fclose($error);
                        
                        continue;

                    }

                    $params["date_interval"] = [$shift->date_start, $shift->date_end];

                    

                    $params["shop_id"] = $shift->shop_id;

                    $type = $params["type"];

                    $worker = $params["worker"];

                    $date_interval = $params["date_interval"];

                    $movement_products = MovementProduct::select('products.*' ,'movement_products.*')
                    ->where('movement_products.movement_id', '=', $movement->id)
                    ->join('products', function($join) {
                        $join->on('products.id', '=', 'movement_products.product_id');
                    })->get();
                    
                    foreach($movement_products as $key => $movement_product) {
                        
                        $movement_products[$key]["quantity"] = $movement_products[$key]["complete"];
                        $movement_products[$key]["amount"] = $movement_products[$key]["quantity"] * $movement_products[$key]["price"];
                        
                    }
                    $params["to_shop_id"] = $movement->to_shop_id;
                    
                    // Определяем тип выгрузки                    
                    $type_of_discharge = $this->typeOfDischarge($params, $shop, $shift_id);

                    $params["movement_id"] = $movement->id;
                    
                    $params["corr_rid"] = $type_of_discharge["corr_rid"];

                    $params["depart_rid"] = $type_of_discharge["depart_rid"];

                    $params["number"] = $type_of_discharge["number"];

                    $options = $this->getOptions($params, $movement_products);
                    

                    if (!empty($options["products"]["rid"]) && $params["number"] != "") {

                        $this->create_invoices($options);

                    }

                }

            } else {

                continue;

            }

        }

    }

    // Внутреннее перемещение кухни
    public function computed_movements($params)
    {

        // Получаем смены по дате
        $type = $params["type"];

        $date_interval = $params["date_interval"];

        $reject_id = $params["reject_id"];

        if ($params["worker"] == "cash" && $params["shift_id"] === 0) {
            $shifts = Shift::where('type', '=', "cash")
                ->whereBetween("date_end", $params["date_interval"])
                ->whereBetween("date_start", $params["date_interval"])
            ->get();
        } else if ($params["shift_id"] === 0) {
            $shifts = Shift::where('type', '!=', "cash")
                ->whereBetween("date_end", $params["date_interval"])
                ->whereBetween("date_start", $params["date_interval"])
            ->get();
        } else {
            // Для ручной выгрузки из консоли
            $shifts = Shift::where('id', '=', $params["shift_id"])->get();

        }
        
        foreach($shifts as $shift) {

            if($shift->id != 0) {

                $params["date_interval"] = [$shift->date_start, $shift->date_end];

            }

            $shop = Shop::find($shift->shop_id);

            // Определяем дату создания накладной, и счётчик накладной по порядку
            $count_and_date = $this->getCountAndDate($params["date_interval"][0], $params["harakter"]);

            $params["count"] = $count_and_date["count"];

            $params["created_at"] = $count_and_date["created_at"];

            // Берём конкретную настройку StoreHouse5
            $params["setting"] = $this->getSettingSH5($shop);

            // Пропускаем цикл, если у магазина нет настроек StoreHouse5
            if ($params["setting"] == false) {

                $error_file = 'StoreHouse5.logs.txt';
                $error = fopen($error_file, 'a+');
                fwrite($error, "Ошибка выгрузки накладной в StoreHouse5 errMessage: Отсутствуют настройки StoreHouse5 в магазине ".$shop->name.PHP_EOL);
                fclose($error);
                
                continue;

            }

            $params["date_interval"] = [$shift->date_start, $shift->date_end];

            $params["shift_id"] = $shift->id;
            
            $shift_id = $params["shift_id"];

            $params["shop_id"] = $shift->shop_id;

            $type = $params["type"];

            $worker = $params["worker"];

            $date_interval = $params["date_interval"];
            
            $shift_products = ShiftProduct::select('products.*', 'shift_products.*', 'shift_products.id')
                ->where('shift_products.status', '=', 3)
                ->where('shift_products.shift_id', '=', $shift_id)
                ->join('products', function($join) {
                    $join->on('products.id', '=', 'shift_products.product_id');
                })->get();

            // Определяем тип выгрузки
            if (!empty($shift_products[0])) {
                
                foreach($shift_products as $shift_product) {

                    $shift_product["quantity"] = $shift_product["accept"];

                    $shift_product["amount"] = $shift_product["quantity"] * $shift_product["price"];
                }

                $type_of_discharge = $this->typeOfDischarge($params, $shop, $shift_products[0]->shift_id);

            } else {

                continue;
            }
            
            $params["shift_products_id"] = $shift_products[0]->shift_id;
            
            $params["corr_rid"] = $type_of_discharge["corr_rid"];

            $params["depart_rid"] = $type_of_discharge["depart_rid"];

            $params["number"] = $type_of_discharge["number"];

            $options = $this->getOptions($params, $shift_products);
            

            if (!empty($options["products"]["rid"]) && $params["number"] != "") {

                $this->create_invoices($options);

            }

        }

    }

    //  Метод выгрузки Брак / Бракераж / Списание
    public function computed_rejects($params)
    {

        // Получаем смены по дате
        $type = $params["type"];

        $date_interval = $params["date_interval"];

        $reject_id = $params["reject_id"];
        
        if ($params["worker"] == "shop") {
            $shift_rejects = Shift::where('type', "cash")
                ->whereBetween('date_start', $params["date_interval"])->get();
        } else if ($params["worker"] == "accept") {
            $shift_rejects = Shift::where('type', "cash")
                ->whereBetween('date_start', $params["date_interval"])->get();
        } else if ($params["worker"] == "kitchen") {
            $shift_rejects = Shift::where('type', "baker")
                ->whereBetween('date_start', $params["date_interval"])->get();
        } else {
            // Для ручной выгрузки из консоли
            $shifts = Shift::where('id', '=', $params["shift_id"])->get();

        }
        
        foreach($shift_rejects as $shift_reject) {

            if($shift_reject->id != 0) {

                $params["date_interval"] = [$shift_reject->date_start, $shift_reject->date_end];

            }
            
            $params["shift_id"] = $shift_reject->id;
            
            $shift_id = $params["shift_id"];

            $rejects = Reject::where('shift_id', '=', $shift_id)
            ->where('type', '=', $params["type"])->get();

            foreach($rejects as $reject) {

                $shop = Shop::find($shift_reject->shop_id);

                // Определяем дату создания накладной, и счётчик накладной по порядку
                $count_and_date = $this->getCountAndDate($params["date_interval"][0], $params["harakter"]);

                $params["count"] = $count_and_date["count"];

                $params["created_at"] = $count_and_date["created_at"];

                // Берём конкретную настройку StoreHouse5
                $params["setting"] = $this->getSettingSH5($shop);
                
                // Пропускаем цикл, если у магазина нет настроек StoreHouse5
                if ($params["setting"] == false) {

                    $error_file = 'StoreHouse5.logs.txt';
                    $error = fopen($error_file, 'a+');
                    fwrite($error, "Ошибка выгрузки накладной в StoreHouse5 errMessage: Отсутствуют настройки StoreHouse5 в магазине ".$shop->name.PHP_EOL);
                    fclose($error);
                    
                    continue;

                }

                $params["date_interval"] = [$shift_reject->date_start, $shift_reject->date_end];

                $params["shop_id"] = $shift_reject->shop_id;

                $type = $params["type"];

                $worker = $params["worker"];

                $date_interval = $params["date_interval"];

                $reject_products = RejectProduct::select('products.*' ,'reject_products.*')
                ->where('reject_id', '=', $reject->id)
                ->join('products', function($join) {
                    $join->on('products.id', '=', 'reject_products.product_id');
                })->get();

                // Определяем тип выгрузки
                if (!empty($reject_products[0])) {

                    foreach($reject_products as $reject_product) {
                        $reject_product["amount"] = $reject_product["quantity"] * $reject_product["price"];
                    }

                    $type_of_discharge = $this->typeOfDischarge($params, $shop, $rejects[0]->shift_id);

                } else {

                    continue;

                }

                $params["reject_id"] = $reject->id;
                
                $params["corr_rid"] = $type_of_discharge["corr_rid"];

                $params["depart_rid"] = $type_of_discharge["depart_rid"];

                $params["number"] = $type_of_discharge["number"];

                $options = $this->getOptions($params, $reject_products);
                

                if (!empty($options["products"]["rid"]) && $params["number"] != "") {

                    $this->create_invoices($options);

                }
            }

        }

    }

    // Комплектация
    public function complectation($params)
    {
        // Получаем смены по дате
        $type = $params["type"];

        $date_interval = $params["date_interval"];

        $reject_id = $params["reject_id"];
        
        // Получаем смены по дате
        if($params["shift_id"] === 0) {
            $shifts = Shift::where('type', '!=', "cash")
                ->whereBetween("date_end", $params["date_interval"])
                ->whereBetween("date_start", $params["date_interval"])
            ->get();
        } else {
            // Для ручной выгрузки из консоли
            $shifts = Shift::where('id', '=', $params["shift_id"])->get();

        }
        
        foreach($shifts as $shift) {

            if($shift->id != 0) {

                $params["date_interval"] = [$shift->date_start, $shift->date_end];

            }
            
            $shop = Shop::find($shift->shop_id);
            
            // Определяем дату создания накладной, и счётчик накладной по порядку
            $count_and_date = $this->getCountAndDate($params["date_interval"][0], $params["harakter"]);

            $params["count"] = $count_and_date["count"];

            $params["created_at"] = $count_and_date["created_at"];

            // Берём конкретную настройку StoreHouse5
            $params["setting"] = $this->getSettingSH5($shop);

            // Пропускаем цикл, если у магазина нет настроек StoreHouse5
            if ($params["setting"] == false) {

                $error_file = 'StoreHouse5.logs.txt';
                $error = fopen($error_file, 'a+');
                fwrite($error, "Ошибка выгрузки накладной в StoreHouse5 errMessage: Отсутствуют настройки StoreHouse5 в магазине ".$shop->name.PHP_EOL);
                fclose($error);
                
                continue;

            }

            $params["shift_id"] = $shift->id;
            
            $shift_id = $params["shift_id"];

            $params["shop_id"] = $shift->shop_id;

            $type = $params["type"];

            $worker = $params["worker"];

            $date_interval = $params["date_interval"];
            
            $shift_products = ShiftProduct::select('products.*', 'shift_products.*')
                ->where('shift_products.shift_id', '=', $shift_id)
                ->where('shift_products.status', '=', 3)
                ->orWhere('shift_products.shift_id', '=', $shift_id)
                ->where('shift_products.status', '=', 2)
                ->join('products', function($join) {
                    $join->on('products.id', '=', 'shift_products.product_id');
                })->get();
            
            
            // Определяем тип выгрузки
            if (!empty($shift_products[0])) {
                
                foreach($shift_products as $shift_product) {

                    $shift_product["amount"] = $shift_product["quantity"] * $shift_product["price"];

                }

                $params["to_shop_id"] = $shift_products[0]->to_shop_id;
                
                $type_of_discharge = $this->typeOfDischarge($params, $shop, $shift_products[0]->shift_id);

            } else {

                continue;

            }
            
            $params["shift_products_id"] = $shift_products[0]->shift_id;
            
            $params["corr_rid"] = $type_of_discharge["corr_rid"];

            $params["depart_rid"] = $type_of_discharge["depart_rid"];

            $params["number"] = $type_of_discharge["number"];

            $options = $this->getOptions($params, $shift_products);
            

            if (!empty($options["products"]["rid"]) && $params["number"] != "") {

                $this->create_invoices($options);

            }

        }

    }

    // Комплектация
    public function inventories($params)
    {
        // Получаем смены по дате
        $type = $params["type"];

        $date_interval = $params["date_interval"];

        $reject_id = $params["reject_id"];
        
        $shifts = Shift::where('type', '=', "cash")
            ->whereBetween("date_end", $params["date_interval"])
            ->whereBetween("date_start", $params["date_interval"])
        ->get();
        
        foreach($shifts as $shift) {

            if($shift->id != 0) {

                $params["date_interval"] = [$shift->date_start, $shift->date_end];

            }

            $params["shift_id"] = $shift->id;
            
            $shift_id = $params["shift_id"];

            $inventories = Inventory::where('shift_id', '=', $shift_id)
            ->where('type', '=', $params["type"])->get();

            if (!empty($inventories[0])) {

                foreach($inventories as $inventory) {

                    $shop = Shop::find($shift->shop_id);
                
                    // Определяем дату создания накладной, и счётчик накладной по порядку
                    $count_and_date = $this->getCountAndDate($params["date_interval"][0], $params["harakter"]);

                    $params["count"] = $count_and_date["count"];

                    $params["created_at"] = $count_and_date["created_at"];

                    // Берём конкретную настройку StoreHouse5
                    $params["setting"] = $this->getSettingSH5($shop);

                    // Пропускаем цикл, если у магазина нет настроек StoreHouse5
                    if ($params["setting"] == false) {

                        $error_file = 'StoreHouse5.logs.txt';
                        $error = fopen($error_file, 'a+');
                        fwrite($error, "Ошибка выгрузки накладной в StoreHouse5 errMessage: Отсутствуют настройки StoreHouse5 в магазине ".$shop->name.PHP_EOL);
                        fclose($error);
                        
                        continue;

                    }

                    $params["date_interval"] = [$shift->date_start, $shift->date_end];

                    $params["shop_id"] = $shift->shop_id;

                    $type = $params["type"];

                    $worker = $params["worker"];

                    $date_interval = $params["date_interval"];
                    
                    $inventory_products = InventoryProduct::select('products.*', 'inventory_products.*')
                        ->where('inventory_products.inventory_id', '=', $inventory->id)
                        ->join('products', function($join) {
                            $join->on('products.id', '=', 'inventory_products.product_id');
                        })->get();
                    
                    // Определяем тип выгрузки
                    if (!empty($inventory_products[0])) {
                        
                        foreach($inventory_products as $inventory_product) {

                            $inventory_product["amount"] = $inventory_product["quantity"] * $inventory_product["price"];

                        }

                        $params["to_shop_id"] = $inventory_products;
                        
                        $type_of_discharge = $this->typeOfDischarge($params, $shop, $inventory->shift_id);

                    } else {

                        continue;
                        
                    }
                    
                    $params["inventory_id"] = $inventory->id;
                    
                    $params["corr_rid"] = $type_of_discharge["corr_rid"];

                    $params["depart_rid"] = $type_of_discharge["depart_rid"];

                    $params["number"] = $type_of_discharge["number"];
                    
                    $options = $this->getOptions($params, $inventory_products);
                    

                    if (!empty($options["products"]["rid"]) && $params["number"] != "") {

                        $this->create_invoices($options);

                    }

                }
                
            } else {

                continue;

            }

        }

    }

    // Ручная выгрузка накладных
    public function hand_made($params, $waybill_products)
    {
        
        $count_and_date = $this->getCountAndDate(Carbon::create($params["date_interval"][0]), $params["harakter"]);
    
        $params["count"] = $count_and_date["count"];

        $params["created_at"] = $count_and_date["created_at"];

        $params["setting"] = Sh5Setting::find($params["setting_id"]);

        $shop = Shop::find($params["shop_id"]);

        $type_of_discharge = $this->typeOfDischarge($params, $shop, $params["shift_id"]);

        $params["number"] = $type_of_discharge["number"];
        
        $options = $this->getOptions($params, $waybill_products);

        if (!empty($options["products"]["rid"]) && $params["number"] != "") {

            $this->create_invoices($options);

        }

    }

    private function getCountAndDate($created_at, $harakter) {

        $Sh5Waybills = NULL;

        $date = [Carbon::parse($created_at->format("Y-m-d")), Carbon::parse($created_at->format("Y-m-d"))->addDay()];
        
        $Sh5Waybills = Sh5Waybill::whereBetween('date_start', $date)->get()->last();
        
        $count = 0;
        
        if ($Sh5Waybills === NULL) {

            $count = 1;

        } else {

            $count = ++$Sh5Waybills->count;

        }
        
        $result = array(
            "created_at" => $created_at,
            "count" => $count
        );

        return $result;

    }

    private function getSettingSH5($shop) {

        if (!empty($shop->devices[0])) {

            $import = Import::where('id', '=', $shop->devices[0]->import_id)->first();

            return Sh5Setting::find($import->setting_id);
        
        } else {
        
            return false;

        }


    }
    // Определяем тип накладной
    private function typeOfDischarge($params, $shop ,$shift_id) {
        
        $number = "";
        // Расходная В смену
        if ($params["type"] === "р/н" && $params["payment"] === "CASHLESS" && $params["in_shift"] === true) {
            
            return array(
                "corr_rid"   => $shop->cashless_corr_rid,
                "depart_rid" => $shop->depart_rid_2,
                "number"     => $params["harakter"]." ".$params["count"]." эквайринг в смену № ".$shift_id." ".$shop->name." ".$params["created_at"],
            );

        } else if ($params["type"] === "р/н" && $params["payment"] === "CASH" && $params["in_shift"] === true) {
            return array(
                "corr_rid"   => $shop->cash_corr_rid,
                "depart_rid" => $shop->depart_rid_2,
                "number"     => $params["harakter"]." ".$params["count"]." наличная оплата в смену № ".$shift_id." ".$shop->name." ".$params["created_at"],
            );
            
        }
        // Расходная Вне смен
        else if ($params["type"] === "р/н" && $params["payment"] === "CASHLESS" && $params["in_shift"] === false) {

            return array(
                "corr_rid"   => $shop->cashless_corr_rid,
                "depart_rid" => $shop->depart_rid_2,
                "number"     => $params["harakter"]." ".$params["count"]." эквайринг вне смен ".$shop->name." ".$params["created_at"],
            );

        } else if ($params["type"] === "р/н" && $params["payment"] === "CASH" && $params["in_shift"] === false) {

            return array(
                "corr_rid"   => $shop->cash_corr_rid,
                "depart_rid" => $shop->depart_rid_2,
                "number"     => $params["harakter"]." ".$params["count"]." наличная оплата вне смен ".$shop->name." ".$params["created_at"],
            );
            
        }
        // Вутреннее перемещение Брак / Бракераж / Списание магазина
        else if ($params["type"] === "1" && $params["worker"] === "shop") {

            return array(
                "corr_rid"   => $shop->marriage_corr_rid,
                "depart_rid" => $shop->depart_rid_2,
                "number"     => $params["harakter"]." ".$params["count"]." брак магазина в смену № ".$shift_id." ".$shop->name." ".$params["created_at"],
            );

        } else if ($params["type"] === "2" && $params["worker"] === "shop") {

            return array(
                "corr_rid"   => $shop->brokerage_corr_rid,
                "depart_rid" => $shop->depart_rid_2,
                "number"     => $params["harakter"]." ".$params["count"]." бракераж магазина в смену № ".$shift_id." ".$shop->name." ".$params["created_at"],
            );

        } else if ($params["type"] === "3" && $params["worker"] === "shop") {

            return array(
                "corr_rid"   => $shop->write_off_corr_rid,
                "depart_rid" => $shop->depart_rid_2,
                "number"     => $params["harakter"]." ".$params["count"]." списание магазина в смену № ".$shift_id." ".$shop->name." ".$params["created_at"],
            );
            
        }
        // Брак / Бракераж кухни при приемке (кассиром)
        else if ($params["type"] === "1" && $params["worker"] === "accept") {

            return array(
                "corr_rid"   => $shop->marriage_corr_rid,
                "depart_rid" => $shop->depart_rid_2,
                "number"     => $params["harakter"]." ".$params["count"]." брак при приёмке магазина в смену № ".$shift_id." ".$shop->name." ".$params["created_at"],
            );

        } else if ($params["type"] === "2" && $params["worker"] === "accept") {

            return array(
                "corr_rid"   => $shop->brokerage_corr_rid,
                "depart_rid" => $shop->depart_rid_2,
                "number"     => $params["harakter"]." ".$params["count"]." бракераж при приёмке магазина в смену № ".$shift_id." ".$shop->name." ".$params["created_at"],
            );

        }
        // Брак / Бракераж кухни
        else if ($params["type"] === "1" && $params["worker"] === "kitchen") {

            return array(
                "corr_rid"   => $shop->marriage_corr_rid,
                "depart_rid" => $shop->depart_rid_1,
                "number"     => $params["harakter"]." ".$params["count"]." брак кухни в смену № ".$shift_id." ".$shop->name." ".$params["created_at"],
            );
        } else if ($params["type"] === "2" && $params["worker"] === "kitchen") {

            return array(
                "corr_rid"   => $shop->brokerage_corr_rid,
                "depart_rid" => $shop->depart_rid_1,
                "number"     => $params["harakter"]." ".$params["count"]." бракераж кухни в смену № ".$shift_id." ".$shop->name." ".$params["created_at"],
            );

        }
        // Перемещения
        // Перемещение кассой на склад другой точки
        else if ($params["type"] === "move_other" && $params["worker"] === "cash" && $params["to_shop_id"] > 0) {

            $other_shop = Shop::find($params["to_shop_id"]);

            if ($other_shop->depart_rid_2 == NULL) {
                return "У второй точки не указан склад StoreHouse5";
            }
            
            return array(
                "corr_rid"   => $other_shop->depart_rid_2,
                "depart_rid" => $shop->depart_rid_2,
                "number"     => $params["harakter"]." ".$params["count"]." перемещение кассиром в смену № ".$shift_id." ".$shop->name." на склад ".$other_shop->name." ".$params["created_at"],
            );
            
        } 
        // Перемещение кухней на склад другой точки
        else if ($params["type"] === "move_other" && $params["worker"] === "kitchen" && $params["to_shop_id"] > 0) {

            $other_shop = Shop::find($params["to_shop_id"]);

            if ($other_shop->depart_rid_2 == NULL) {
                return "У второй точки не указан склад StoreHouse5";
            }
            
            return array(
                "corr_rid"   => $other_shop->depart_rid_2,
                "depart_rid" => $shop->depart_rid_1,
                "number"     => $params["harakter"]." ".$params["count"]." перемещение кухни в смену № ".$shift_id." ".$shop->name." на склад ".$other_shop->name." ".$params["created_at"],
            );
            
        } 
        // Внутреннее перемещение кухни
        else if ($params["type"] === "move" && $params["worker"] === "kitchen" && $params["to_shop_id"] === 0) {
            
            return array(
                "corr_rid"   => $shop->depart_rid_2,
                "depart_rid" => $shop->depart_rid_1,
                "number"     => $params["harakter"]." ".$params["count"]." внутреннее перемещение кухни в смену № ".$shift_id." ".$shop->name." ".$params["created_at"],
            );
            
        } 
        // Комплектация
        else if ($params["type"] === "comp" && $params["worker"] === "kitchen") {

            return array(
                "corr_rid"   => $shop->depart_rid_2,
                "depart_rid" => $shop->depart_rid_2,
                "number"   => $params["harakter"]." ".$params["count"]." комплектация кухни в смену № ".$shift_id." ".$shop->name." ".$params["created_at"],
            );
            
        }
        // Сличительная ведомость
        else if ($params["type"] === "open" && $params["worker"] === "cash") {

            return array(
                "corr_rid"   => 0,
                "depart_rid" => $shop->depart_rid_2,
                "number"   => $params["harakter"]." ".$params["count"]." сличительная ведомость при открытии смены № ".$shift_id." ".$shop->name." ".$params["created_at"],
            );
            
        } else if ($params["type"] === "close" && $params["worker"] === "cash") {

            return array(
                "corr_rid"   => 0,
                "depart_rid" => $shop->depart_rid_2,
                "number"   => $params["harakter"]." ".$params["count"]." сличительная ведомость при закрытии смены № ".$shift_id." ".$shop->name." ".$params["created_at"],
            );
            
        }

    }

    private function getOptions($params, $orders) {

        $products = array(
            // "210\\1" Rid товара
            "rid" => array(),
            // "210\\206\\1" RID ед.изм
            "base_unit" => array(),
            // "212#1\\9" Ставка НДС
            "rate_nds" => array(),
            // "213#1\\9" Ставка НСП
            "rate_nsp" => array(),
            // "31" Количество
            "quantity" => array(),
            // "32" Опции спецификаций накладных - ВРЕМЕННО ['rate_nds']
            "specification" => array(),
            // "45" Отпускная сумма
            "amount" => array(),
            // "46" Отпускная сумма НДС
            "v_a_nds" => array(),
            // "47" Отпускная сумма НСП
            "v_a_nsp" => array(),
            //
            // Локальные поля
            "name" => array(),
            "guid" => array(),
            "price" => array(),
            "product_id" => array(),
            "waybill_id" => array(),
        );        
        
        foreach($orders as $order) {

            if(($params["type"] === "р/н" || $params["type"] === "1" || $params["type"] === "2" || $params["type"] === "3" || $params["type"] === "comp") && floatval($order->quantity) <= floatval(0)) {

                $text = "Ошибка выгрузки накладной в StoreHouse5 errMessage: Количество товара ".$order->name." не может быть равно 0, или меньше 0. Итого: ".$order->quantity.PHP_EOL;

                $error_file = 'StoreHouse5.logs.txt';
                $error = fopen($error_file, 'a+');
                fwrite($error, $params["number"].PHP_EOL);
                fwrite($error, $text);
                fclose($error);
                
                continue;

            } else if (($params["type"] === "move_other") && isset($order->complete) && floatval($order->complete) <= floatval(0)) {

                $text = "Ошибка выгрузки накладной в StoreHouse5 errMessage: Количество товара ".$order->name." не может быть равно 0, или меньше 0. Итого: ".$order->complete.PHP_EOL;

                $error_file = 'StoreHouse5.logs.txt';
                $error = fopen($error_file, 'a+');
                fwrite($error, $params["number"].PHP_EOL);
                fwrite($error, $text);
                fclose($error);
                
                continue;

            } else if ($params["type"] === "move" && isset($order->accept) && floatval($order->accept) <= floatval(0)) {

                $text = "Ошибка выгрузки накладной в StoreHouse5 errMessage: Количество товара ".$order->name." не может быть равно 0, или меньше 0. Итого: ".$order->accept.PHP_EOL;

                $error_file = 'StoreHouse5.logs.txt';
                $error = fopen($error_file, 'a+');
                fwrite($error, $params["number"].PHP_EOL);
                fwrite($error, $text);
                fclose($error);
                
                continue;

            } else { 
                

                if ($params["type"] === "move") {
                    
                    $order["quantity"] = floatval($order["accept"]);
                
                } else if ($params["type"] === "move_other") {

                    $order["quantity"] = floatval($order["complete"]);

                } else {

                    floatval($order["quantity"]);

                }

                array_push($products["rid"], $order->rid);
                array_push($products["base_unit"], $order->base_unit);
                array_push($products["quantity"], $order["quantity"]);
                array_push($products["amount"], floatval($order->amount / 100));
                array_push($products["rate_nds"], 0);
                array_push($products["rate_nsp"], 0);
                array_push($products["specification"], 0);
                array_push($products["v_a_nds"], 0);
                array_push($products["v_a_nsp"], 0);
                array_push($products["name"], $order->name);
                array_push($products["guid"], $order->sh5_guid == null ? $order->guid : $order->sh5_guid);
                array_push($products["price"], $order->price);
                array_push($products["product_id"], $order->product_id);

            }

        }

        return  array(
            "type"                => $params["type"],
            "worker"             => $params["worker"],
            "created_at"          => $params["created_at"],
            "count"               => $params["count"],
            "number"              => $params["number"],
            "shift_id"            => $params["shift_id"],
            "shop_id"             => $params["shop_id"],
            "reject_id"           => $params["reject_id"],
            "movement_id"         => $params["movement_id"],
            "to_shop_id"          => $params["to_shop_id"],
            "inventory_id"        => $params["inventory_id"],
            "shift_products_id"   => $params["shift_products_id"],
            "payment"             => $params["payment"],
            "depart_rid"          => $params["depart_rid"],
            "corr_rid"            => $params["corr_rid"],
            "date_interval"       => $params["date_interval"],
            "products"            => $products,
            "setting"             => $params["setting"],
            "proc_name"           => $params["proc_name"]
        );

    }

}
