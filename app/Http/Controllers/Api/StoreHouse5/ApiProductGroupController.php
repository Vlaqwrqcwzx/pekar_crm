<?php

namespace App\Http\Controllers\Api\StoreHouse5;

use App\Sh5Setting;
use App\Sh5SettingGroup;

use App\Http\Controllers\StoreHouse5\StoreHouse5Controller;

class ApiProductGroupController extends StoreHouse5Controller
{
    // Метод получения список складов
    public function getGroups($ip, $port, $username, $password) {
        
        $StoreHouse5 = new StoreHouse5Controller();

        $request = $StoreHouse5->SH5_CONNECT($ip.":"."$port"."/api/sh5exec", $username, $password, 'GGroups', array());

        if ($request["errorCode"] === 0) {

            $response = $request["errMessage"];

            $request = array(
                'rid' => $request['shTable'][0]['values'][0],
                'guid' => $request['shTable'][0]['values'][1],
                'name' => $request['shTable'][0]['values'][2],
            );

            $setting = Sh5Setting::where('ip', '=', $ip)->where('port', '=', $port)->get()->first();
            $setting_id = $setting->id;

            $groups = Sh5SettingGroup::where('setting_id', '=', $setting_id)->get();

            $array_db_guid = array();

            foreach($groups as $group) {

                array_push($array_db_guid, $group->guid);

            }

            // update
            $array_diff = array_diff($request["guid"], $array_db_guid);

            foreach($array_diff as $key => $guid) {

                if ($request["guid"][$key] != NULL) {

                    Sh5SettingGroup::updateOrCreate(
                        [
                            'rid'  => $request['rid'][$key],
                            'name' => $request['name'][$key],
                            'guid' => $request['guid'][$key],
                            'setting_id' => $setting_id,
                        ]
                    );

                }

            }

            // delete
            $array_diff = array_diff($array_db_guid, $request["guid"]);

            foreach($array_diff as $key => $guid) {

                Sh5SettingGroup::where('guid', $guid)->delete();
                
            }

        } else {

            $error_file = 'StoreHouse5.logs.txt';
            $error = fopen($error_file, 'a+');
            fwrite($error, "Ошибка папок номенклатур из StoreHouse5 errMessage: ".$array["errMessage"].PHP_EOL);
            fclose($error);

            $response = $request["errMessage"];

        }
        
        return $response;
    }
}
