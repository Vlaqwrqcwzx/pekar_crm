<?php

namespace App\Http\Controllers\Api\StoreHouse5;

use Illuminate\Http\Request;

use App\Http\Controllers\StoreHouse5\StoreHouse5Controller;

class ApiCheckConnectController extends StoreHouse5Controller
{
  // Метод проверки подключения к серверу StoreHouse5
  public function checkConnect(Request $request) {

    $response = "";

    if(isset($request)) {

        $ip = $request["ip"];
        $port = $request["port"];
        $username = $request["username"];
        $password = $request["password"];

        $StoreHouse5 = new StoreHouse5Controller();

        $request = $StoreHouse5->SH5_CONNECT($ip.":"."$port"."/api/sh5info", $username, $password, '', array());

        if ($request["errorCode"] === 0) {

        $response = "OK";

        } else {

        $error_file = 'StoreHouse5.logs.txt';
        $error = fopen($error_file, 'a+');
        fwrite($error, "Ошибка соединения со StoreHouse5 errMessage: ".$array["errMessage"].PHP_EOL);
        fclose($error);

        $response = "ERROR!";

        }

    }
    return $response;
  }
  
}
