<?php

namespace App\Http\Controllers\Api\StoreHouse5;

use App\Sh5Depart;

use App\Sh5Setting;

use Illuminate\Http\Request;

use App\Http\Controllers\StoreHouse5\StoreHouse5Controller;

class ApiDepartController extends StoreHouse5Controller
{
    // Метод получения список складов
    public function apiDeparts(Request $request) {

        $response = "";

        $settings = Sh5Setting::where('id', '=', intval($request->all()["setting_id"]))->get();
        
        foreach($settings as $setting) {

            $response = $this->getDeparts($setting);

        }
        
        return $response;

    }

     public function getDeparts($setting) {

        $response = "";

        $StoreHouse5 = new StoreHouse5Controller();

        $request = $StoreHouse5->SH5_CONNECT($setting->ip.":"."$setting->port"."/api/sh5exec", $setting->username, $setting->password, 'Departs', array());

        if ($request["errorCode"] === 0) {

            $response = $request["errMessage"];

            $request = array(
                'rid' => $request['shTable'][1]['values'][0],
                'guid' => $request['shTable'][1]['values'][1],
                'name' => $request['shTable'][1]['values'][2],
            );

            $db_guids = Sh5Depart::pluck('guid');

            $array_db_guid = array();

            foreach($db_guids as $db_guid) {

                array_push($array_db_guid, $db_guid);

            }

            // update
            $array_diff = array_diff($request["guid"], $array_db_guid);

            foreach($array_diff as $key => $guid) {

                if ($request["guid"][$key] != NULL) {

                    Sh5Depart::updateOrCreate(
                        [
                            'rid'     => $request['rid'][$key],
                            'name' => $request['name'][$key],
                            'guid'     => $request['guid'][$key],
                            'setting_id' => $setting->id,
                        ]
                    );

                } else {

                    continue;

                }
            }

            // delete
            $array_diff = array_diff($array_db_guid, $request["guid"]);

            foreach($array_diff as $key => $guid) {

                Sh5Depart::where('guid', '=', $guid)->where('setting_id', '=', $setting->id)->delete();

            }

        } else {

            $error_file = 'StoreHouse5.logs.txt';
            $error = fopen($error_file, 'a+');
            fwrite($error, "Ошибка подгрузки складов из StoreHouse5 errMessage: ".$array["errMessage"].PHP_EOL);
            fclose($error);

            $response = $request["errMessage"];

        }

        return $response;

    }

}
