<?php

namespace App\Http\Controllers\Api\StoreHouse5;

use App\Product;
use App\Sh5Setting;

use Illuminate\Http\Request;

use App\Http\Controllers\StoreHouse5\StoreHouse5Controller;

class ApiProductController extends StoreHouse5Controller {

	// Медот выгрузки товаров в StoreHouse5
	public function postProducts($products, $setting) {

		$request = array();

		foreach($products as $product) {

			if ($product->base_unit == NULL) {
				$base_unit = $product->type == "SCALABLE" ? 1 : 12;
			} else {
				$base_unit = $product->base_unit;
			}

			if($product['sh5_guid'] == NULL) {

				$StoreHouse5 = new StoreHouse5Controller();

				$request = $StoreHouse5->SH5_CONNECT($setting->ip.":"."$setting->port"."/api/sh5exec", $setting->username, $setting->password, 'InsGood',
					array(
						array(
							"head" => "210",
							"original" => array(
								// "3" наименование товара
								"3",
								// "42" флаги товара
								"42",
								// "25" Тип 0-товар, 1-услуга, 2- для калькуляций
								"25",
								// "209\\1" Товарная группа
								"209\\1",
								// "200\\1" Категория товаров
								"200\\1",
								// "290\\1" Бухгалтерская категория товаров
								"290\\1",
								// "212\\9" Ставки НДС
								"212\\9",
								// "213\\9" Ставки НСП
								"213\\9",
								// "212#1\\9" Отпускные ставки налогов (НДС)
								"212#1\\9",
								// "213#1\\9" Отпускные ставки налогов (НСП)
								"213#1\\9",
								// "53" Цена закупочная по умолчанию без налогов
								"53",
								// "56" Цена отпускная  по умолчанию без налогов
								"56",
								// "54" Цена закупочная по умолчанию включая налоги
								"54",
								// "57" Цена отпускная  по умолчанию включая налоги
								"57",
								// "19" Неизвестный параметр, но он обязателен. В документации о нём упоминании нету.
								"19"
							), 
							"values" => array(
								// "3" наименование товара
								array($product['name']),
								// "42" флаги товара
								array(0),
								// "25" Тип 0-товар, 1-услуга, 2- для калькуляций
								array(0),
								// "209\\1" Товарная группа
								array($product->rid),
								// "200\\1" Категория товаров
								array(0),
								// "290\\1" Бухгалтерская категория товаров
								array(0),
								// "212\\9" Ставки НДС
								array(0),
								// "213\\9" Ставки НСП
								array(0),
								// "212#1\\9" Отпускные ставки налогов (НДС)
								array(0),
								// "213#1\\9" Отпускные ставки налогов (НСП)
								array(0),
								// "53" Цена закупочная по умолчанию без налогов
								array(0),
								// "56" Цена отпускная  по умолчанию без налогов
								array(0),
								// "54" Цена закупочная по умолчанию включая налоги
								array(0),
								// "57" Цена отпускная  по умолчанию включая налоги
								array(0),
								// "19" Неизвестный параметр, но он обязателен. В документации о нём упоминании нету.
								array(0)
							),
						),
						array(
							"head" => "211#1", 
							"original" => array(
								// "206\\1" единица измерения 12 - Шт.
								"206\\1",
								// "10" масса тары в гр
								"10",
								// "8" Маска типов ед.изм. товара
								"8",
								// "41" Коэффициент пересчета в базовую ед.изм 
								"41" ,
							),
							"values" => array(
								// "206\\1" единица измерения 12 - Шт.
								array($base_unit),
								// "10" масса тары в гр
								array(0),
								// "8" Маска типов ед.изм. товара
								array(31),
								// "41" Коэффициент пересчета в базовую ед.изм
								array(1),
							)
						)
					)
				);

				if ($request["errorCode"] === 0) {

					$response = $request["errMessage"];

					$product = Product::where('id', $product['id'])->update(
						[
							'rid'     => $request['shTable'][0]['values'][36][0],
							'sh5_guid' => $request['shTable'][0]['values'][0][0],
							'base_unit' => $request['shTable'][1]['values'][0][0],
						]
					);
				
				} else {

					$error_file = 'StoreHouse5.logs.txt';
					$error = fopen($error_file, 'a+');
					fwrite($error, "Ошибка выгрузки товаров в StoreHouse5 errMessage: ".$array["errMessage"].PHP_EOL);
					fclose($error);
					
					$response = $request["errMessage"];
		
				}

			}

		}

		return $response;
		
	}

  	// Метод получения списка товаров
  	public function Goods($products, $setting, $import_id=0) {

		$request = array();

		if($import_id == 0) {
			$import_id = $setting->import_id;
		}

		$setting_group = $products[0]->rid;

		$StoreHouse5 = new StoreHouse5Controller();

		$request = $StoreHouse5->SH5_CONNECT($setting->ip.":"."$setting->port"."/api/sh5exec", $setting->username, $setting->password, 'Goods',
			array(
				array(
					"head" => "209",
					// "209\\1" Товарная группа
					"original" => array("1"),
					"values" => array(array($setting_group))
				)
			)
		);

		if ($request["errorCode"] != 0) {

			return $request["errMessage"];

		}

		$array_db_guid = array();

		foreach($products as $product) {

			array_push($array_db_guid, $product->sh5_guid);

		}


		$sh5_guids = $request['shTable'][1]['values'][1];
		
		$array_diff = array_diff($array_db_guid, $sh5_guids);

		foreach($array_diff as $guid) {

			if ($guid != NULL) {

				Product::where('sh5_guid', '=', $guid)->update(
					[
						'rid'     => NULL,
						'sh5_guid' => NULL,
						'base_unit' => NULL,
					]
				);

			}
		}

		if(empty($array_diff)) {

			return "NOTHING";

		} else {
			$products = Product::select('products.name', 'products.type', 'products.id', 
			'products.sh5_guid', 'products.base_unit', 'imports.setting_group_id',
			'imports.setting_id', 'sh5_setting_groups.rid')
                    ->join('imports', function($join) use($import_id) {
						$join->on('imports.id', '=', 'products.import_id')
						->where('products.import_id', '=', $import_id)
						->whereNull('products.sh5_guid');
                    })
                    ->join('sh5_setting_groups', function($join) {
                        $join->on('sh5_setting_groups.id', '=', 'imports.setting_group_id');
                    });
					
			$products = $products->get();

			return $products;
		}
	}
	
	public function apiProduct(Request $request)
	{
		$import_id = intval($request["import_id"]);

		$products = Product::select('imports.setting_group_id', 'imports.setting_id',
                'sh5_setting_groups.rid', 'products.name', 'products.id', 'products.sh5_guid')
                    ->join('imports', function($join) use($import_id) {
						$join->on('imports.id', '=', 'products.import_id')
						->where('products.import_id', '=', $import_id);
                    })
                    ->join('sh5_setting_groups', function($join) {
                        $join->on('sh5_setting_groups.id', '=', 'imports.setting_group_id');
                    })
				->get();

		$setting = Sh5Setting::find($products[0]->setting_id);
		
		if ($this->Goods($products, $setting, $import_id) == "NOTHING") {

			return "NOTHING";

		} else {
			$products = $this->Goods($products, $setting, $import_id);

			return $this->postProducts($products, $setting);
		}
	}
}