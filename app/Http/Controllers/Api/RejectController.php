<?php

namespace App\Http\Controllers\Api;

use App\ProductShopQuantity;
use App\Reject;
use App\RejectProduct;
use App\Shift;
use App\Shop;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class RejectController extends Controller
{
    public function addReject(Shift $shift)
    {
        Reject::addReject($shift, request()->post('type'), request()->post('products'), Auth::user()->group->isKitchen ? 'kitchen' : 'shop');
    }
}
