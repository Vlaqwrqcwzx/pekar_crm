<?php

namespace App\Http\Controllers\Api;

use App\Movement;
use App\Product;
use App\Reject;
use App\Shift;
use App\ShiftProduct;
use App\ProductShopQuantity;
use App\ShiftProductHistory;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class ShiftController extends Controller
{
    public function getShift(Shift $shift)
    {
        $json = [
            'shop'       => $shift->shop->name,
            'shop_id'    => $shift->shop_id,
            'user'       => Auth::user()->name,
            'place'      => Auth::user()->group->isKitchen ? 'kitchen' : 'shop',
            'csrf'       => csrf_token(),

            // Принятие товара
            'accepts'    => [
                'products' => [],
                'total'    => 0
            ],

            // Смена
            'shift'      => [
                'hours' => collect(range(0, 23))->map(function ($item) {
                    return [
                        'products'  => [],
                        'available' => (bool)(date('H') <= $item),
                        'current'   => (bool)(date('H') == $item)
                    ];
                })->toArray(),
                'total' => 0
            ],

            // Наличия на витринах
            'stocks' => [
                'shop_id'  => request()->shop_id ? request()->shop_id : $shift->shop_id,
                'products' => [],
                'total'    => 0
            ],

            // Выпущенные и на перемещении товары
            'products' => [
                'products' => [
                    'completed' => [],
                    'moved'     => [],
                    'wronged'   => []
                ],
                'total'    => 0
            ],

            // Брак, списание
            'rejects' => [
                'types'   => Reject::TYPES,
                'rejects' => [],
                'totals'  => [],
                'total'   => 0
            ],

            // Перемещения и принятые товары
            'movements'  => [
                'movements' => [
                    'in'  => [],
                    'out' => []
                ],
                'accepted'  => [],
                'total'     => 0
            ]
        ];

        // Работник кухни видит товары только своей смены

        if (Auth::user()->group->isKitchen) {
            $shiftProducts = ShiftProduct::where('shift_id', $shift->id);
        } else {
            // Кассир видит все товары сегодняшних смен

            $shiftProducts = ShiftProduct::where('shop_id', $shift->shop_id)
                ->whereDate('updated_at', Carbon::now()->toDateString())
                ->with('shift');
        }

        $shiftProducts->with([
            'plan',
            'movementProduct',
            'product' => function ($query) use ($shift) {
                $query->with([
                    'category',
                    'shopPrice' => function ($query) use ($shift) {
                        $query->where('shop_id', $shift->shop_id)->whereDate('date', date('Y-m-d'));
                    },
                    'shopQuantity' => function ($query) use ($shift) {
                        $query->where('shop_id', $shift->shop_id)
                            ->whereDate('date', Carbon::today()->toDateString());
                    }
                ]);
            },
            'histories' => function ($query) {
                $query->orderByDesc('created_at');
            }
        ]);

        $categoryProducts = $shiftProducts->get()->sortBy('product.category.sort')->groupBy('product.category.id');

        foreach ($categoryProducts as $products) {
            foreach ($products->sortBy('product.name') as $product) {
                // Если товар добавлен во время смены или сейчас смена кассира, то не смещаем часы

                if (!$product->plan || Auth::user()->group->isCash) {
                    $hour = $product->hour;
                } else {
                    $hour = Carbon::createFromTime($product->hour, 30)->subMinutes($product->product->time)->format('G');
                }

                $json['shift']['total'] += $product->product->getPrice() * (float)$product->quantity;
                $json['shift']['hours'][$hour]['products'][] = [
                    'id'        => $product->id,
                    'product'   => [
                        'name'  => $product->product->getName(),
                        'id'    => $product->product->id,
                        'color' => $product->product->getColor()
                    ],
                    'plan'      => [
                        'name' => $product->plan ? $product->plan->name : '-',
                        'id'   => $product->plan ? $product->plan->id : 0
                    ],
                    'quantity'  => (float)$product->quantity,
                    'complete'  => (float)$product->complete,
                    'accept'    => (float)$product->accept,
                    'diff'      => $product->toAccept(),
                    'moved'     => $product->isMovementAccepted,
                    'total'     => $product->product->getQuantity(),
                    'status'    => $product->status,
                    'hour'      => $hour,
                    'available' => $json['shift']['hours'][$hour]['available'],
                    'code1'     => $product->code1,
                    'code2'     => $product->code2,
                    'error'     => ''
                ];

                $completeHistory = $product->histories->first(function ($item) {
                    return $item->isCompleted() ? $item : false;
                });

                $acceptHistory = $product->histories->first(function ($item) {
                    return $item->isAccepted() ? $item : false;
                });

                // Выпущенные товары и принятые товары

                $productData = [
                    'id'            => $product->id,
                    'product_id'    => $product->product->id,
                    'name'          => $product->product->getName(),
                    'color'         => $product->product->getColor(),
                    'sort'          => $product->product->getSort(),
                    'code1'         => $product->code1,
                    'code2'         => $product->code2,
                    'code3'         => $product->code3,
                    'date_complete' => $completeHistory ? $completeHistory->created_at->toDateTimeString() : '',
                    'date_accept'   => $acceptHistory ? $acceptHistory->created_at->toDateTimeString() : ''
                ];

                // Если товар принят полностью или частично
                if ((float)$product->accept) {
                    $json['movements']['accepted'][] = array_merge($productData, [
                        'quantity' => (float)$product->accept,
                        'price'    => $product->accept * $product->product->getPrice()
                    ]);
                }

                if ($product->isCompleted || $product->isAccepted) {
                    // Если товар частично на перемещении
                    if ((float)$product->move && !$product->isMoveAccepted()) {
                        $key = 'moved';

                        $json['products']['products'][$key][] = array_merge($productData, [
                            'quantity' => (float)$product->move,
                            'price'    => $product->move * $product->product->getPrice()
                        ]);
                    }

                    // Товары ложные
                    if ($product->code3) {
                        $key = 'wronged';

                        $json['products']['products'][$key][] = array_merge($productData, [
                            'quantity' => (float)$product->wrong,
                            'price'    => $product->wrong * $product->product->getPrice()
                        ]);
                    }

                    // Если есть еще что то принять
                    if ($product->toAccept()) {
                        $key = 'completed';

                        $json['products']['products'][$key][] = array_merge($productData, [
                            'quantity' => $product->toAccept(),
                            'price'    => $product->toAccept() * $product->product->getPrice()
                        ]);

                        $json['products']['total'] += $product->product->getPrice() * $product->toAccept();

                        // Список товаров, которые должен принять кассир от пекаря

                        $json['accepts']['products'][] = [
                            'id'         => $product->id,
                            'product_id' => $product->product->id,
                            'name'       => $product->product->getName(),
                            'quantity'   => (float)$product->quantity,
                            'complete'   => (float)round($product->toAccept() + (float)$product->accept, 3),
                            'accept'     => (float)$product->accept,
                            'code1'      => $product->code1,
                            'code2'      => $product->code2
                        ];

                        $json['accepts']['total'] += $product->product->getPrice() * $product->complete;
                    }

                    // Все выпущенные товары

                    $key = 'all';

                    $json['products']['products'][$key][] = array_merge($productData, [
                        'quantity' => (float)$product->complete,
                        'price'    => $product->complete * $product->product->getPrice()
                    ]);
                }
            }
        }

        // Наличие на точках
        $categoryProducts = Product::visibleShop()->with([
            'category',
            'shopPrice' => function($query) use ($shift) {
                $query->where('shop_id', request()->shop_id ? request()->shop_id : $shift->shop_id)->whereDate('date', date('Y-m-d'));
            },
            'shopQuantity' => function($query) use ($shift) {
                $query->where('shop_id', request()->shop_id ? request()->shop_id : $shift->shop_id)
                    ->whereDate('date', Carbon::today()->toDateString());
                    // ->whereColumn('created_at', '!=', 'updated_at');

            }
        ])->where('hide_shop', '!=', '1')
            ->orderBy('name')
            ->get()
            ->groupBy('category.id')
            ->sortBy(function ($item) {
                return $item->first()->getSort();
            });

        foreach ($categoryProducts as $products) {
            foreach ($products as $product) {
                if ($product->getQuantity()) {
                    $json['stocks']['total'] += $product->getPrice() * $product->getQuantity();
                    $json['stocks']['products'][] = [
                        'product_id' => $product->id,
                        'name'       => $product->getName(),
                        'category'   => $product->category_id,
                        'color'      => $product->getColor(),
                        'price'      => $product->getPrice(),
                        'quantity'   => $product->getQuantity()
                    ];
                }
            }
        }

        // Списания

        // Добавим ключи из типов
        array_map(function ($type) use (&$json) {
            $json['rejects']['rejects'][$type] = [];
            $json['rejects']['totals'][$type] = 0;
        }, array_keys(Reject::TYPES));

        $rejects = Reject::with([
            'rejectProducts.product.shopPrice',
            'user'
        ])->where('shift_id', $shift->id)->get();

        foreach ($rejects as $reject) {
            $json['rejects']['rejects'][$reject->type][] = [
                'id'       => $reject->id,
                'code'     => $reject->code,
                'user'     => $reject->user->name,
                'name'     => $reject->typeName,
                'type'     => $reject->type,
                'place'    => $reject->getPlace(),
                'time'     => $reject->created_at->toDateTimeString(),
                'products' => $reject->rejectProducts->map(function ($item) use (&$json, $reject) {
                    $json['rejects']['total'] += $item->product->getPrice() * $item->quantity;
                    $json['rejects']['totals'][$reject->type] += $item->product->getPrice() * $item->quantity;

                    return [
                        'name'     => $item->product->getName(),
                        'price'    => $item->product->getPrice(),
                        'quantity' => (float)$item->quantity
                    ];
                })
            ];
        }

        // Перемещения

        $movements = Movement::with([
            'movementProducts.product.shopPrice',
            'fromUser',
            'toUser',
            'fromShop',
            'toShop'
        ])->whereDate('created_at', date('Y-m-d'))
            ->where(function ($query) use ($shift) {
                $query->where('to_shop_id', $shift->shop_id)->orWhere('from_shop_id', $shift->shop_id);
            })->get();

        foreach ($movements as $movement) {
            $group = ($movement->to_shop_id == $shift->shop_id) ? 'in' : 'out';

            $json['movements']['movements'][$group][] = [
                'id'       => $movement->id,
                'group'    => $group,
                'code'     => $movement->code,
                'user'     => $movement->fromUser->name,
                'accept'   => $movement->toUser ? $movement->toUser->name : '',
                'shop'     => $movement->getName(),
                'count'    => $movement->movementProducts->count(),
                'created'  => $movement->created_at->toDateTimeString(),
                'updated'  => $movement->updated_at->toDateTimeString(),
                'products' => $movement->movementProducts->map(function ($item) use ($movement) {
                    return [
                        'id'               => $item->id,
                        'shift_product_id' => $item->shift_product_id,
                        'name'             => $item->product->getName(),
                        'price'            => $item->product->getPrice(),
                        'quantity'         => (float)$item->quantity,
                        'complete'         => (float)$item->complete,
                        'diff'             => $movement->toUser ? $item->quantity - $item->complete : 0
                    ];
                }),
                'total'    => $movement->movementProducts->sum(function ($item) use (&$json) {
                    $json['movements']['total'] += $item->product->getPrice() * $item->quantity;

                    return $item->product->getPrice() * $item->quantity;
                }),
                'diff'    => $movement->toUser ? $movement->movementProducts->sum(function ($item) {
                    return $item->quantity - $item->complete;
                }) : 0,
                'edit'    => (bool)(!$movement->toUser && $movement->to_shop_id == $shift->shop_id)
            ];
        }

        // Заменяем уникальные ключи на обычные
        //foreach ($json['totals']['products'] as &$products) {
            //$products = array_values($products);
        //}

        return response()->json($json);
    }

    public function setStatus(Shift $shift)
    {
        Validator::make(request()->all(), [
            'status' => [
                'required',
                Rule::in([0, 1, 2])
            ]
        ])->validate();

        $json = [];

        $status = request()->status;
        $complete = request()->complete;
        $plan = request()->plan;

        $shift->load(['shiftProducts' => function ($query) {
            $query->where('id', request()->id);
        }]);

        if ($status === 2 && $complete == 0 && !$plan) {
            $json['error'] = 'Выпуск не может быть нулевым';
        } else {
            $shiftProduct = $shift->shiftProducts->first();

            if ($shiftProduct) {
                $shiftProduct->update([
                    'status'   => $status,
                    'complete' => $complete,
                ]);

                // Записываем историю смены статусов
                ShiftProductHistory::create([
                    'shift_product_id' => $shiftProduct->id,
                    'user_id'          => Auth::user()->id,
                    'complete'         => $complete ? $complete : 0,
                    'status'           => $status
                ]);
            }
        }

        return response()->json($json);
    }

    /**
     * Принятие товара кассиром от пекаря
     */
    public function acceptProduct(Shift $shift)
    {
        $shiftProduct = ShiftProduct::findOrFail(request()->shift_product_id);

        Validator::make([
            'total'  => request()->accept + request()->reject1 + request()->reject2
        ], [
            'total'  => 'numeric|max:' . $shiftProduct->toAccept()
        ], [
            'total.max'  => 'Принятых товаров больше чем выпущенных'
        ])->validate();

        if ($shiftProduct) {
            $shiftProduct->update([
                'accept'  => $shiftProduct->accept + request()->accept,
                'status'  => 3
            ]);

            ShiftProductHistory::create([
                'shift_product_id' => request()->shift_product_id,
                'user_id'          => Auth::user()->id,
                'accept'           => request()->accept,
                'reject1'          => request()->reject1,
                'reject2'          => request()->reject2,
                'status'           => 3
            ]);

            // Если есть брак, то числим его за кухней

            if (request()->reject1) {
                Reject::addReject($shiftProduct->shift, 1, [
                    [
                        'id'         => request()->shift_product_id,
                        'product_id' => $shiftProduct->product_id,
                        'quantity'   => request()->reject1
                    ]
                ], 'accept');
            }

            if (request()->reject2) {
                Reject::addReject($shift, 2, [
                    [
                        'id'         => request()->shift_product_id,
                        'product_id' => $shiftProduct->product_id,
                        'quantity'   => request()->reject2
                    ]
                ], 'accept');
            }
        }
    }

    /**
     * @param Shift $shift
     *
     * Смена кол-ва товара в плане производства
     */
    public function setQuantity(Shift $shift)
    {
        Validator::make(request()->all(), [
            'id'       => ['required'],
            'complete' => ['required']
        ])->validate();

        if (request()->complete == 0) {
            $json['error'] = 'Нельзя начать с нулевым значением';

            return response()->json($json);
        } else {
            $shift->shiftProducts()
                ->where('id', request()->id)
                ->update([
                    'complete' => request()->complete
                ]);
        }
    }

    public function addProduct(Shift $shift)
    {
        Validator::make(request()->all(), [
            'hour' => [
                'required',
                Rule::in(range(0, 24))
            ]
        ])->validate();

        foreach (request()->post('products') as $product) {
            $key = $product['product_id'] . request()->hour . Carbon::now()->format('His');

            $code1 = sprintf('%s-%s', mb_substr($key . request()->hour, 0, 6), rand(111111, 999999));
            $code2 = sprintf('%s-%s', mb_substr($key . request()->hour, 0, 6), rand(111111, 999999));

            ShiftProduct::create([
                'shift_id'   => $shift->id,
                'plan_id'    => 0,
                'product_id' => $product['product_id'],
                'shop_id'    => $shift->shop_id,
                'hour'       => request()->hour,
                'status'     => 0,
                'quantity'   => $product['quantity'],
                'code1'       => $code1,
                'code2'       => $code2
            ]);
        }
    }

    public function addWrongProduct(Shift $shift)
    {
        foreach (request()->post('products') as $product) {
            $shiftProduct = ShiftProduct::findOrFail($product['id']);
            $key = $product['product_id'] . Carbon::now()->format('His');

            $shiftProduct->update([
                'wrong' => $shiftProduct->wrong + $product['quantity'],
                'code3' => $code2 = sprintf('%s-%s', mb_substr($key . rand(1111, 9999), 0, 6), rand(111111, 999999))
            ]);
        }
    }

    public function removeWrongProduct(Shift $shift)
    {
        $shiftProduct = ShiftProduct::findOrFail(request()->product['id']);
        $shiftProduct->update([
            'wrong' => 0,
            'code3' => ''
        ]);
    }
}
