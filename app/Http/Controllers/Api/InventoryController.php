<?php

namespace App\Http\Controllers\Api;

use App\Inventory;
use App\InventoryProduct;
use App\Product;
use App\Shift;
use App\ProductShopQuantity;
use App\Http\Controllers\Controller;
use App\UserShift;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class InventoryController extends Controller
{
    public function getInventory(Shift $shift)
    {
        $json = [
            'shop_id'  => $shift->shop_id,
            'type'     => Inventory::OPEN,
            'products' => []
        ];

        $shift->load('shop');

        // Берем последнюю на этой точке
        $lastInventory = Inventory::last($shift->shop_id);

        if ($lastInventory->isNotEmpty()) {
            // Если последняя открытая, то текующая получается закрытая

            if ($lastInventory->first()->isOpen) {
                $json['type'] = Inventory::CLOSE;

                // Получаем товары по которым сегодня были измеения в наличии
                $categoryProducts = Product::visibleShop()
                    ->where('import_id', $shift->shop->import_id)
                    ->with([
                        'category',
                        'shopQuantity' => function ($query) use ($shift) {
                            $query->where('shop_id', $shift->shop_id)
                                ->whereDate('date', Carbon::today()->toDateString());
                        }]
                    )->orderBy('name')
                        ->get()
                        ->groupBy('category.id')
                        ->sortBy(function ($item) {
                            return $item->first()->getSort();
                        });

                foreach ($categoryProducts as $products) {
                    foreach ($products as $product) {
                        if ($product->shopQuantity) {
                            $json['products'][] = [
                                'product_id' => $product->id,
                                'name'       => $product->getName(),
                                'color'      => $product->getColor(),
                                'quantity'   => 0,
                                'complete'   => 0,
                                'visible'    => (float)$product->shopQuantity->quantity > 0
                            ];
                        }
                    }
                }
            } else {
                // Если нет, то мы открываем новую смену и получаем наличия последней инвентаризации

                $categoryProducts = $lastInventory->first()
                    ->inventoryProducts
                    ->load('product.category')
                    ->sortBy('product.category.sort')
                    ->groupBy('product.category.id');;

                foreach ($categoryProducts as $products) {
                    foreach ($products->sortBy('product.name') as $product) {
                        if ((float)$product->quantity > 0) {
                            $json['products'][] = [
                                'product_id' => $product->product->id,
                                'name'       => $product->product->getName(),
                                'color'      => $product->product->getColor(),
                                'quantity'   => (float)$product->quantity,
                                'complete'   => 0,
                                'visible'    => true
                            ];
                        }
                    }
                }
            }
        }

        return response()->json($json);
    }

    public function addInventory(Shift $shift)
    {
        $inventory = Inventory::create([
            'shift_id' => $shift->id,
            'shop_id'  => $shift->shop_id,
            'user_id'  => Auth::user()->id,
            'type'     => request()->post('type')
        ]);

        $inventory->code = sprintf('%s-%s', mb_substr($inventory->id . date('His'), 0, 6), rand(111111, 999999));
        $inventory->save();

        foreach (request()->post('products') as $product) {
            InventoryProduct::create([
                'inventory_id' => $inventory->id,
                'product_id'   => $product['product_id'],
                'shop_id'      => $shift->shop_id,
                'quantity'     => $product['complete']
            ]);
        }

        if (request()->post('type') == Inventory::OPEN) {
            $success = route('shift.view', ['shop' => UserShift::open()->first()->shift_id]);
        } else {
            $success = route('shift.close', ['shop' => $shift->id]);
        }

        return response()->json([
            'success' => $success
        ]);
    }
}
