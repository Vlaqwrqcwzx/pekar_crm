<?php

namespace App\Http\Controllers;

use App\Reject;
use App\Shop;

class RejectController extends ReportController
{
    public function index()
    {
        $this->data = [
            'shops' => Shop::all(),
            'types' => Reject::TYPES
        ];

        $rejects = Reject::with('rejectProducts.product')
            ->with('user')
            ->with('shop')
            ->whereDate('created_at', '>=', $this->filters['filter_date_start'])
            ->whereDate('created_at', '<=', $this->filters['filter_date_end']);

        if ($this->filters['filter_shop']) {
            $rejects->where('shop_id', $this->filters['filter_shop']);
        }

        if ($this->filters['filter_type']) {
            $rejects->where('type', $this->filters['filter_type']);
        }

        $this->data['rejects'] = $rejects->get();

        return view('reject.index', $this->data, $this->filters);
    }

    public function view(Reject $reject)
    {
        $totals = [
            'count' => [
                'countable' => 0,
                'scalable'  => 0,
            ],
            'price' => [
                'countable' => 0,
                'scalable'  => 0,
            ]
        ];

        $reject->load(['rejectProducts.product.shopPrice' => function ($query) use ($reject) {
            $query->where('shop_id', $reject->shop_id)
                ->whereDate('date', $reject->created_at->format('Y-m-d'));
        }]);

        $reject->rejectProducts->each(function ($item) use (&$totals) {
            $type = $item->product->getType();
            $price = $item->product->getPrice();

            $totals['count'][$type] += $item->quantity;
            $totals['price'][$type] += $item->quantity * $price;
        });

        return view('reject.view', [
            'reject' => $reject,
            'totals' => $totals
        ]);
    }
}
