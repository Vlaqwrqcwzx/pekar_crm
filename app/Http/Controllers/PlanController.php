<?php

namespace App\Http\Controllers;

use App\Plan;
use App\PlanProduct;
use App\Shop;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class PlanController extends Controller
{
    protected $days = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'];

    public function index()
    {
        return $this->view(Auth::user()->shopsVisible()->first());
    }

    public function shop(Shop $shop)
    {
        return $this->view($shop);
    }

    public function plan(Plan $plan)
    {
        return $this->view($plan->shop()->first(), $plan);
    }

    public function view(Shop $shop, Plan $plan = null)
    {
        $plans = Plan::where('shop_id', $shop->id)
            ->with([
                'products.product.shopPrice' => function($query) use ($shop) {
                    $query->where('shop_id', $shop->id)->whereDate('date', date('Y-m-d'));
                }
            ])->get();

        $plans->each(function ($item) {
            $item->total = $item->products->sum(function ($item) {
                return $item->product->getPrice() * $item->quantity;
            });
        });

        $data = [
            'shops' => Auth::user()->shopsVisible,
            'plans' => $plans,
            'shop'  => $shop,
            'plan'  => $plan,
            'days'  => $this->days,
            'types' => Plan::TYPES
        ];

        return view('plan.index', $data);
    }

    public function store(Request $request)
    {
        Validator::make($request->all(), [
            'name' => [
                'required',
                'max:255',
            ],
            'shop_id' => [
                'min:1'
            ]
        ])->validate();

        Plan::create([
            'name'    => $request->post('name'),
            'shop_id' => $request->post('shop_id'),
            'type'    => $request->post('type')
        ]);

        return redirect()
            ->route('plan.shop', ['shop_id' => $request->post('shop_id')])
            ->with('success', 'План успешно создан');
    }

    public function update(Plan $plan)
    {
        $plan->update([
            'name' => request()->name
        ]);

        return redirect()->route('plan.plan', $plan->id);
    }

    public function copy(Plan $plan) {
        $plan->load('products');

        $copyPlan = $plan->replicate();

        $copyPlan->fill([
            'name'    => sprintf('Копия - %s', $plan->name),
            'shop_id' => request()->get('shop_id', $plan->shop_id)
        ]);

        foreach ($this->days as $day) {
            $copyPlan->$day = 0;
        }

        $copyPlan->save();

        $plan->products->each(function ($item) use ($copyPlan) {
            $item->replicate()->fill(['plan_id' => $copyPlan->id])->save();
        });

        return redirect()->route('plan.plan', $copyPlan->id);
    }

    public function delete(Plan $plan) {
        foreach ($this->days as $day) {
            if ($plan->$day) {
                return redirect()->route('plan.shop', $plan->shop_id)->with('warning', 'Нельзя удалять активный план');
            }
        }

        $plan->products()->delete();
        $plan->delete();

        return redirect()->route('plan.shop', $plan->shop_id);
    }

    public function updatePlanDays(Request $request, $shop_id = 0)
    {
        if ($shop_id) {
            Plan::where('shop_id', (int)$shop_id)->update(array_combine($this->days, array_values(array_fill(0, count($this->days), 0))));

            if ($request->has('plan')) {
                foreach ($request->post('plan') as $id => $days) {
                    Plan::where('id', (int)$id)->update($days);
                }
            }
        }

        if ($request->has('plan_id')) {
            $redirect = redirect()->route('plan.plan', ['plan_id' => $request->post('plan_id')]);
        } else {
            $redirect = redirect()->route('plan.shop', ['shop_id' => $shop_id]);
        }

        return $redirect->with('success', 'Планы успешно обновлены');
    }

    public function getPlan(Plan $plan)
    {
        $hours = array_map(function () {
            return 0;
        }, range(0, 23));

        $json = [
            'hours'    => $hours,
            'counts'   => $hours,
            'totals'   => $hours,
            'products' => []
        ];

        $categoryProducts = Product::whereNotNull('time')
            ->with([
                'category',
                'planProducts' => function ($query) use ($plan) {
                    $query->where('plan_id', $plan->id);
                },
                'shopPrice' => function ($query) use ($plan) {
                    $query->where('shop_id', $plan->shop_id)->whereDate('date', date('Y-m-d'));
                }
            ])
                ->where('import_id', $plan->shop->import_id)
                ->orderBy('name')
                ->get()
                ->groupBy('category.id')
                ->sortBy(function ($item) {
                    return $item->first()->getSort();
                });

        foreach ($categoryProducts as $products) {
            foreach ($products as $product) {
                $json['products'][$product->id] = [
                    'id'    => $product->id,
                    'name'  => $product->getName(),
                    'color' => $product->getColor(),
                    'total' => 0,
                    'time'  => $product->time,
                    'count' => 0,
                    'top'   => $product->top,
                    'new'   => $product->new,
                    'hours' => $hours
                ];

                foreach ($product->planProducts as $planProduct) {
                    $json['products'][$product->id]['total'] += $planProduct->quantity;
                    $json['products'][$product->id]['hours'][$planProduct->hour] = (float)$planProduct->quantity;

                    $json['counts'][$planProduct->hour] += $planProduct->quantity;
                    $json['totals'][$planProduct->hour] += $planProduct->quantity * $product->getPrice();
                }
            }
        }

        foreach ($json['totals'] as &$total) {
            $total = round($total / 1000, 1);
        }

        $json['products'] = array_values($json['products']);

        return response()->json($json);
    }

    public function updatePlanProducts(Request $request, $plan_id = 0)
    {
        PlanProduct::updateOrCreate(
            [
                'plan_id'    => $plan_id,
                'product_id' => $request->post('product_id'),
                'hour'       => $request->post('hour'),
            ],
            [
                'quantity' => $request->post('quantity')
            ]
        );
    }
}
