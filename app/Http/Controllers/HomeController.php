<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\UserGroup;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if (Auth::user()->group) {
            if (Auth::user()->group->isKitchen || Auth::user()->group->isCash) {
                return redirect()->route('shift.index');
            } else {
                $user_group = Auth::user()->group;
                $group_menu = (array)unserialize($user_group->menu);
                
                if (in_array('report', $group_menu))
                    return redirect()->route('report.shop');
            }
        }

        return view('home', [
            'user' => Auth::user()
        ]);
    }
}
