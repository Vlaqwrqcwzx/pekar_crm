<?php

namespace App\Http\Controllers;

use App\Device;
use App\Shop;
use App\Import;
use App\Product;
use App\Sh5Setting;
use App\Sh5SettingGroup;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Validator;

class DeviceController extends Controller
{
    public function index()
    {

        return view('device.index', [
            'devices' => Device::all(),
            'imports' => Import::all()
        ]);
    }

    public function edit(Device $device)
    {
        return view('device.form', [
            'device'   => $device,
            'shops'  => Shop::all(),
            'settings' => Sh5Setting::all(),
            'setting_groups' => Sh5SettingGroup::all(),
            'action' => route('device.update', ['id' => $device->id])
        ]);
    }

    public function update(Request $request, Device $device)
    {

        Validator::make($request->all(), [
            'shop_id' => [
                'required',
            ],
            'setting_group_id' => [
                'required',
            ]
        ])->validate();

        $device->update([
            'shop_id'      => $request->post('shop_id'),
            'setting_group_id' => $request->post('setting_group_id'),
        ]);

        return redirect()
            ->route('device.index')
            ->with('success', 'Касса успешно обновлена');
    }

}
