<?php

namespace App\Http\Controllers;

use App\Inventory;
use App\Shop;

class InventoryController extends ReportController
{
    public function index()
    {
        $this->data = [
            'shops' => Shop::all()
        ];

        $inventories = Inventory::with([
            'user',
            'shop'
        ])->whereDate('created_at', '>=', $this->filters['filter_date_start'])
            ->whereDate('created_at', '<=', $this->filters['filter_date_end'])
            ->orderBy('created_at');

        if ($this->filters['filter_shop']) {
            $inventories->where('shop_id', $this->filters['filter_shop']);
        }

        $this->data['inventories'] = $inventories->get();

        return view('inventory.index', $this->data, $this->filters);
    }

    public function view(Inventory $inventory)
    {
        $totals = [
            'count' => [
                'countable' => 0,
                'scalable'  => 0,
            ],
            'price' => [
                'countable' => 0,
                'scalable'  => 0,
            ]
        ];

        $inventory->load(['inventoryProducts.product.shopPrice' => function ($query) use ($inventory) {
            $query->where('shop_id', $inventory->shop_id)
                ->whereDate('date', $inventory->created_at->format('Y-m-d'));
        }]);

        $inventory->inventoryProducts->each(function ($item) use (&$totals) {
            $type = $item->product->getType();

            $totals['count'][$type] += $item->quantity;
            $totals['price'][$type] += $item->quantity * $item->product->getPrice();
        });

        return view('inventory.view', [
            'inventory' => $inventory,
            'totals'    => $totals
        ]);
    }
}
