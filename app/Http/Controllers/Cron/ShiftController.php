<?php

namespace App\Http\Controllers\Cron;

use App\Http\Controllers\Controller;
use App\Inventory;
use App\InventoryProduct;
use App\Product;
use App\Shift;
use App\UserShift;
use App\ProductShopQuantity;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class ShiftController extends Controller
{
    public static function index()
    {
        $userShifts = UserShift::whereNull('date_end')->with(['user.shopsVisible'])->get();
        $now = Carbon::now();

        foreach ($userShifts as $userShift) {
            // Получаем максимальное время сессии работника на этом магазине

            $shop = $userShift->user->shopsVisible->first(function ($item) use ($userShift) {
                return $item->id === $userShift->shop_id;
            });

            // Сравниваем даты

            $time = ($shop && $shop->pivot->time) ? $shop->pivot->time : 23;

            if ($userShift->date_start->addHours($time)->lte(Carbon::now())) {
                // Если сессия уже кончилась, то закрываем сессию

                $userShift->update(['date_end' => $now]);
            }
        }

        // Закрывае смены в которых нет сессий

        $shifts = Shift::open()->with(['userShifts' => function ($query) {
            $query->whereNull('date_end');
        }])->get();

        foreach ($shifts as $shift) {
            if ($shift->userShifts->isEmpty()) {

                // Если кассовая смена, то заполняем автоматическую инвентаризацию

                if ($shift->isCash) {
                    $inventory = Inventory::create([
                        'shift_id' => $shift->id,
                        'shop_id'  => $shift->shop_id,
                        'type'     => 'close'
                    ]);

                    // Получаем товары, которые меняли свои наличия в течение этой смены

                    $products = ProductShopQuantity::where('shop_id', $shift->shop_id)
                        ->whereDate('date', Carbon::today()->subDay()->toDateString())
                        ->whereColumn('created_at', '!=', 'updated_at')
                        ->get();

                    foreach ($products as $product) {
                        InventoryProduct::create([
                            'inventory_id' => $inventory->id                                                                                                                                                                                                                          ,
                            'product_id'   => $product->product_id,
                            'shop_id'      => $shift->shop_id,
                            'quantity'     => max(0, $product->quantity)
                        ]);
                    }
                }

                // Закрываем
                $shift->update(['date_end' => $now]);
            }
        }
    }
}
