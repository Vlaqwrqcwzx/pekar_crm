<?php

namespace App\Http\Controllers\Cron;

use App\Http\Controllers\Controller;
use App\Product;
use App\ProductShopQuantity;
use App\Shop;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class QuantityController extends Controller
{
    protected $date = '2020-06-23';

    public function index()
    {
        set_time_limit(500);
        ini_set('memory_limit', '1G');

        $this->date = date('Y-m-d');

        $this->update($this->date);
    }

    public function update($date)
    {
        $dateFormat = new Carbon($date);

        $shopQuantityToday = [];
        $shopQuantityYesterday = [];
        $shopEmptyProducts = [];

        $products = Product::getAction($dateFormat);

        $shops = Shop::all();

        foreach ($products as $product) {
            foreach ($shops as $shop) {
                // Ищем вчерашнее наличие
                $search = $product->shopQuantityYesterday->first(function ($item) use ($shop) {
                    return $item['shop_id'] === $shop->id;
                });

                if ($search) {
                    $shopQuantityYesterday[$product->id][$shop->id] = $search->quantity;
                } else {
                    $shopQuantityYesterday[$product->id][$shop->id] = 0;
                }

                // Если за сегодня нет записи по наличию
                // записываем вчерашние остатки за сегодня для всех магазинов

                $search = $product->shopQuantityToday->first(function ($item) use ($shop) {
                    return $item['shop_id'] === $shop->id;
                });

                if (!$search) {
                    ProductShopQuantity::create([
                        'product_id' => $product->id,
                        'shop_id'    => $shop->id,
                        'date'       => $date,
                        'quantity'   => max(0, $shopQuantityYesterday[$product->id][$shop->id])
                    ]);
                }
            }

            // Заказы за сегодня
            foreach ($product->shopOrders as $order) {
                $shopQuantityToday[$product->id][$order->shop_id][$date][$order->date->format('His')][] = [
                    'type'     => 'sub',
                    'quantity' => $order->quantity
                ];
            }

            // Выпуски за сегодня
            foreach ($product->shiftProducts as $shiftProduct) {
                // Если принятый выпуск кассиром
                $shopQuantityToday[$product->id][$shiftProduct->shop_id][$date][$shiftProduct->updated_at->format('His')][] = [
                    'type'     => 'add',
                    'quantity' => $shiftProduct->accept
                ];
            }

            // Все списания за сегодня
            foreach ($product->rejectProducts as $rejectProduct) {
                // Если списание было в магазине
                if ($rejectProduct->isShop) {
                    $shopQuantityToday[$product->id][$rejectProduct->shop_id][$date][$rejectProduct->created_at->format('His')][] = [
                        'type'     => 'sub',
                        'quantity' => $rejectProduct->quantity
                    ];
                }
            }

            // Все перемещения за сегодня
            foreach ($product->movementProducts as $movementProduct) {
                // Берем в расчет только принятые перемещения
                if ($movementProduct->isAccepted) {
                    // Перемещение от, минусуем у них наличие
                    // Если от кассира к кассиру
                    if ($movementProduct->isCash) {
                        $shopQuantityToday[$product->id][$movementProduct->from_shop_id][$date][$movementProduct->updated_at->format('His')][] = [
                            'type'     => 'sub',
                            'quantity' => $movementProduct->complete
                        ];
                    }

                    // Магазин который принял товар, то ему плюсуем наличие
                    $shopQuantityToday[$product->id][$movementProduct->to_shop_id][$date][$movementProduct->updated_at->format('His')][] = [
                        'type'     => 'add',
                        'quantity' => $movementProduct->complete
                    ];
                }

            }

            // Инвентаризация
            foreach ($product->inventoryProducts as $inventoryProduct) {
                $shopQuantityToday[$product->id][$inventoryProduct->shop_id][$date][$inventoryProduct->created_at->format('His')][] = [
                    'type'     => 'set',
                    'quantity' => $inventoryProduct->quantity,
                    'model'    => $inventoryProduct
                ];
            }

            // Получаем товары, по которым сегодня не было движух
            foreach ($shops as $shop) {
                if (!isset($shopQuantityToday[$product->id][$shop->id])) {
                    $shopEmptyProducts[$shop->id][] = $product->id;
                }
            }
        }

        foreach ($shopEmptyProducts as $shop_id => $products) {
            ProductShopQuantity::where('shop_id', $shop_id)
                ->whereIn('product_id', $products)
                ->update([
                    'updated_at' => DB::raw('created_at')
                ]);
        }

        foreach ($shopQuantityToday as $product_id => $shops) {
            foreach ($shops as $shop_id => $dates) {
                foreach ($dates as $day => $times) {

                    // Начальное наличие
                    $quantity = isset($shopQuantityYesterday[$product_id][$shop_id])
                        ? max(0, $shopQuantityYesterday[$product_id][$shop_id])
                        : 0;

                    // Сортируем массив по ключам, в ключах как раз время
                    ksort($times);

                    foreach ($times as $items) {
                        foreach ($items as $item) {
                            if ($item['type'] == 'sub') {
                                $quantity -= $item['quantity'];
                            } elseif ($item['type'] == 'add') {
                                $quantity += $item['quantity'];
                            } elseif ($item['type'] == 'set') {
                                // Была инвентаризация, запишем на всякий случай кол-во ДО
                                $item['model']->update([
                                    'before' => $quantity
                                ]);

                                $quantity = $item['quantity'];
                            }
                        }
                    }

                    ProductShopQuantity::where([
                        'product_id' => $product_id,
                        'shop_id'    => $shop_id,
                        'date'       => $day
                    ])->update([
                        'quantity' => round($quantity, 3)
                    ]);
                }
            }
        }

        // Если это не текущая дата, то считаем остатки на след. день
        if ($dateFormat->lt(Carbon::today())) {
            $this->update($dateFormat->addDay()->toDateString());
        }
    }
}
