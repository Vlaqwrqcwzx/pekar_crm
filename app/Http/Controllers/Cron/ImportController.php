<?php

namespace App\Http\Controllers\Cron;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Import\DreamKasController;
use App\Import;

class ImportController extends Controller
{
    public static function index()
    {
        set_time_limit(500);

        $imports = Import::where('status', 1)->whereNotNull('token')->get();

        $imports->each(function ($item) {
            $dk = new DreamKasController($item->token, $item->id);
            $dk->run();
        });
    }
}
