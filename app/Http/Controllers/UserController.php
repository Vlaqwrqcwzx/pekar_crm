<?php

namespace App\Http\Controllers;

use App\Category;
use App\ReportSetting;
use App\Shop;
use App\User;
use App\UserGroup;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public const PASSWORD = 'password';

    public function index()
    {
        return view('user.index', [
            'users' => User::with('group')->get()
        ]);
    }

    public function create()
    {
        return view('user.form', [
            'user'         => '',
            'groups'       => UserGroup::all(),
            'shops'        => Shop::all(),
            'shopsVisible' => old('shops_visible', []),
            'shopsStock'   => old('shops_stock', []),
            'action'       => route('user.store')
        ]);
    }

    public function store(Request $request)
    {
        Validator::make($request->all(), [
            'name' => [
                'required',
                'max:255',
                'unique:users'
            ],
            'code' => [
                'required',
                'min:6',
                'unique:users'
            ]
        ])->validate();

        $user = User::create([
            'group_id' => $request->post('group_id'),
            'name'     => $request->post('name'),
            'email'    => sprintf('%s@mail.ru', $request->post('code')),
            'code'     => $request->post('code'),
            'password' => Hash::make(self::PASSWORD)
        ]);

        $this->addShops($user);

        return redirect()
            ->route('user.index')
            ->with('success', 'Пользователь успешно добавлен');
    }

    public function edit(User $user)
    {
        $shopsTime = [];

        $user->shopsVisible()->each(function ($item) use (&$shopsTime) {
            $shopsTime[$item->pivot->shop_id] = $item->pivot->time;
        });

        return view('user.form', [
            'user'         => $user,
            'groups'       => UserGroup::all(),
            'shops'        => Shop::all(),
            'shopsVisible' => $user->shopsVisible()->pluck('shop_id')->toArray(),
            'shopsTime'    => $shopsTime,
            'shopsStock'   => $user->shopsStock()->pluck('shop_id')->toArray(),
            'action'       => route('user.update', ['id' => $user->id])
        ]);
    }

    public function update(Request $request, User $user)
    {
        Validator::make($request->all(), [
            'name' => [
                'required',
                'max:255',
                Rule::unique('users')->ignore($user->id),
            ],
            'code' => [
                'required',
                'min:6',
                Rule::unique('users')->ignore($user->id),
            ]
        ])->validate();

        $user->group_id = $request->post('group_id');
        $user->name = $request->post('name');
        $user->code = $request->post('code');
        $user->password = Hash::make(self::PASSWORD);
        $user->save();

        $this->addShops($user);

        return redirect()
            ->route('user.index')
            ->with('success', 'Пользователь успешно обновлен');
    }

    private function addShops($user)
    {
        DB::table('user_shop')->where('user_id', $user->id)->delete();

        if (request()->has('shops_visible')) {
            foreach (request()->post('shops_visible') as $shop_id) {
                DB::table('user_shop')->insert([
                    'user_id' => $user->id,
                    'shop_id' => $shop_id,
                    'type'    => 'visible',
                    'time'    => request()->input('shops_time.' . $shop_id)
                ]);
            }
        }

        if (request()->has('shops_stock')) {
            foreach (request()->post('shops_stock') as $shop_id) {
                DB::table('user_shop')->insert([
                    'user_id' => $user->id,
                    'shop_id' => $shop_id,
                    'type'    => 'stock'
                ]);
            }
        }
    }

    public function reportSettings($report)
    {
        $settings = Auth::user()->reportSettings($report);

        return view('user.report-settings', [
            'report'         => $report,
            'categories'     => Category::all()->prepend((object)[
                'id'   => 0,
                'name' => 'Без категории'
            ]),
            'userCategories' => isset($settings->settings['categories']) ? $settings->settings['categories'] : []
        ]);
    }

    public function saveReportSettings()
    {
        ReportSetting::updateOrCreate([
            'user_id' => Auth::user()->id,
            'report'  => request()->report
        ], [
            'settings' => request()->only(['categories'])
        ]);

        return redirect()->route('report.' . request()->report);
    }
}
