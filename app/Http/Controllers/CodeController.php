<?php

namespace App\Http\Controllers;

use App\Inventory;
use App\Movement;
use App\ProductShopOrder;
use App\ProductShopQuantity;
use App\Reject;
use App\ShiftProduct;
use App\UserShift;
use Carbon\Carbon;
use Illuminate\Http\Request;

class CodeController extends Controller
{
    public function index()
    {
        return view('code.index');
    }

    public function update()
    {
        if (request()->code) {
            $error = 'Код не найден';

            $shiftProduct = ShiftProduct::where(function ($query) {
                $query->where('code1', request()->code)->orWhere('code2', request()->code)->orWhere('code3', request()->code);
            })
                ->whereDate('updated_at', Carbon::now())
                ->with('shift')
                ->get()
                ->first();

            if ($shiftProduct) {
                // Значит пекарь хочет отменить выпуск
                if ($shiftProduct->code1 == request()->code) {
                    // Если выпуск еще не трогали
                    if ($shiftProduct->toAccept() == $shiftProduct->complete) {
                        $shiftProduct->update([
                            'status'   => 1,
                            'complete' => $shiftProduct->complete - $shiftProduct->toAccept()
                        ]);

                        $shiftProduct->histories()->where('status', '>', 1)->delete();

                        return redirect()
                            ->route('code.index')
                            ->with('success', 'Код найден. Выпуск отменен');
                    } else {
                        $error = 'Выпуск нельзя отменить. Частично принят';
                    }
                }

                // Значит кассир хочет отменить принятую продукцию
                if ($shiftProduct->code2 == request()->code) {
                    // Если частично отправили в брак
                    if ($shiftProduct->reject1 > 0 || $shiftProduct->reject2 > 0) {
                        $error = 'Принятие нельзя отменить. Частично в браке или бракераже';
                    } else {
                        $shiftProduct->update([
                            'accept'   => 0
                        ]);

                        return redirect()
                            ->route('code.index')
                            ->with('success', 'Код найден. Принятие отменено');
                    }
                }

                // Ложный выпуск
                if ($shiftProduct->code3 == request()->code) {
                    $shiftProduct->update([
                        'complete' => $shiftProduct->complete - $shiftProduct->wrong,
                        'wrong'    => 0,
                        'code3'    => ''
                    ]);

                    return redirect()
                        ->route('code.index')
                        ->with('success', 'Код найден. Ложный выпуск');
                }
            }

            // Ищем все браки и тд. за сегодня

            $reject = Reject::with('rejectProducts')
                ->whereDate('created_at', Carbon::now())
                ->where('code', request()->code)
                ->get()
                ->first();

            if ($reject) {
                $reject->rejectProducts->each(function ($item) use ($reject) {
                    // Если это кухоное списание, то возвращаем кол-во обратно

                    if ($item->shift_product_id) {
                        $shiftProduct = ShiftProduct::find($item->shift_product_id);
                        if ($shiftProduct) {
                            $shiftProduct->update([
                                'reject' . $reject->type => $shiftProduct->{'reject' . $reject->type} - $item->quantity
                            ]);
                        }
                    }

                    $item->delete();
                });

                $reject->delete();

                return redirect()
                    ->route('code.index')
                    ->with('success', sprintf('Код найден. %s отменен', $reject->typeName));
            }

            // Ищем все перемещения по коду за сегодня

            $movement = Movement::with('movementProducts')
                ->whereDate('created_at', Carbon::now())
                ->where('code', request()->code)
                ->get()
                ->first();

            if ($movement) {
                $movement->movementProducts->each(function ($item) {
                    // Значит было перемещение с кухни
                    // Вернем это кол-во обратно
                    if ($item->shift_product_id) {
                        $shiftProduct = ShiftProduct::find($item->shift_product_id);
                        if ($shiftProduct) {
                            $shiftProduct->update([
                                'move' => $shiftProduct->move - $item->quantity
                            ]);
                        }
                    }

                    $item->delete();
                });

                $movement->delete();

                return redirect()
                    ->route('code.index')
                    ->with('success', 'Код найден. Перемещение отменено');
            }

            // Ищем сличилки при закрытии на сегодня

            $inventory = Inventory::where('code', request()->code)
                ->where('type', 'close')
                ->whereDate('created_at', Carbon::now())
                ->get()
                ->first();

            if ($inventory) {
                // Есть ли сличилки после этой
                $nextInventory = Inventory::where('shop_id', $inventory->shop_id)
                    ->where('created_at', '>', $inventory->created_at->toDateTimeString())
                    ->count();

                if ($nextInventory) {
                    $error = 'Сличительную ведомость нельзя закрыть. Уже открыта новая смена';
                } else {
                    $inventory->inventoryProducts()->delete();
                    $inventory->delete();
                    $inventory->shift()->update([
                        'date_end' => null
                    ]);
                    $inventory->shift->userShifts()->update([
                        'date_end' => null
                    ]);

                    return redirect()
                        ->route('code.index')
                        ->with('success', 'Код найден. Сличительную ведомость отменена. Смена открыта заново');
                }
            }
        }

        return redirect()
            ->route('code.index')
            ->with('warning', $error);
    }

}
