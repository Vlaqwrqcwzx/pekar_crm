<?php

namespace App\Http\Controllers;

use App\Franchise;
use App\Inventory;
use App\ProductShopOrder;
use App\Reject;
use App\Shift;
use App\ShiftProduct;
use App\MovementProduct;
use App\Shop;
use App\Category;
use App\Product;
use App\User;
use App\UserShift;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Jenssegers\Agent\Agent;

class ReportController extends Controller
{
    protected $data = [];
    protected $hours = [];
    protected $hoursArray = [];
    protected $filters;

    protected $shops;

    public function __construct()
    {
        $this->hours = array_map(function () {
            return 0;
        }, range(0, 23));

        $this->hoursArray = array_map(function () {
            return [];
        }, range(0, 23));

        $this->filters = [
            'filter_date_start' => request()->get('filter_date_start', date('Y-m-d')),
            'filter_date_end'   => request()->get('filter_date_end', date('Y-m-d')),
            'filter_franchise'  => request()->get('filter_franchise', 0),
            'filter_shop'       => request()->get('filter_shop', 0),
            'filter_category'   => request()->get('filter_category', 0),
            'filter_type'       => request()->get('filter_type', 0),
            'filter_shift'      => request()->get('filter_shift', 0),
            'filter_hidden'     => request()->get('filter_hidden', 1),
            'filter_sort'       => request()->get('filter_sort', 'count'),
        ];
    }

    public function order()
    {
        $shops = Shop::orderBy('name')->get();

        $this->data = [
            'products'   => [],
            'shops'      => $shops,
            'hours'      => $this->hours,
            'hourCounts' => collect($this->hoursArray)->map(function () {
                return [
                    'countable' => 0,
                    'scalable'  => 0
                ];
            })->toArray(),
            'hourTotals' => $this->hours
        ];

        $this->filters['filter_date'] = request()->get('filter_date', date('Y-m-d'));

        if ($this->filters['filter_shop']) {
            $shop = $shops->first(function ($item) {
                return $item->id == $this->filters['filter_shop'];
            });
        }

        if (empty($shop)) {
            $shop = $shops->first();
        }

        if ($shop) {
            $this->filters['filter_shop'] = $shop->id;

            $categoryProducts = Product::with(['category',
                'shopOrders' => function ($query) {
                    $query->whereDate('date', $this->filters['filter_date'])
                        ->where('shop_id', $this->filters['filter_shop']);
                }
            ])->orderBy('name')
                ->get()
                ->groupBy('category.id')
                ->sortBy(function ($item) {
                    return $item->first()->getSort();
                });

            foreach ($categoryProducts as $products) {
                foreach ($products as $product) {
                    $orders = $this->hours;
                    $count = 0;

                    foreach ($product->shopOrders as $order) {
                        $hour = $order->date->format('G');
                        $quantity = $order->quantity;

                        $orders[$hour] += $quantity;

                        $this->data['hourCounts'][$hour][$product->getType()] += $quantity;

                        if ($product->isScalable()) {
                            $this->data['hourTotals'][$hour] += $order->price / 100;
                        } else {
                            $this->data['hourTotals'][$hour] += $quantity * ($order->price / 100);
                        }

                        $count += $quantity;
                    }

                    $this->data['products'][] = [
                        'name'           => $product->getName(),
                        'category_color' => $product->getColor(),
                        'time'           => $product->time,
                        'top'            => $product->top,
                        'orders'         => $orders,
                        'count'          => $count
                    ];
                }
            }

            if ($this->filters['filter_hidden']) {
                $this->data['products'] = array_filter($this->data['products'], function ($item) {
                    return $item['count'] ? true : false;
                });
            }

            foreach ($this->data['hourTotals'] as &$total) {
                $total = round($total / 1000, 3);
            }
        }

        return view('report.order', $this->data, $this->filters);
    }

    public function shift()
    {
        $shops = Shop::all();

        $this->data = array_merge($this->data, [
            'products' => [],
            'shops'    => $shops,
            'hours'    => $this->hours,
            'totals'   => $this->hours,
            'counts'   => $this->hours
        ]);

        $this->filters['filter_date'] = request()->get('filter_date', date('Y-m-d'));

        if ($this->filters['filter_shop']) {
            $shop = $shops->first(function ($item) {
                return $item->id == $this->filters['filter_shop'];
            });
        }

        if (empty($shop)) {
            $shop = $shops->first();
        }

        if ($shop) {
            $this->filters['filter_shop'] = $shop->id;

            $shop->load(['shifts' => function ($query) {
                $query->where('type', '!=', 'cash')
                    ->whereDate('date_start', '<=', $this->filters['filter_date'])
                    ->where(function ($query) {
                        $query->where('date_end', '>=', $this->filters['filter_date'])->orWhereNull('date_end');
                    });
            }]);

            $this->data['shifts'] = $shop->shifts;

            if ($this->filters['filter_shift']) {
                $shift = $shop->shifts->first(function ($item) {
                    return $item->id == $this->filters['filter_shift'];
                });
            }

            if (empty($shift)) {
                $shift = $shop->shifts->first();
            }

            if ($shift) {
                $this->filters['filter_shift'] = $shift->id;

                $categoryProducts = Product::with([
                    'category',
                    'shiftProducts' => function ($query) {
                        $query->where('shift_id', $this->filters['filter_shift']);
                    }
                ])->orderBy('name')
                    ->get()
                    ->groupBy('category.id')
                    ->sortBy(function ($item) {
                        return $item->first()->getSort();
                    });

                foreach ($categoryProducts as $products) {
                    foreach ($products as $product) {
                        $rows = $this->hours;
                        $total = 0;

                        foreach ($product->shiftProducts as $shiftProduct) {
                            $hour = date('G', strtotime($shiftProduct->created_at));

                            $rows[$hour] += $shiftProduct->accept;

                            $this->data['counts'][$hour] += $shiftProduct->accept;
                            $this->data['totals'][$hour] += $shiftProduct->accept * ($product->price / 100);

                            $total += $shiftProduct->accept;
                        }

                        $this->data['products'][] = [
                            'name'           => $product->name,
                            'category_color' => $product->getColor(),
                            'time'           => $product->time,
                            'top'            => $product->top,
                            'rows'           => $rows,
                            'total'          => $total
                        ];
                    }
                }

                if ($this->filters['filter_hidden']) {
                    $this->data['products'] = array_filter($this->data['products'], function ($item) {
                        return $item['total'] ? true : false;
                    });
                }
            }
        }

        return view('report.shift', $this->data, $this->filters);
    }

    public function shiftUser()
    {
        $userShift = UserShift::where(function ($query) {
            $query->whereDate('date_start', '<=', $this->filters['filter_date_end'])
                ->where(function ($query) {
                    $query->whereDate('date_end', '>=', $this->filters['filter_date_start'])
                        ->orWhereNull('date_end');
                });
        })->with(['user.group', 'shop', 'shift']);

        if ($this->filters['filter_shop']) {
            $userShift->where('shop_id', $this->filters['filter_shop']);
        }

        $this->data = [
            'user_shifts' => $userShift->get(),
            'shops'       => Shop::all()
        ];

        return view('report.shift-user', $this->data, $this->filters);
    }

    public function shiftPlan()
    {
        $shops = Shop::all();

        $this->data = [
            'products' => [],
            'shops'    => $shops,
            'hours'    => $this->hours,
            'totals'   => $this->hours,
            'counts'   => $this->hours
        ];

        $this->filters['filter_date'] = request()->get('filter_date', date('Y-m-d'));

        if ($this->filters['filter_shop']) {
            $shop = $shops->first(function ($item) {
                return $item->id == $this->filters['filter_shop'];
            });
        }

        if (empty($shop)) {
            $shop = $shops->first();
        }

        if ($shop) {
            $this->filters['filter_shop'] = $shop->id;

            $categoryProducts = Product::with([
                'category',
                'shiftProducts' => function($query) use ($shop) {
                    $query->whereDate('updated_at', $this->filters['filter_date'])
                        ->where('shop_id', $shop->id)
                        ->with([
                            'histories' => function ($query) {
                                $query->orderByDesc('created_at');
                            }
                        ]);
            }])->orderBy('name')
                ->get()
                ->groupBy('category.id')
                ->sortBy(function ($item) {
                    return $item->first()->getSort();
                });

            foreach ($categoryProducts as $products) {
                foreach ($products as $product) {
                    $total = 0;

                    $rows = collect($this->hoursArray)->map(function ($item) {
                        return [
                            'plan'  => 0,
                            'shift' => 0,
                            'total' => 0
                        ];
                    })->toArray();

                    foreach ($product->shiftProducts as $shiftProduct) {
                        $hourPlan = $shiftProduct->hour;
                        $firstHistory = $shiftProduct->histories->first(function ($item) {
                            return $item->status === 2;
                        });

                        // Если по плану
                        if ($shiftProduct->plan_id) {
                            $rows[$hourPlan]['plan'] += $shiftProduct->quantity;
                        }

                        if ($firstHistory) {
                            $hourShift = $firstHistory->created_at->format('G');

                            $rows[$hourShift]['shift'] += $shiftProduct->complete;
                            $this->data['counts'][$hourShift] += $shiftProduct->complete;
                            $this->data['totals'][$hourShift] += $shiftProduct->complete * ($product->price / 100);

                            $total += $shiftProduct->complete;
                        }
                    }

                    $this->data['products'][] = [
                        'name'           => $product->name,
                        'category_color' => $product->getColor(),
                        'time'           => $product->time,
                        'rows'           => $rows,
                        'total'          => $total
                    ];
                }
            }

            if ($this->filters['filter_hidden']) {
                $this->data['products'] = array_filter($this->data['products'], function ($item) {
                    return $item['total'] ? true : false;
                });
            }
        }

        return view('report.shift-plan', $this->data, $this->filters);
    }

    public function reject()
    {
        $this->data = [
            'products' => [],
            'shops'    => Shop::all(),
            'hours'    => $this->hours,
            'totals'   => $this->hours,
            'counts'   => $this->hours
        ];

        $categoryProducts = Product::with([
            'category',
            'rejectProducts' => function ($query) {
                $query->whereDate('created_at', '>=', $this->filters['filter_date_start']);
                $query->whereDate('created_at', '<=', $this->filters['filter_date_end']);

                if ($this->filters['filter_shop']) {
                    $query->where('shop_id', $this->filters['filter_shop']);
                }
            }
        ])->orderBy('name')
            ->get()
            ->groupBy('category.id')
            ->sortBy(function ($item) {
                return $item->first()->getSort();
            });

        foreach ($categoryProducts as $products) {
            foreach ($products as $product) {
                $rows = $this->hours;
                $total = 0;

                foreach ($product->rejectProducts as $rejectProduct) {
                    $hour = date('G', strtotime($rejectProduct->created_at));

                    $rows[$hour] += $rejectProduct->quantity;

                    $this->data['counts'][$hour] += $rejectProduct->quantity;
                    $this->data['totals'][$hour] += $rejectProduct->quantity * ($product->price / 100);

                    $total += $rejectProduct->quantity;
                }

                $this->data['products'][] = [
                    'name'           => $product->name,
                    'category_color' => $product->getColor(),
                    'time'           => $product->time,
                    'top'            => $product->top,
                    'rows'           => $rows,
                    'total'          => $total
                ];
            }
        }

        if ($this->filters['filter_hidden']) {
            $this->data['products'] = array_filter($this->data['products'], function ($item) {
                return $item['total'] ? true : false;
            });
        }

        return view('report.reject', $this->data, $this->filters);
    }

    public function quality()
    {
        $shops = Shop::all();

        $this->filters['filter_date'] = request()->get('filter_date', date('Y-m-d'));

        $this->data = [
            'shops'    => $shops,
            'products' => [],
            'totals' => [
                'time_start_diff'    => 0,
                'time_complete_diff' => 0,
                'time_moment'        => 0
            ]
        ];

        if ($this->filters['filter_shop']) {
            $shop = $shops->first(function ($item) {
                return $item->id == $this->filters['filter_shop'];
            });
        }

        if (empty($shop)) {
            $shop = $shops->first();
        }

        if ($shop) {
            $products = ShiftProduct::with([
                'product.category',
                'shift.shop',
                'histories' => function ($query) {
                    $query->orderByDesc('created_at');
                }
            ])
                ->where('shop_id', $shop->id)
                ->whereDate('created_at', $this->filters['filter_date'])
                ->whereIn('status', [2, 3])
                ->orderBy('hour')
                ->get();


            $this->filters['filter_shop'] = $shop->id;
            // Час в секундах без одной секунды
            $seconds = (60 * 60) - 1;

            foreach ($products as $product) {
                $timeStartFact = $product->histories->first(function ($item) {
                    return $item['status'] === 1;
                });

                $timeCompleteFact = $product->histories->first(function ($item) {
                    return $item['status'] === 2;
                });

                if ($timeStartFact && $timeCompleteFact) {
                    $timeCompletePlan = Carbon::createFromTime($product->hour, 0, 0);

                    if ($product->hour - ceil($product->product->time / 60) < 0) {
                        $timeStartPlan = Carbon::createFromTime(0, 0, 0);
                    } else {
                        $timeStartPlan = Carbon::createFromTime($product->hour, 30)->subMinutes($product->product->time)->setMinutes(0)->setSeconds(0);
                    }

                    $filterDate = Carbon::createFromFormat('Y-m-d', $this->filters['filter_date']);
                    $timeStartPlan->setDateFrom($filterDate);
                    $timeCompletePlan->setDateFrom($filterDate);

                    // Расхождение время начала
                    $timeStartDiff = '00:00:00';

                    // Если фактический час начала не совпадает с планом
                    if ($timeStartPlan->format('H') != $timeStartFact->created_at->format('H')) {
                        // Если время по плану больше фактического, то разницу берем от нее
                        if ($timeStartPlan->gt($timeStartFact)) {
                            $timeStartDiff = $timeStartPlan->diff($timeStartFact->created_at)->format('%H:%I:%S');

                            $this->data['totals']['time_start_diff'] += $timeStartPlan->diffInSeconds($timeStartFact->created_at);
                        } else {
                            $timeStartDiff = $timeStartPlan->copy()->addHour()->diff($timeStartFact->created_at)->format('%H:%I:%S');

                            $this->data['totals']['time_start_diff'] += $timeStartPlan->copy()->addHour()->diffInSeconds($timeStartFact->created_at);
                        }
                    }

                    // Расхождение время конца
                    $timeCompleteDiff = '00:00:00';

                    if ($timeCompletePlan->format('H') != $timeCompleteFact->created_at->format('H')) {
                        // Если время по плану больше фактического, то разницу берем от нее
                        if ($timeCompletePlan->gt($timeCompleteFact)) {
                            $timeCompleteDiff = $timeCompletePlan->diff($timeCompleteFact->created_at)->format('%H:%I:%S');

                            $this->data['totals']['time_complete_diff'] += $timeCompletePlan->diffInSeconds($timeCompleteFact->created_at);
                        } else {
                            $timeCompleteDiff = $timeCompletePlan->copy()->addHour()->addMinutes($product->product->time)->diff($timeCompleteFact->created_at)->format('%H:%I:%S');

                            $this->data['totals']['time_complete_diff'] += $timeCompletePlan->copy()->addHour()->addMinutes($product->product->time)->diffInSeconds($timeCompleteFact->created_at);
                        }
                    }

                    if ($product->product->time) {
                        $percent = floor(($timeStartFact->created_at->diffInSeconds($timeCompleteFact->created_at) / ($product->product->time * 60)) * 100) . ' ';

                        if ($percent <= 75) {
                            $timeMoment = 'Да';

                            $this->data['totals']['time_moment']++;
                        } else {
                            $timeMoment = 'Нет';
                        }
                    } else {
                        $timeMoment = '-';
                    }

                    $this->data['products'][] = [
                        'rows'           => [
                            'product'                 => $product->product->getName(),
                            'time_complete_plan_from' => $timeCompletePlan->format('H:i:s'),
                            'time_complete_plan_to'   => $timeCompletePlan->copy()->addSeconds($seconds)->format('H:i:s'),
                            'time_start_need'         => $timeCompletePlan->copy()->addMinutes(30)->subMinutes($product->product->time)->format('H:i:s'),
                            'time_start_plan_from'    => $timeStartPlan->format('H:i:s'),
                            'time_start_plan_to'      => $timeStartPlan->copy()->addSeconds($seconds)->format('H:i:s'),
                            'time_start_fact'         => $timeStartFact->created_at->format('H:i:s'),
                            'time_start_diff'         => $timeStartDiff,
                            'time_complete_min'       => $timeStartPlan->copy()->addMinutes($product->product->time)->format('H:i:s'),
                            'time_complete_max'       => $timeStartPlan->copy()->addSeconds($seconds)->addMinutes($product->product->time)->format('H:i:s'),
                            'time_complete_fact'      => $timeCompleteFact->created_at->format('H:i:s'),
                            'time_complete_diff'      => $timeCompleteDiff,
                            'time_product_plan'       => Carbon::createFromTime(0, 0, 0)->addSeconds($product->product->time)->format('H:i:s'),
                            'time_product_fact'       => $timeStartFact->created_at->diff($timeCompleteFact->created_at)->format('%H:%I:%S'),
                            'time_moment'             => $timeMoment
                        ],
                        'color' => $product->product->getColor()
                    ];
                }
            }

            $this->data['products'] = collect($this->data['products'])->sortBy('rows.time_complete_fact')->toArray();

            $now = Carbon::now();

            $this->data['totals']['time_start_diff'] = $now->diff($now->copy()->addSeconds($this->data['totals']['time_start_diff']))->format('%d %H:%I:%S');
            $this->data['totals']['time_complete_diff'] = $now->diff($now->copy()->addSeconds($this->data['totals']['time_complete_diff']))->format('%d %H:%I:%S');
        }

        return view('report.quality', $this->data, $this->filters);
    }

    public function inventory()
    {
        $shops = Shop::all();

        $this->data = [
            'products' => [],
            'shops'    => $shops,
            'hours'    => $this->hours
        ];

        $this->filters['filter_date'] = request()->get('filter_date', date('Y-m-d'));

        if ($this->filters['filter_shop']) {
            $shop = $shops->first(function ($item) {
                return $item->id == $this->filters['filter_shop'];
            });
        }

        if (empty($shop)) {
            $shop = $shops->first();
        }

        if ($shop) {
            $this->filters['filter_shop'] = $shop->id;

            $shop->load(['shifts' => function ($query) {
                $query->whereDate('date_start', '<=', $this->filters['filter_date'])
                    ->with('shop')
                    ->where(function ($query) {
                        $query->where('date_end', '>=', $this->filters['filter_date'])->orWhereNull('date_end');
                    });
            }]);

            // В фильтре показываем только кассовые смены
            $this->data['shifts'] = $shop->shifts->filter(function ($item) {
                return $item->isCash;
            });

            if ($this->filters['filter_shift']) {
                $shift = $shop->shifts->first(function ($item) {
                    return $item->id == $this->filters['filter_shift'];
                });
            }

            if (empty($shift)) {
                $shift = $shop->shifts->first();
            }

            // Если у магазина есть какие то смены попадающие в эту дату

            if ($shift) {
                $this->filters['filter_shift'] = $shift->id;

                $categoryProducts = Product::getShiftAction($shift)
                    ->groupBy('category.id')
                    ->sortBy(function ($item) {
                        return $item->first()->getSort();
                    });

                foreach ($categoryProducts as $products) {
                    foreach ($products as $product) {
                        $price = $product->getPrice();

                        $inventoryOpen = [];
                        $inventoryClose = [];

                        // Получаем открытую и закрытую инвенторизацию
                        foreach ($product->inventoryProducts as $inventoryProduct) {
                            if ($inventoryProduct->inventory->type == Inventory::OPEN) {
                                $inventoryOpen = $inventoryProduct;
                            }

                            if ($inventoryProduct->inventory->type == Inventory::CLOSE) {
                                $inventoryClose = $inventoryProduct;
                            }
                        }

                        $shiftProducts = $product->shiftProducts->sum(function ($item) {
                            return (float)($item->accept + $item->reject2);
                        });

                        $movementProductsOut = $product->movementProducts->sum(function ($item) use ($shift) {
                            if ($item->from_shop_id == $shift->shop_id) {
                                return (float)$item->complete;
                            } else {
                                return 0;
                            }
                        });

                        $movementProductsIn = $product->movementProducts->sum(function ($item) use ($shift) {
                            if ($item->to_shop_id == $shift->shop_id) {
                                return (float)$item->complete;
                            } else {
                                return 0;
                            }
                        });

                        $this->data['products'][$product->id] = [
                            'name'         => $product->getName(),
                            'color'        => $product->getColor(),
                            'start'        => [
                                'quantity' => $inventoryOpen ? $inventoryOpen->quantity : '-',
                                'total'    => $inventoryOpen ? ($inventoryOpen->quantity * $price) : '-'
                            ],
                            'shift'        => [
                                'quantity' => $shiftProducts,
                                'total'    => $shiftProducts * $price
                            ],
                            'movement_out' => [
                                'quantity' => $movementProductsOut,
                                'total'    => $movementProductsOut * $price
                            ],
                            'movement_in'  => [
                                'quantity' => $movementProductsIn,
                                'total'    => $movementProductsIn * $price
                            ],
                            'order'        => [
                                'quantity' => $product->shopOrders->sum('quantity'),
                                'total'    => $product->shopOrders->sum('quantity') * $price
                            ],
                            'end'          => [
                                'quantity' => $inventoryClose ? $inventoryClose->quantity : '-',
                                'total'    => $inventoryClose ? ($inventoryClose->quantity * $price) : '-'
                            ],
                            'result'       => [
                                'quantity' => 0,
                                'total'    => 0
                            ]
                        ];

                        foreach (Reject::TYPES as $id => $type) {
                            $rejects = $product->rejectProducts->filter(function ($item) use ($id) {
                                // Исключаем брак при приемке
                                if ($item->reject->type == 1 && $item->isAccept) {
                                    return false;
                                }

                                return $item->reject->type == $id;
                            });

                            $this->data['products'][$product->id]['reject-' . $id] = [
                                'quantity' => $rejects->sum('quantity'),
                                'total'    => $rejects->sum('quantity') * $price
                            ];
                        }

                        foreach ($this->data['products'][$product->id] as $key => $value) {
                            if (in_array($key, ['start', 'shift', 'movement_in'])) {
                                $this->data['products'][$product->id]['result']['quantity'] -= (float)$value['quantity'];
                                $this->data['products'][$product->id]['result']['total'] -= (float)$value['total'];
                            }

                            if (in_array($key, ['order', 'movement_out', 'reject-1', 'reject-2', 'reject-3', 'end'])) {
                                $this->data['products'][$product->id]['result']['quantity'] += (float)$value['quantity'];
                                $this->data['products'][$product->id]['result']['total'] += (float)$value['total'];
                            }
                        }
                    }
                }
            }

            foreach ($this->data['products'] as &$product) {
                $product['result']['quantity'] = round($product['result']['quantity'], 3);
                $product['result']['total'] = round($product['result']['total'], 2);
            }

            if ($this->filters['filter_hidden']) {
                $this->data['products'] = array_filter($this->data['products'], function ($item) {
                    return $item['result']['quantity'];
                });
            }
        }

        return view('report.inventory', $this->data, $this->filters);
    }

    public function inventory2()
    {
        $shops = Shop::all();

        $this->data = [
            'products' => [],
            'shops'    => $shops
        ];

        $this->filters['filter_date'] = request()->get('filter_date', date('Y-m-d'));

        if ($this->filters['filter_shop']) {
            $shop = $shops->first(function ($item) {
                return $item->id == $this->filters['filter_shop'];
            });
        }

        if (empty($shop)) {
            $shop = $shops->first();
        }

        if ($shop) {
            $this->filters['filter_shop'] = $shop->id;

            $shop->load(['shifts' => function ($query) {
                $query->whereDate('date_start', '<=', $this->filters['filter_date'])
                    ->where(function ($query) {
                        $query->where('date_end', '>=', $this->filters['filter_date'])->orWhereNull('date_end');
                    });
            }]);

            // В фильтре показываем только кассовые смены
            $this->data['shifts'] = $shop->shifts->filter(function ($item) {
                return $item->isCash;
            });

            if ($this->filters['filter_shift']) {
                $shift = $shop->shifts->first(function ($item) {
                    return $item->id == $this->filters['filter_shift'];
                });
            }

            if (empty($shift)) {
                $shift = $shop->shifts->first();
            }

            // Если у магазина есть какие то смены попадающие в эту дату

            if ($shift) {
                $this->filters['filter_shift'] = $shift->id;

                $categoryProducts = Product::with([
                    'category',
                    'shopPrice'             => function ($query) {
                        $query->where('shop_id', $this->filters['filter_shop'])
                            ->whereDate('date', $this->filters['filter_date']);
                    },
                    'shopQuantity'          => function ($query) {
                        $query->whereDate('created_at', '<=', $this->filters['filter_date'])
                            ->where('shop_id', $this->filters['filter_shop'])
                            ->orderBy('created_at');
                    },
                    'shiftProducts'         => function ($query) use ($shop) {
                        $query->whereIn('shift_id', $shop->shifts->pluck('id'))
                            ->whereIn('status', [2, 3]);
                    },
                    'shopOrders'            => function ($query) {
                        $query->whereDate('date', $this->filters['filter_date'])
                            ->where('shop_id', $this->filters['filter_shop']);
                    },
                    'movementProductsOut'   => function ($query) {
                        $query->whereDate('created_at', $this->filters['filter_date'])
                            ->where('from_shop_id', $this->filters['filter_shop']);
                    },
                    'movementProductsIn'    => function ($query) {
                        $query->whereDate('created_at', $this->filters['filter_date'])
                            ->where('to_shop_id', $this->filters['filter_shop']);
                    },
                    'rejectProducts'        => function ($query) {
                        $query->whereDate('created_at', $this->filters['filter_date'])
                            ->where('shop_id', $this->filters['filter_shop'])
                            ->with('reject');
                    },
                    'inventoryProducts'      => function ($query) use ($shift) {
                        $query->whereIn('inventory_id', $shift->inventory->pluck('id'))
                            ->with('inventory');
                    }
                ])->orderBy('name')
                    ->get()
                    ->groupBy('category.id')
                    ->sortBy(function ($item) {
                        return $item->first()->getSort();
                    });

                foreach ($categoryProducts as $products) {
                    foreach ($products as $product) {
                        $price = $product->getPrice();
                        $inventoryOpen = [];
                        $inventoryClose = [];

                        // Получаем открытую и закрытую инвенторизацию
                        foreach ($product->inventoryProducts as $inventoryProduct) {
                            if ($inventoryProduct->inventory->type == Inventory::OPEN) {
                                $inventoryOpen = $inventoryProduct;
                            }

                            if ($inventoryProduct->inventory->type == Inventory::CLOSE) {
                                $inventoryClose = $inventoryProduct;
                            }
                        }

                        $this->data['products'][$product->id] = [
                            'name'           => $product->name,
                            'category_color' => $product->getColor(),
                            'start'          => [
                                'quantity' => $inventoryOpen ? $inventoryOpen->quantity : '-',
                                'total'    => $inventoryOpen ? ($inventoryOpen->quantity * $price) : '-'
                            ],
                            'shift'          => [
                                'quantity' => $product->shiftProducts->sum('accept'),
                                'total'    => $product->shiftProducts->sum('accept') * $price
                            ],
                            'movement_out'   => [
                                'quantity' => $product->movementProductsOut->sum('complete'),
                                'total'    => $product->movementProductsOut->sum('complete') * $price
                            ],
                            'movement_in'    => [
                                'quantity' => $product->movementProductsIn->sum('complete'),
                                'total'    => $product->movementProductsIn->sum('complete') * $price
                            ],
                            'order'          => [
                                'quantity' => $product->shopOrders->sum('quantity'),
                                'total'    => $product->shopOrders->sum('quantity') * $price
                            ],
                            'end'            => [
                                'quantity' => $inventoryClose ? $inventoryClose->quantity : '-',
                                'total'    => $inventoryClose ? ($inventoryClose->quantity * $price) : '-'
                            ],
                            'result'         => [
                                'quantity' => 0,
                                'total'    => 0
                            ]
                        ];

                        foreach (Reject::TYPES as $id => $type) {
                            $rejects = $product->rejectProducts->filter(function ($item) use ($id) {
                                return $item->reject->type == $id;
                            });

                            $this->data['products'][$product->id]['reject-' . $id] = [
                                'quantity' => $rejects->sum('quantity'),
                                'total'    => $rejects->sum('quantity') * $price
                            ];
                        }

                        foreach ($this->data['products'][$product->id] as $key => $value) {
                            if (in_array($key, ['start', 'shift', 'movement_in'])) {
                                $this->data['products'][$product->id]['result']['quantity'] -= (float)$value['quantity'];
                                $this->data['products'][$product->id]['result']['total'] -= (float)$value['total'];
                            }

                            if (in_array($key, ['order', 'movement_out', 'reject-1', 'reject-2', 'reject-3', 'end'])) {
                                $this->data['products'][$product->id]['result']['quantity'] += (float)$value['quantity'];
                                $this->data['products'][$product->id]['result']['total'] += (float)$value['total'];
                            }
                        }

                    }
                }
            }
        }

        return view('report.inventory', $this->data, $this->filters);
    }

    public function inventoryDiff()
    {
        $shops = Shop::all();

        $this->data = [
            'products' => [],
            'shops'    => $shops,
            'hours'    => []
        ];

        $this->filters['filter_date'] = request()->get('filter_date', date('Y-m-d'));

        if ($this->filters['filter_shop']) {
            $shop = $shops->first(function ($item) {
                return $item->id == $this->filters['filter_shop'];
            });
        }

        if (empty($shop)) {
            $shop = $shops->first();
        }

        if ($shop) {
            $this->filters['filter_shop'] = $shop->id;

            $shop->load(['shifts' => function ($query) {
                $query->where('type', 'cash')
                    ->whereDate('date_start', '<=', $this->filters['filter_date'])
                    ->where(function ($query) {
                        $query->where('date_end', '>=', $this->filters['filter_date'])->orWhereNull('date_end');
                    });
            }]);

            $this->data['shifts'] = $shop->shifts;

            if ($this->filters['filter_shift']) {
                $shift = $shop->shifts->first(function ($item) {
                    return $item->id == $this->filters['filter_shift'];
                });
            }

            if (empty($shift)) {
                $shift = $shop->shifts->first();
            }

            // Если у магазина есть какие то смены попадающие в эту дату

            if ($shift) {
                $this->filters['filter_shift'] = $shift->id;

                // Подргружаем сличилки при открытии в этой смене
                $shift->load([
                    'inventory' => function ($query) {
                        $query->where('type', 'open');
                    }
                ]);

                // Если есть сличилки при открытии

                if ($shift->inventory->isNotEmpty()) {
                    // Берем предыдущую сличилку

                    $lastInventory = Inventory::where('shop_id', $shift->shop_id)
                        ->where('id', '<', $shift->inventory->first()->id)
                        ->orderByDesc('created_at')
                        ->limit(1)
                        ->get()
                        ->first();

                    $categoryProducts = Product::with([
                        'category',
                        'shopPrice'             => function ($query) {
                            $query->where('shop_id', $this->filters['filter_shop'])
                                ->whereDate('date', $this->filters['filter_date']);
                        },
                        'inventoryProducts'     => function ($query) use ($shift) {
                            $query->whereIn('inventory_id', $shift->inventory->pluck('id'));
                        },
                        'lastInventoryProducts' => function ($query) use ($shift, $lastInventory) {
                            $query->where('inventory_id', $lastInventory ? $lastInventory->id : -1);
                        }
                    ])->orderBy('name')
                        ->get()
                        ->groupBy('category.id')
                        ->sortBy(function ($item) {
                            return $item->first()->getSort();
                        });

                    foreach ($categoryProducts as $products) {
                        foreach ($products as $product) {
                            $price = $product->getPrice();
                            $end = (float)($product->lastInventoryProducts ? $product->lastInventoryProducts->quantity : 0);
                            $start = (float)($product->inventoryProducts->isNotEmpty() ? $product->inventoryProducts->first()->quantity : 0);

                            $this->data['products'][] = [
                                'name' => $product->getName(),
                                'color' => $product->getColor(),
                                'end' => [
                                    'quantity' => $end,
                                    'total'    => $end * $price
                                ],
                                'start' => [
                                    'quantity' => $start,
                                    'total'    => $start * $price
                                ],
                                'result' => [
                                    'quantity' => $start - $end,
                                    'total'    => ($start - $end) * $price
                                ]
                            ];
                        }
                    }

                    if ($this->filters['filter_hidden']) {
                        $this->data['products'] = array_filter($this->data['products'], function ($item) {
                            return ($item['end']['quantity'] || $item['start']['quantity']);
                        });
                    }
                }
            }
        }

        return view('report.inventory-diff', $this->data, $this->filters);
    }

    public function action()
    {
        $shops = Shop::all();

        $this->data = array_merge($this->data, [
            'products' => [],
            'shops'    => $shops,
            'hours'    => $this->hours
        ]);

        $this->filters['filter_date'] = request()->get('filter_date', date('Y-m-d'));

        if ($this->filters['filter_shop']) {
            $shop = $shops->first(function ($item) {
                return $item->id == $this->filters['filter_shop'];
            });
        }

        if (empty($shop)) {
            $shop = $shops->first();
        }

        if ($shop) {
            $this->filters['filter_shop'] = $shop->id;

            $categoryProducts = Product::getAction(new Carbon($this->filters['filter_date']), $this->filters['filter_shop'])
                ->groupBy('category.id')
                ->sortBy(function ($item) {
                    return $item->first()->getSort();
                });

            foreach ($categoryProducts as $products) {
                foreach ($products as $product) {
                    $total = 0;

                    $rows = collect($this->hoursArray)->map(function ($item) {
                        return [
                            'plan'        => 0,
                            'complete'    => 0,
                            'order'       => 0,
                            'reject'      => 0,
                            'movementOut' => 0,
                            'accept'      => 0,
                            'movementIn'  => 0,
                            'inventory'   => 0
                        ];
                    })->toArray();

                    foreach ($product->shiftProducts as $shiftProduct) {
                        // Если товар из плана
                        if ($shiftProduct->plan_id) {
                            $rows[$shiftProduct->hour]['plan'] += $shiftProduct->quantity;
                            $total += $shiftProduct->quantity;
                        }

                        // Если принятый выпуск кассиром
                        // Суммируем с бракеражем, потом мы это вычтем
                        if ($shiftProduct->isAccepted) {
                            $rows[$shiftProduct->updated_at->format('G')]['accept'] += $shiftProduct->accept + $shiftProduct->reject2;
                            $total += $shiftProduct->accept + $shiftProduct->reject2;
                        }
                    }

                    foreach ($product->shopOrders as $shopOrder) {
                        $rows[$shopOrder->date->format('G')]['order'] += $shopOrder->quantity;
                        $total += $shopOrder->quantity;
                    }

                    foreach ($product->rejectProducts as $rejectProduct) {
                        if ($rejectProduct->isShop) {
                            $rows[$rejectProduct->created_at->format('G')]['reject'] += $rejectProduct->quantity;
                            $total += $rejectProduct->quantity;
                        }
                    }

                    foreach ($product->movementProducts as $movementProduct) {
                        if ($movementProduct->isAccepted) {
                            if ($movementProduct->from_shop_id == $shop->id) {
                                $rows[$movementProduct->updated_at->format('G')]['movementOut'] += $movementProduct->complete;
                                $total += $movementProduct->complete;
                            }

                            if ($movementProduct->to_shop_id == $shop->id) {
                                $rows[$movementProduct->updated_at->format('G')]['movementIn'] += $movementProduct->complete;
                                $total += $movementProduct->complete;
                            }
                        }
                    }

                    foreach ($product->inventoryProducts as $inventoryProduct) {
                        $rows[$inventoryProduct->created_at->format('G')]['inventory'] += $inventoryProduct->quantity;
                        $total += $inventoryProduct->quantity;
                    }

                    $inventoryOpen = $product->inventoryProducts->first(function ($item) {
                        if ($item->inventory->type == Inventory::OPEN) {
                            return $item;
                        }
                    });

                    $this->data['products'][] = [
                        'id'    => $product->id,
                        'name'  => $product->getName(),
                        'color' => $product->getColor(),
                        'rows'  => $rows,
                        'start' => $inventoryOpen ? $inventoryOpen->quantity : '-',
                        'end'   => $product->shopQuantityToday->isNotEmpty() ? $product->shopQuantityToday->first()->quantity : 0,
                        'total' => $total
                    ];
                }
            }

            if ($this->filters['filter_hidden']) {
                $this->data['products'] = array_filter($this->data['products'], function ($item) {
                    return $item['total'] ? true : false;
                });
            }
        }

        return view('report.action', $this->data, $this->filters);
    }

    public function shop()
    {

        $user_shop_ids = [];

        foreach(Auth::user()->shopsVisible as $shop) {
            $user_shop_ids[] = $shop->id;
        };

        $shops = Shop::whereIn('id', $user_shop_ids)->get();
        
        $franchise_ids = [];
        
        foreach($shops as $shop) {
            $franchise_ids[] = $shop->franchise_id;
        };

        $franchises = Franchise::whereIn('id', $franchise_ids)->get();
        
        $this->data = array_merge($this->data, [
            'franchises'  => $franchises,
            'reportShops' => []
        ]);

        $this->filters['filter_date'] = request()->get('filter_date', date('Y-m-d'));

        if ($this->filters['filter_franchise']) {
            $franchise = $franchises->first(function ($item) {
                return $item->id == $this->filters['filter_franchise'];
            });
        }

        if (empty($franchise)) {
            $franchise = $franchises->first();
        }

        if ($franchise) {
            $this->data['filter_franchise'] = $franchise->id;

            $franchise->load([
                'shops' => function ($query) use($user_shop_ids) {
                    $query->whereIn('id', $user_shop_ids);
                    $query->with([
                        'shopOrders' => function ($query) {
                            $query->whereDate('date', $this->filters['filter_date']);
                        },
                        'shiftProducts' => function ($query) {
                            $query->whereDate('created_at', $this->filters['filter_date'])
                                ->orderByDesc('created_at')
                                ->with([
                                    'product',
                                    'histories' => function ($query) {
                                        $query->orderByDesc('created_at');
                                    }
                                ]);
                        },
                        'shifts' => function ($query) {
                            $query->whereDate('date_start', $this->filters['filter_date'])
                                ->where('type', ['baker', 'confectioner'])
                                ->with('shiftProducts');
                        },
                        'plans' => function ($query) {
                            $query->whereDate('created_at', $this->filters['filter_date'])->with('products');
                        },
                        'shopQuantity' => function ($query) {
                            $query->whereDate('date', $this->filters['filter_date']);
                            $query->with('product');
                        },
                        'rejectProducts' => function ($query) {
                            $query->whereDate('created_at', $this->filters['filter_date'])->with('reject');
                        },
                        'movementProductsMinus' => function ($query) {
                            $query->whereDate('created_at', $this->filters['filter_date']);
                        },
                        'movementProductsPlus' => function ($query) {
                            $query->whereDate('created_at', $this->filters['filter_date']);
                        },
                        'inventories' => function ($query) {
                            $query->whereDate('created_at', $this->filters['filter_date'])->orderByDesc('created_at');
                        },

                        'inventoryProducts' => function ($query) {
                            $query->whereDate('created_at', $this->filters['filter_date'])->orderByDesc('created_at');
                        }
                    ]);
                }
            ]);

            $productsPrice = [];
            $productsTime = [];

            $products = Product::with([
                'shopPrices'=> function ($query) {
                    $query->whereDate('date', $this->filters['filter_date']);
                }])->get();

            $products->each(function ($item) use (&$productsPrice, &$productsTime, $franchise) {
                foreach ($franchise->shops as $shop) {
                    $price = $item->shopPrices->first(function ($row) use ($shop) {
                        return $row->shop_id == $shop->id;
                    });

                    $productsPrice[$item->id][$shop->id] = ($price ? $price->price : $item->price) / 100;
                    $productsTime[$item->id] = $item->time * 60;
                }
            });
            
            $franchise->shops->each(function ($item) use ($productsPrice, $productsTime) {
                $shop_id = $item->id;

                $movementProducts = array(
                    'product_id' => array(),
                    'shop_id'    => array(),
                    'complete'   => array()
                );

                $lastComplete = $item->shiftProducts->first(function ($item) {
                    if ($item->status === 2 || $item->status === 3) {
                        return $item;
                    }
                });

                // Фактическое время выпуска
                $timeComplete = 0;

                // Моментальные выпуски
                $timeMoment = 0;

                $item->shiftProducts->each(function ($item) use (&$timeComplete, &$timeMoment, $productsTime) {
                    $timeStartFact = $item->histories->first(function ($item) {
                        return $item['status'] === 1;
                    });

                    $timeCompleteFact = $item->histories->first(function ($item) {
                        return $item['status'] === 2;
                    });

                    if ($timeStartFact && $timeCompleteFact) {
                        $timeCompletePlan = Carbon::createFromTime($item->hour, 0, 0);

                        if ($item->hour - ceil($item->product->time / 60) < 0) {
                            $timeStartPlan = Carbon::createFromTime(0, 0, 0);
                        } else {
                            $timeStartPlan = Carbon::createFromTime($item->hour, 30)->subMinutes($item->product->time)->setMinutes(0)->setSeconds(0);
                        }

                        $filterDate = Carbon::createFromFormat('Y-m-d', $this->filters['filter_date']);
                        $timeStartPlan->setDateFrom($filterDate);
                        $timeCompletePlan->setDateFrom($filterDate);

                        // Если фактический час начала не совпадает с планом
                        if ($timeStartPlan->format('H') != $timeStartFact->created_at->format('H')) {
                            // Если время по плану больше фактического, то разницу берем от нее
                            if ($timeStartPlan->gt($timeStartFact)) {
                                $timeComplete += $timeStartPlan->diffInSeconds($timeStartFact->created_at);
                            } else {
                                $timeComplete += $timeStartPlan->copy()->addHour()->diffInSeconds($timeStartFact->created_at);
                            }
                        }

                        if ($timeCompletePlan->format('H') != $timeCompleteFact->created_at->format('H')) {
                            // Если время по плану больше фактического, то разницу берем от нее
                            if ($timeCompletePlan->gt($timeCompleteFact)) {
                                $timeComplete += $timeCompletePlan->diffInSeconds($timeCompleteFact->created_at);
                            } else {
                                $timeComplete += $timeCompletePlan->copy()->addHour()->addMinutes($item->product->time)->diffInSeconds($timeCompleteFact->created_at);
                            }
                        }

                        // За сколько секунд выпустили продукцию
                        $seconds = $timeStartFact->created_at->diffInSeconds($timeCompleteFact->created_at);

                        // Сравниваем со временем готовки по плану
                        $percent = floor(($seconds / max(1, $productsTime[$item->product_id])) * 100);

                        $timeMoment += $percent <= 75 ? 1 : 0;
                    }
                });

                if ($timeComplete) {
                    $now = Carbon::now();

                    $timeComplete = $now->diff($now->copy()->addSeconds($timeComplete))->format('%d %H:%I:%S');
                } else {
                    $timeComplete = '';
                }

                $this->data['reportShops'][] = [
                    'name'          => $item->name,
                    'order'         => $item->shopOrders->sum(function ($item) {
                        return $item->amount / 100;
                    }),
                    'shift'         => $item->shifts->sum(function ($item) use($shop_id, $productsPrice) {
                        return $item->shiftProducts->sum(function ($item) use ($shop_id, $productsPrice) {
                            if ($item->plan_id) {
                                return $item->quantity * $productsPrice[$item->product_id][$shop_id];
                            }
                        });
                    }),
                    'quantity'      => $item->shopQuantity->sum(function ($item) use ($shop_id, $productsPrice) {
                        if($item->product->hide_shop != 1) {
                            return $item->quantity * $productsPrice[$item->product_id][$shop_id];
                        }
                    }),
                    'sku'           => $item->shopQuantity->sum(function ($item) {
                        return $item->isLive ? 1 : 0;
                    }),
                    'complete'      => $item->shiftProducts->sum(function ($item) use ($shop_id, $productsPrice) {
                        return $item->complete * $productsPrice[$item->product_id][$shop_id];
                    }),
                    'accept'        => $item->shiftProducts->sum(function ($item) use ($shop_id, $productsPrice) {
                        return $item->accept * $productsPrice[$item->product_id][$shop_id];
                    }),
                    'discount'      => $item->shopOrders->sum(function ($item) {
                        return $item->discount / 100;
                    }),
                    'reject'        => $item->rejectProducts->sum(function ($item) use ($shop_id, $productsPrice) {
                        // ХЗ все списания или только кассовые
                        //if ($rejectProduct->isShop || ($rejectProduct->isAccept && $rejectProduct->reject->type == 2)) {

                        return $item->quantity * $productsPrice[$item->product_id][$shop_id];
                    }),
                    'movementsMinus'     => $item->movementProductsMinus->sum(function ($item) use ($shop_id, $productsPrice) {
                        return -(float)$item->complete * $productsPrice[$item->product_id][$shop_id];
                    }),
                    'movementsPlus'     => $item->movementProductsPlus->sum(function ($item) use ($shop_id, $productsPrice) {
                        return (float)$item->complete * $productsPrice[$item->product_id][$shop_id];
                    }),
                    'lastComplete'  => $lastComplete && $item->shifts->count() > 0 ? $lastComplete->updated_at->diff(
                        is_numeric($item->shifts->keys()->last()) ?
                            $item->shifts[$item->shifts->keys()->last()]->date_end != "0000-00-00 00:00:00"
                                ? $item->shifts[$item->shifts->keys()->last()]->date_end
                                : Carbon::now()
                            : "0000-00-00 00:00:00"
                        )->format('%H:%I:%S') : '',
                    'timeComplete'  => $timeComplete,
                    'timeMoment'    => $timeMoment,
                    'quantityMinus' => $item->shopQuantity->sum(function ($item) {
                        return $item->quantity < 0 ? 1 : 0;
                    }),
                    'plus' => is_numeric($item->inventories->keys()->first())
                    ?   $item->inventories[$item->inventories->keys()->first()]->inventoryProducts->sum(function ($item) use ($shop_id, $productsPrice) {
                            return $item->quantity > $item->before ? ($item->quantity - $item->before) * $productsPrice[$item->product_id][$shop_id] : 0;
                        })
                    : 0,
                    // 'plus'          => $item->inventoryProducts->sum(function ($item) use ($shop_id, $productsPrice) {
                    //     // if ($item->shop_id === 1) {
                    //     // //     dump($item);
                    //     // };
                    //     return $item->quantity > $item->before ? $item->quantity * $productsPrice[$item->product_id][$shop_id] : 0;
                    // }),
                    'minus' => is_numeric($item->inventories->keys()->first())
                    ?   $item->inventories[$item->inventories->keys()->first()]->inventoryProducts->sum(function ($item) use ($shop_id, $productsPrice) {
                            return $item->quantity < $item->before ? -($item->before - $item->quantity) * $productsPrice[$item->product_id][$shop_id] : 0;
                        })
                    : 0,
                    // 'minus'         => $item->inventoryProducts->sum(function ($item) use ($shop_id, $productsPrice) {
                    //     // dump($item->created_at);
                    //     // if ($item->shop_id === 1) {
                    //     //     dump($item->quantity < $item->before ? $item->quantity * $productsPrice[$item->product_id][$shop_id] : 0);
                    //     // };
                    //     return $item->quantity < $item->before ? $item->quantity * $productsPrice[$item->product_id][$shop_id] : 0;
                    // })
                ];
            });
        }

        //print_r($this->data['reportShops']);

        $agent = new Agent();
        $this->data['mobileView'] = ($agent->isTablet() || $agent->isPhone()) ? true : false;

        return view('report.shop', $this->data, $this->filters);
    }

    public function top()
    {
        $this->data = [
            'products' => [],
            'shops'    => Shop::all(),
            'sorts'    => [
                'count' => 'Количество',
                'total' => 'Сумма'
            ]
        ];

        $products = ProductShopOrder::select('*', DB::raw('SUM(quantity) as count'), DB::raw('SUM(amount) as total'))
            ->whereDate('date', '>=', $this->filters['filter_date_start'])
            ->whereDate('date', '<=', $this->filters['filter_date_end'])
            ->groupBy('product_id')
            ->orderByDesc($this->filters['filter_sort'] == 'count' ? 'count' : 'total')
            ->with('product.category');

        if ($this->filters['filter_shop']) {
            $products->where('shop_id', $this->filters['filter_shop']);
        }

        foreach ($products->get() as $product) {
            $this->data['products'][] = [
                'name'     => $product->product->getName(),
                'category' => $product->product->getCategory(),
                'color'    => $product->product->getColor(),
                'count'    => (float)$product->count,
                'total'    => $product->total / 100
            ];
        }

        return view('report.top', $this->data, $this->filters);
    }

    public function quantity()
    {
        $shops = Shop::all();
        $settings = Auth::user()->reportSettings( __FUNCTION__);
        $categories = isset($settings->settings['categories']) ? $settings->settings['categories'] : [];

        $this->data = array_merge($this->data, [
            'products' => [],
            'shops'    => $shops,
            'hours'    => $this->hours,
            'settings' => route('user.reportSettings', __FUNCTION__)
        ]);

        $this->filters['filter_date'] = request()->get('filter_date', date('Y-m-d'));

        if ($this->filters['filter_shop']) {
            $shop = $shops->first(function ($item) {
                return $item->id == $this->filters['filter_shop'];
            });
        }

        if (empty($shop)) {
            $shop = $shops->first();
        }

        if ($shop) {
            $this->filters['filter_shop'] = $shop->id;

            $categoryProducts = Product::getAction(new Carbon($this->filters['filter_date']), $this->filters['filter_shop'])
                ->groupBy('category.id')
                ->sortBy(function ($item) {
                    return $item->first()->getSort();
                });

            foreach ($categoryProducts as $products) {
                foreach ($products as $product) {
                    if ($categories && !in_array((int)$product->category_id, $categories)) {
                        continue;
                    }

                    $rows = collect($this->hoursArray)->map(function ($item) {
                        return 0;
                    })->toArray();

                    foreach ($product->shopOrders as $shopOrder) {
                        $rows[$shopOrder->date->format('G')] -= (float)$shopOrder->quantity;
                    }

                    foreach ($product->rejectProducts as $rejectProduct) {
                        if ($rejectProduct->isShop) {
                            $rows[$rejectProduct->created_at->format('G')] -= (float)$rejectProduct->quantity;
                        }
                    }

                    foreach ($product->movementProducts as $movementProduct) {
                        if ($movementProduct->isAccepted) {
                            if ($movementProduct->from_shop_id == $shop->id) {
                                $rows[$movementProduct->updated_at->format('G')] -= (float)$movementProduct->complete;
                            }

                            if ($movementProduct->to_shop_id == $shop->id) {
                                $rows[$movementProduct->updated_at->format('G')] += (float)$movementProduct->complete;
                            }
                        }
                    }

                    foreach ($product->shiftProducts as $shiftProduct) {
                        if ($shiftProduct->isAccepted) {
                            $rows[$shiftProduct->updated_at->format('G')] += (float)$shiftProduct->accept;
                        }
                    }

                    $start = $product->shopQuantityYesterday->isNotEmpty() ? $product->shopQuantityYesterday->first()->quantity : 0;
                    $end = $product->shopQuantityToday->isNotEmpty() ? $product->shopQuantityToday->first()->quantity : 0;

                    foreach ($rows as $hour => &$row) {
                        // Берем колличество из предыдущего часа и плюсуем его со всеми движухами в этом часе
                        $before = isset($rows[$hour - 1]) ? $rows[$hour - 1] : $start;
                        $row = $before + $row;
                    }

                    $this->data['products'][] = [
                        'name'  => $product->getName(),
                        'color' => $product->getColor(),
                        'rows'  => $rows,
                        'start' => (float)$start,
                        'end'   => (float)$end,
                        'total' => array_sum($rows)
                    ];
                }
            }

            if ($this->filters['filter_hidden']) {
                $this->data['products'] = array_filter($this->data['products'], function ($item) {
                    return $item['total'] ? true : false;
                });
            }
        }

        return view('report.quantity', $this->data, $this->filters);
    }

    public function control()
    {
        $shops = Shop::all();

        $this->data = [
            'products' => [],
            'shops'    => $shops
        ];

        $this->filters['filter_date'] = request()->get('filter_date', date('Y-m-d'));

        if ($this->filters['filter_shop']) {
            $shop = $shops->first(function ($item) {
                return $item->id == $this->filters['filter_shop'];
            });
        }

        if (empty($shop)) {
            $shop = $shops->first();
        }

        if ($shop) {
            $this->filters['filter_shop'] = $shop->id;

            $shop->load(['shifts' => function ($query) {
                $query->where('type', 'cash')
                    ->whereDate('date_start', '<=', $this->filters['filter_date'])
                    ->where(function ($query) {
                        $query->where('date_end', '>=', $this->filters['filter_date'])->orWhereNull('date_end');
                    });
            }]);

            $this->data['shifts'] = $shop->shifts;

            if ($this->filters['filter_shift']) {
                $shift = $shop->shifts->first(function ($item) {
                    return $item->id == $this->filters['filter_shift'];
                });
            }

            if (empty($shift)) {
                $shift = $shop->shifts->first();
            }

            // Если у магазина есть какие то смены попадающие в эту дату

            if ($shift) {
                $this->filters['filter_shift'] = $shift->id;

                // Подргружаем сличилки при открытии в этой смене
                // И все списания в этой смене
                $shift->load([
                    'inventory' => function ($query) {
                        $query->where('type', 'open');
                    },
                    'rejects' => function($query) {
                        $query->where('type', 3);
                    }
                ]);

                $categoryProducts = Product::with([
                    'category',
                    'shopPrice'         => function ($query) {
                        $query->where('shop_id', $this->filters['filter_shop'])
                            ->whereDate('date', $this->filters['filter_date']);
                    },
                    'inventoryProducts' => function ($query) use ($shift) {
                        $query->whereIn('inventory_id', $shift->inventory->pluck('id'));
                    },
                    'shopOrders'        => function ($query) {
                        $date = Carbon::createFromDate($this->filters['filter_date']);

                        $query->whereDate('date', '>=', $date)
                            ->whereDate('date', '<=', $date->addHours(12))
                            ->where('shop_id', $this->filters['filter_shop'])
                            ->whereRaw('(((discount / (discount + amount)) * 100) between 25 and 35)');
                    },
                    'rejectProducts'    => function ($query) use ($shift) {
                        $query->whereIn('reject_id', $shift->rejects->pluck('id'));
                    }
                ])->orderBy('name')
                    ->get()
                    ->groupBy('category.id')
                    ->sortBy(function ($item) {
                        return $item->first()->getSort();
                    });

                foreach ($categoryProducts as $products) {
                    foreach ($products as $product) {
                        $price = $product->getPrice();
                        $start = (float)($product->inventoryProducts->isNotEmpty() ? $product->inventoryProducts->first()->quantity : 0);

                        $order = $product->shopOrders->sum(function ($item) {
                            return $item->quantity;
                        });

                        $reject = [
                            'all' => [
                                'quantity' => 0,
                                'total'    => 0
                            ],
                            'time' => [
                                'quantity' => 0,
                                'total'    => 0
                            ],
                            'other' => [
                                'quantity' => 0,
                                'total'    => 0
                            ]
                        ];

                        $product->rejectProducts->each(function ($item) use (&$reject, $price) {
                            $reject['all']['quantity'] += $item->quantity;
                            $reject['all']['total'] += $item->quantity * $price;

                            if ($item->created_at->format('H') >= 12 && $item->created_at->format('H') <= 14) {
                                $reject['time']['quantity'] += $item->quantity;
                                $reject['time']['total'] += $item->quantity * $price;
                            } else {
                                $reject['other']['quantity'] += $item->quantity;
                                $reject['other']['total'] += $item->quantity * $price;
                            }
                        });

                        $this->data['products'][] = [
                            'name'  => $product->getName(),
                            'color' => $product->getColor(),
                            'start' => [
                                'quantity' => $start,
                                'total'    => $start * $price
                            ],
                            'order' => [
                                'quantity' => $order,
                                'total'    => $order * $price
                            ],
                            'reject' => $reject,
                            'result' => [
                                'quantity' => ($start - $order - $reject['all']['quantity']),
                                'total'    => ($start - $order - $reject['all']['quantity']) * $price
                            ]
                        ];
                    }
                }

                if ($this->filters['filter_hidden']) {
                    $this->data['products'] = array_filter($this->data['products'], function ($item) {
                        return ($item['start']['quantity'] || $item['order']['quantity'] ||  $item['reject']['all']['quantity']);
                    });
                }
            }
        }

        return view('report.control', $this->data, $this->filters);
    }
}
