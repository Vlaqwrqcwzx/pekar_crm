<?php

namespace App\Http\Controllers;

use App\Franchise;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class FranchiseController extends Controller
{
    public function index()
    {
        return view('franchise.index', ['franchises' => Franchise::all()]);
    }

    public function create()
    {
        return view('franchise.form', [
            'franchise' => '',
            'action'    => route('franchise.store')
        ]);
    }

    public function store(Request $request)
    {
        Validator::make($request->all(), [
            'name' => [
                'required',
                'max:100'
            ]
        ])->validate();

        Franchise::create([
            'name' => $request->post('name')
        ]);

        return redirect()
            ->route('franchise.index')
            ->with('success', 'Франшиза успешно добавлена');
    }

    public function edit(Franchise $franchise)
    {
        return view('franchise.form', [
            'franchise' => $franchise,
            'action'    => route('franchise.update', ['id' => $franchise->id])
        ]);
    }

    public function update(Request $request, Franchise $franchise)
    {
        Validator::make($request->all(), [
            'name' => [
                'required',
                'max:100'
            ]
        ])->validate();

        $franchise->update([
            'name' => $request->post('name')
        ]);

        return redirect()
            ->route('franchise.index')
            ->with('success', 'Франшиза успешно обновлена');
    }
}
