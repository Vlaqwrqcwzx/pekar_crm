<?php

namespace App\Http\Controllers;

use App\Import;
use App\Product;
use App\Category;
use App\DeviceCategory;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Validator;

class CategoryController extends Controller
{

    public function index()
    {
        return view('category.index', [
            'imports' => Import::all()
        ]);
    }

    public function import(Import $import) {
        return view('category.import', [
            'categories' => Category::where('import_id', '=', $import->id)->get(),
            'import_id'  => $import->id,
        ]);
    }

    public function create(Request $request)
    {
        return view('category.form', [
            'category'          => '',
            'sort'              => '',
            'color'             => '#FFFFFF',
            'device_categories' => DeviceCategory::where('import_id', '=', intval($request->all()["import"]))->get(),
            'import'            => Import::find(intval($request->all()["import"])),
            'action'            => route('category.store')
        ]);
    }

    public function store(Request $request)
    {
        Validator::make($request->all(), [
            'name' => [
                'required',
                'max:200',
            ],
        ])->validate();

        $category = Category::create($request->except(['device_category']));

        if ($request->has('device_category')) {
            DeviceCategory::whereIn('device_category_id', $request->post('device_category'))->update([
                'category_id' => $category->id
            ]);
            Product::whereIn('device_category_id', $request->post('device_category'))->update([
                'category_id' => $category->id
            ]);
        }

        return redirect()
            ->route('category.index')
            ->with('success', 'Категория успешно добавлена');
    }

    public function edit(Category $category)
    {
        return view('category.form', [
            'category'          => $category,
            'device_categories' => DeviceCategory::all(),
            'import'            => Import::find($category->import_id),
            'action'            => route('category.update', ['id' => $category->id])
        ]);
    }

    public function update(Request $request, Category $category)
    {
        Validator::make($request->all(), [
            'name' => [
                'required',
                'max:200',
            ],
        ])->validate();

        $category->update([
            'name'    => $request->post('name'),
            'color'   => $request->post('color'),
            'sort'    => $request->post('sort'),
            'shop'    => $request->post('shop', 0),
            'kitchen' => $request->post('kitchen', 0)
        ]);

        DeviceCategory::where('category_id', $category->id)->update(['category_id' => 0]);
        Product::where('category_id', $category->id)->update(['category_id' => 0]);

        if ($request->has('device_category')) {
            DeviceCategory::whereIn('device_category_id', $request->post('device_category'))->update([
                'category_id' => $category->id
            ]);
            Product::whereIn('device_category_id', $request->post('device_category'))->update([
                'category_id' => $category->id
            ]);
        }

        return redirect()
            ->route('category.index')
            ->with('success', 'Категория успешно изменена');
    }

    public function destroy()
    {
        foreach (request()->input('id') as $id) {
            Category::destroy($id);
        }

        return redirect()
            ->route('category.index')
            ->with('success', 'Выбранные категории успешно удалены');
    }
}
