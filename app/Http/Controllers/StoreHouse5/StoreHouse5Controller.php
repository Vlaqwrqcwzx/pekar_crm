<?php

namespace App\Http\Controllers\StoreHouse5;

use App\Http\Controllers\Controller;

use App\Product;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;


class StoreHouse5Controller extends Controller {

  // Метод коннекта к StoreHouse5
  protected function SH5_CONNECT($url, $username, $password="", $procName, $param=false){
    // if(strpos(" ".$_SERVER['HTTP_HOST'], "localhost")>0){
    //     // тогда оставляем как есть, т.к. на локалке сторхауз есть
    // }else{
    //     $url = str_replace('127.0.0.1','85.95.183.21',$url);
    // }
    if(!$url) return false;
    
    if(!$procName) $procName = 'NoProcedureName';
    
    $data = json_encode( array( "UserName"=>$username, "Password"=>$password, "procName"=>$procName ,"Input" => $param ) );  

    $ch = curl_init($url);

    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_ENCODING, "");
    curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
    curl_setopt($ch, CURLOPT_TIMEOUT, 0);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json') );    
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $html = curl_exec($ch);
    
    curl_close($ch);        
    $array = json_decode($html,true);

    return $array;
  }

}

?>