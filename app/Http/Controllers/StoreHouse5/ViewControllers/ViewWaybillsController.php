<?php

namespace App\Http\Controllers;

use App\Shop;
use App\Reject;
use App\Product;
use App\Sh5Corr;
use App\Sh5Depart;
use App\Sh5Waybill;
use App\Sh5WaybillProduct;

use Carbon\Carbon;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

use App\Http\Controllers\Api\StoreHouse5\ApiInvoicesController;

class ViewWaybillsController extends Controller
{

    public function index()
    {
        return view('storehouse5/waybills.index', [
            'corrs' => Sh5Corr::all(),
            'departs' => Sh5Depart::all(),
            'waybills' => Sh5Waybill::orderBy('updated_at','desc')->get()
        ]);
    }

    public function edit(Sh5Waybill $waybill)
    {
        $products = Sh5WaybillProduct::where('waybill_id', $waybill->id)->get();

        return view('storehouse5/waybills.form', [
            'waybill' => $waybill,
            'products' => $products,
            'corrs' => Sh5Corr::all(),
            'departs' => Sh5Depart::all(),
            'action' => route('waybills.update', ['id' => $waybill->id])
        ]);
    }

    public function unload(Request $request)
    {

        $waybill = Sh5Waybill::where('id', '=', intval($request->all()["id"]))->with('waybillProducts')->first();

        $params = array(
            "proc_name"           => $waybill->proc_name,
            "harakter"            => "Р",
            "type"                => $waybill->type,
            "in_shift"            => $waybill->shift_id === 0 ? false : true,
            "worker"              => $waybill->worker,
            "payment"             => $waybill->payment,
            "date_interval"       => [$waybill->date_start, $waybill->date_end],
            "created_at"          => Carbon::now(),
            "shop_id"             => $waybill->shop_id,
            "shift_id"            => $waybill->shift_id,
            "reject_id"           => $waybill->reject_id,
            "movement_id"         => $waybill->movement_id,
            "to_shop_id"          => $waybill->to_shop_id,
            "inventory_id"        => $waybill->inventory_id,
            "shift_products_id"   => $waybill->shift_products_id,
            "depart_rid"          => intval($request->all()["depart_rid"]),
            "corr_rid"            => intval($request->all()["corr_rid"]),
            "setting_id"          => $waybill->setting_id,
        );

        $waybill_products = $waybill->waybillProducts;
        
        $api_invoices = new ApiInvoicesController();
        
        $api_invoices->hand_made($params, $waybill_products);

        return redirect()
            ->route('waybills.index')
            ->with('success', 'Накладная успешно отправлена!');
    }

}
