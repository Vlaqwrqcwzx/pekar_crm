<?php

namespace App\Http\Controllers;

use App\Sh5Depart;
use App\Sh5Setting;

class ViewDepartController extends Controller
{
    public function index()
    {
        return view('storehouse5/departs.index', [
            'departs' => Sh5Depart::all(),
            'settings' => Sh5Setting::all()
        ]);
    }

    public function setting(Sh5Setting $setting) {
        $departs = Sh5Depart::where('setting_id', '=', $setting->id)->get();
        return view('storehouse5/departs.setting', [
            'departs' => $departs,
            'setting_id' => $setting->id,
        ]);
    }

}
