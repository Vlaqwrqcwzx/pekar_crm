<?php

namespace App\Http\Controllers;

use App\Sh5SettingGroup;
use App\Sh5Setting;

class ViewSettingGroupController extends Controller
{
    public function index()
    {
        return view('storehouse5/setting_groups.index', [
            'groups' => Sh5SettingGroup::all(),
            'settings' => Sh5Setting::all()
        ]);
    }

}
