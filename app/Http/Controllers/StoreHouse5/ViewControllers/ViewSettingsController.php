<?php

namespace App\Http\Controllers;

use App\Sh5Setting;
use App\Sh5SettingGroup;
use App\Import;
use App\Device;
use App\Http\Controllers\Api\StoreHouse5\ApiProductGroupController;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Validator;

class ViewSettingsController extends Controller
{
    public function index()
    {
        return view('storehouse5/settings.index', [
            'settings' => Sh5Setting::all()
        ]);
    }

    public function create()
    {
        return view('storehouse5/settings.form', [
            'imports' => Import::all(),
            'groups'  => Sh5SettingGroup::all(),
            'action'  => route('settings.store')
        ]);
    }

    public function edit(Sh5Setting $setting)
    {
        return view('storehouse5/settings.form', [
            'setting' => $setting,
            'imports' => Import::all(),
            'groups'  => Sh5SettingGroup::where('setting_id', '=', $setting->id)->get(),
            'action'  => route('settings.update', ['id' => $setting->id])
        ]);
    }

    public function update(Request $request, Sh5Setting $setting)
    {

        Validator::make($request->all(), [
            'name' => [
                'required',
                'max:255',
            ],
            'ip' => [
                'required',
                'max:20',
            ],
            'port' => [
                'required',
                'max:10',
                'unique:sh5_settings,port,' . $setting->id
            ],
            'username' => [
                'required',
                'max:255',
            ],
        ])->validate();

        $password = $request->post('password') == NULL ? "" : $request->post('password');

        $setting->update($request->except(['import', 'connect']),
            [
                'name'     => $request->post('name'),
                'ip'    => $request->post('ip'),
                'port'    => $request->post('port'),
                'username'    => $request->post('username'),
                'password'    => $password,
            ]
        );

        Import::where('setting_id', $setting->id)->update(['setting_id' => 0]);

        Device::where('setting_id', $setting->id)->update(['setting_id' => 0]);

        if ($request->has('import')) {
            Import::whereIn('id', $request->post('import'))->update([
                'setting_id' => $setting->id
            ]);

            Device::whereIn('import_id', $request->post('import'))->update([
                'setting_id' => $setting->id
            ]);
        }


        $product_group = new ApiProductGroupController();

        $product_group->getGroups($request->post('ip'), $request->post('port'), $request->post('username'), $password);

        return redirect()
            ->route('settings.index')
            ->with('success', 'Настройка успешно обновлена');
    }

    public function store(Request $request)
    {

        Validator::make($request->all(), [
            'connect' => [
                'required',
            ],
            'name' => [
                'required',
                'max:255',
            ],
            'ip' => [
                'required',
                'max:20',
            ],
            'port' => [
                'required',
                'max:10',
                'unique:sh5_settings'
            ],
            'username' => [
                'required',
                'max:255',
            ],
        ])->validate();

        $password = $request->post('password') == NULL ? "" : $request->post('password');

        Sh5Setting::create([
            'name'     => $request->post('name'),
            'ip'    => $request->post('ip'),
            'port'    => $request->post('port'),
            'username'    => $request->post('username'),
            'password'    => $password,
        ]);

        $product_group = new ApiProductGroupController();

        $product_group->getGroups($request->post('ip'), $request->post('port'), $request->post('username'), $password);

        return redirect()
            ->route('settings.index')
            ->with('success', 'Настройка успешно добавлена');
    }

}
