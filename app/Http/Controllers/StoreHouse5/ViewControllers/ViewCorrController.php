<?php

namespace App\Http\Controllers;

use App\Sh5Corr;
use App\Sh5Setting;

class ViewCorrController extends Controller
{
    public function index()
    {
        return view('storehouse5/corrs.index', [
            'corrs' => Sh5Corr::all(),
            'settings' => Sh5Setting::all()
        ]);
    }

    public function setting(Sh5Setting $setting) {
        $corrs = Sh5Corr::where('setting_id', '=', $setting->id)->get();
        return view('storehouse5/corrs.setting', [
            'corrs' => $corrs,
            'setting_id' => $setting->id,
        ]);
    }

}
