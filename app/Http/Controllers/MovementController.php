<?php

namespace App\Http\Controllers;

use App\Movement;
use App\Shop;
use App\Sh5Depart;

class MovementController extends ReportController
{
    public function index()
    {
        $this->data = [
            'shops' => Shop::all()
        ];

        $movements = Movement::with([
            'fromUser',
            'toUser',
            'fromShop' => function($query) {
                $query->with('from_depart_one');
                $query->with('from_depart_two');
            },
            'toShop'=> function($query) {
                $query->with('to_depart');
            },
            'movementProducts' => function($query) {
                $query->with('product');
            },
        ])->whereDate('created_at', '>=', $this->filters['filter_date_start'])
            ->whereDate('created_at', '<=', $this->filters['filter_date_end'])
            ->orderBy('created_at');

        if ($this->filters['filter_shop']) {
            $movements->where(function ($query) {
                $query->where('from_shop_id', $this->filters['filter_shop'])
                    ->orWhere('to_shop_id', $this->filters['filter_shop']);
            });
        }

        $this->data['movements'] = $movements->get();
        $this->data['summ_countable'] = array();
        // $this->data['summ_scalable'] = array();

        foreach($this->data['movements'] as $movement) {
            $this->data['summ_countable'][] = $movement->movementProducts->sum(function ($item) {
                // if($item->product->getType() === "countable") {
                    return $item->complete * $item->product->getPrice();
                // }
            });
        }

        // foreach($this->data['movements'] as $movement) {
        //     $this->data['summ_scalable'][] = $movement->movementProducts->sum(function ($item) {
        //         if($item->product->getType() === "scalable") {
        //             return $item->complete * $item->product->getPrice();
        //         }
        //     });
        // }

        return view('movement.index', $this->data, $this->filters);
    }

    public function view(movement $movement)
    {
        $totals = [
            'count' => [
                'countable' => 0,
                'scalable'  => 0,
            ],
            'price' => [
                'countable' => 0,
                'scalable'  => 0,
            ]
        ];

        $movement->load(['movementProducts.product.shopPrice' => function ($query) use ($movement) {
            $query->where('shop_id', $movement->shop_id)
                ->whereDate('date', $movement->created_at->format('Y-m-d'));
        }]);

        $movement->movementProducts->each(function ($item) use (&$totals) {
            $type = $item->product->getType();

            $totals['count'][$type] += $item->complete;
            $totals['price'][$type] += $item->complete * $item->product->getPrice();
        });

        return view('movement.view', [
            'movement' => $movement,
            'totals'    => $totals
        ]);
    }
}
