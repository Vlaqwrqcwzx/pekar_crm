<?php

namespace App\Http\Controllers;

use App\UserGroup;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class UserGroupController extends Controller
{
    public function index()
    {
        return view('user_group.index', ['groups' => UserGroup::all()]);
    }

    public function create()
    {
        return view('user_group.form', [
            'group'      => '',
            'action'     => route('user_group.store'),
            'menu'       => config('menu'),
            'group_menu' => old('menu', []),
            'types'      => UserGroup::TYPES
        ]);
    }

    public function store(Request $request)
    {
        Validator::make($request->all(), [
            'name' => [
                'required',
                'max:100',
                'unique:user_groups'
            ]
        ])->validate();

        $group = new UserGroup();
        $group->name = $request->post('name');
        $group->menu = serialize($request->post('menu'));
        $group->type = $request->post('type');
        $group->save();

        return redirect()
            ->route('user_group.index')
            ->with('success', 'Группа успешно добавлена');
    }

    public function edit(UserGroup $user_group)
    {
        return view('user_group.form', [
            'group'      => $user_group,
            'action'     => route('user_group.update', ['id' => $user_group->id]),
            'menu'       => config('menu'),
            'group_menu' => old('menu', (array)unserialize($user_group->menu)),
            'types'      => UserGroup::TYPES
        ]);
    }

    public function update(Request $request, UserGroup $user_group)
    {
        Validator::make($request->all(), [
            'name' => [
                'required',
                'max:100',
                Rule::unique('user_groups')->ignore($user_group->id),
            ]
        ])->validate();

        $user_group->name = $request->post('name');
        $user_group->menu = serialize($request->post('menu'));
        $user_group->type = $request->post('type');
        $user_group->save();

        return redirect()
            ->route('user_group.index')
            ->with('success', 'Группа успешно обновлена');
    }
}
