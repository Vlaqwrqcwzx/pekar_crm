<?php

namespace App\Http\Controllers;

use App\Inventory;
use App\InventoryProduct;
use App\Movement;
use App\MovementProduct;
use App\Plan;
use App\Reject;
use App\RejectProduct;
use App\Shift;
use App\ShiftProduct;
use App\ShiftProductHistory;
use App\UserShift;
use App\Shop;
use App\ProductShopQuantity;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class ShiftController extends Controller
{
    public function index()
    {
        if (request()->has('delete')) {
            Inventory::truncate();
            InventoryProduct::truncate();
            Movement::truncate();
            MovementProduct::truncate();
            Reject::truncate();
            RejectProduct::truncate();
            Shift::truncate();
            ShiftProduct::truncate();
            ShiftProductHistory::truncate();
            UserShift::truncate();

            exit;
        }

        // Если у пользователя есть открытая смена, перекидываем его в нее

        $userShift = UserShift::open()->with('Shift')->first();

        if ($userShift) {
            return redirect()->route('shift.shop', ['shop' => $userShift->shop_id]);
        }

        return view('shift.index', [
            'shops' => Auth::user()->shopsVisible()->get(),
            'shop'  => false,
            'shift' => false,
            'open'  => false
        ]);
    }

    public function shop(Shop $shop)
    {
        // Если у пользователя есть открытая смена и она не совпадает с этим магазином, перекидываем его в нее

        $userShift = UserShift::open()->with('Shift')->first();

        if ($userShift && $userShift->shop_id != $shop->id) {
            return redirect()->route('shift.shop', ['shop' => $userShift->shop_id]);
        }

        return view('shift.index', [
            'shops'     => [],
            'shop'      => $shop,
            'shift'     => UserShift::open($shop->id)->first(),
            // Есть ли открытые смены для текущего пользователя?
            'openShift' => Shift::open($shop->id, Auth::user()->group->shiftType)->first(),
            'canOpen'   => Auth::user()->group->canOpenShift
        ]);
    }

    public function open(Shop $shop)
    {
        $user = Auth::user();

        // Есть ли открытые смены у пользователя?

        $userShift = UserShift::open()->first();

        if ($userShift) {
            return redirect()->route('shift.view', ['shift' => $userShift->shift_id]);
        }

        // Открыта ли смена в этом магазине

        $openShift = Shift::open($shop->id, Auth::user()->group->shiftType)->first();

        if ($openShift) {
            return redirect()->route('shift.shop', ['shop' => $shop->id])->with('warning', 'В этом магазине уже открыта смена');
        }

        // Создаем смену

        $shift = Shift::create([
            'shop_id'       => $shop->id,
            'type'          => $user->group->shiftType,
            'user_id_start' => Auth::user()->id,
            'date_start'    => Carbon::now()
        ]);

        // Записываем этого пользователя в смену

        UserShift::create([
            'shift_id'   => $shift->id,
            'shop_id'    => $shop->id,
            'user_id'    => Auth::user()->id,
            'date_start' => Carbon::now()
        ]);

        // Если смену открыват пекарь
        // и есть пользователи, которые раньше в этом магазине отметили приход, то привязываем их к этой смене

        if ($user->group->isBaker) {
            UserShift::where('shop_id', $shop->id)
                ->whereDate('date_start', Carbon::now()->toDateString())
                ->where('shift_id', 0)
                ->update(['shift_id' => $shift->id]);
        }

        // Если группа "кухня", то добавляем еще план

        if ($user->group->isKitchen) {
            $plans = Plan::where('shop_id', $shop->id)
                ->where(date('D'), 1)
                ->where('type', $user->group->shiftType)
                ->with(['products' => function ($query) {
                    $query->where('quantity', '>', 0);
                }])
                ->get();

            foreach ($plans as $plan) {
                foreach ($plan->products as $product) {
                    $key = $product->product_id . $product->hour . Carbon::now()->format('His');

                    $code1 = sprintf('%s-%s', mb_substr($key . $product->hour, 0, 6), rand(111111, 999999));
                    $code2 = sprintf('%s-%s', mb_substr($key . $product->hour, 0, 6), rand(111111, 999999));

                    ShiftProduct::create([
                        'shift_id'   => $shift->id,
                        'plan_id'    => $plan->id,
                        'product_id' => $product->product_id,
                        'shop_id'    => $shop->id,
                        'hour'       => $product->hour,
                        'quantity'   => $product->quantity,
                        'code1'      => $code1,
                        'code2'      => $code2,
                    ]);
                }
            }
        }

        return redirect()->route('shift.view', ['shift' => $shift->id])->with('success', 'Смена открыта');
    }

    public function close(Shift $shift)
    {
        // Если кассир закрывает смену

        if (Auth::user()->group->isCash) {
            // Если есть товары, которые нужно принять от пекаря
            // то смену не можем закрыть

            $shiftProducts = ShiftProduct::where('shop_id', $shift->shop_id)
                ->whereDate('created_at', Carbon::now()->toDateString())
                ->whereIn('status', [2, 3])
                ->get();

            foreach ($shiftProducts as $shiftProduct) {
                if ($shiftProduct->toAccept()) {
                    return redirect()->route('shift.view', ['shift' => $shift->id])->with('warning', 'Есть не принятые товары от пекаря');
                }
            }

            // Пока не заполнит инвентаризацию при открытии, не сможет закрыть смену
            if (!Inventory::open($shift->id)->first()) {
                return redirect()->route('shift-inventory.view', ['shift' => $shift->id])->with('warning', 'Необходимо заполнить сличительную ведомость при открытии');
            }

            // Пока не заполнит инвентаризацию при зыкрытие, не сможет закрыть смену

            if (!Inventory::close($shift->id)->first()) {
                return redirect()->route('shift-inventory.view', ['shift' => $shift->id, 'close' => '1'])->with('warning', 'Необходимо заполнить сличительную ведомость при закрытии');
            }
        }

        // Закрываем саму смену
        $now = Carbon::now();

        $shift->date_end = $now;
        $shift->user_id_end = Auth::user()->id;
        $shift->save();

        // Закрываем смену всем пользователям этой смены
        UserShift::where('shift_id', $shift->id)
            ->whereNull('date_end')
            ->update(['date_end' => $now]);

        return redirect()->route('shift.index')->with('success', 'Смена успешно закрыта');
    }

    public function view(Shift $shift)
    {
        // Если кассир открывает смену, то пока не заполнит инвентаризацию, не сможет смотреть смену

        if (Auth::user()->group->isCash && !Inventory::open($shift->id)->first()) {
            return redirect()->route('shift-inventory.view', ['shift' => $shift->id])->with('warning', 'Необходимо заполнить сличительную ведомость при открытии');
        }

        return view('shift.view', [
            'shift' => $shift
        ]);
    }

    public function enter(Shop $shop)
    {
        // Есть ли зачекиненые магазины у работника
        $userShift = UserShift::open()->with('Shift')->first();

        if ($userShift) {
            if ($userShift->shift) {
                return redirect()->route('shift.view', ['shift' => $userShift->shift->id]);
            } else {
                return redirect()->route('shift.shop', ['shop' => $userShift->shop_id])->with('warning', 'В этом магазине у Вас открытая смена');
            }
        }

        $shift = Shift::open($shop->id, Auth::user()->group->shiftType)->first();

        UserShift::create([
            'shift_id'   => $shift ? $shift->id : 0,
            'shop_id'    => $shop->id,
            'user_id'    => Auth::user()->id,
            'date_start' => Carbon::now()
        ]);

        return redirect()->route('shift.view', ['shift' => $shift->id])->with('success', 'Ваш приход отмечен');
    }

    public function exit(Shop $shop)
    {
        $userShift = UserShift::open($shop->id)->with('Shift')->first();

        if ($userShift) {
            $userShift->update([
                'date_end' => Carbon::now()
            ]);

            return redirect()->route('shift.index')->with('success', 'Ваш уход отмечен');
        }

        return redirect()->route('shift.index');
    }

    public function inventory(Shift $shift)
    {
        // Берем последнюю на этой точке
        $lastInventory = Inventory::last($shift->shop_id);

        // Мы пытамем заполнить сличилку при закрытии
        if ($lastInventory->isNotEmpty() && $lastInventory->first()->isOpen && !request()->has('close')) {
            return redirect()->route('shift.shop', ['shop' => $shift->shop_id]);
        }

        return view('shift.inventory', [
            'shift' => $shift
        ]);
    }
}
