<?php

namespace App\Http\Controllers\Import;

use App\DreamKas;
use App\Device;
use App\Http\Controllers\Controller;
use App\Product;
use App\DeviceCategory;
use App\ProductDevicePrice;
use App\ProductShopOrder;
use App\ProductShopPrice;
use App\ProductShopQuantity;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class DreamKasController extends Controller
{
    protected $host = 'https://kabinet.dreamkas.ru/api/';
    protected $token;
    protected $import_id;

    public function __construct($token, $import_id)
    {
        $this->token = $token;
        $this->import_id = $import_id;
    }


    public function run()
    {
        ini_set('memory_limit', '1G');

        $this->getDevice();
        $this->getProduct();
        $this->getOrder();
    }

    public function getProduct()
    {
        $api = new DreamKas($this->host, $this->token);

        foreach ($api->getProducts() as $product) {
            $data = Product::updateOrCreate(
                [
                    'uuid' => $product->id
                ],
                [
                    'name'               => $product->name,
                    'device_category_id' => isset($product->department) ? $product->department->id : NULL,
                    'type'               => $product->type,
                    'price'              => $product->price,
                    'import_id'          => $this->import_id
                ]
            );

            if (isset($product->department)) {
                DeviceCategory::updateOrCreate(
                    [
                        'name' => $product->department->name,
                        'import_id'  => $this->import_id,
                        'device_category_id'  => $product->department->id,
                    ]
                );
            }

            foreach ($product->prices as $price) {
                $device = Device::where('export_id', $price->deviceId)->get()->first();

                if ($device) {
                    ProductDevicePrice::updateOrCreate(
                        [
                            'product_id' => $data->id,
                            'device_id'  => $device->id,
                        ],
                        [
                            'price' => $price->value
                        ]
                    );

                    if ($device->shop_id) {
                        ProductShopPrice::updateOrCreate(
                            [
                                'product_id' => $data->id,
                                'shop_id'    => $device->shop_id,
                                'date'       => date('Y-m-d')
                            ],
                            [
                                'price' => $price->value
                            ]
                        );
                    }
                }
            }
        }

        Product::where('id', '>', 0)->update(['new' => 0, 'top' => 0]);

        foreach (Product::whereDate('created_at', '>=', Carbon::now()->subDays(7))->get() as $product) {
            $product->update(['new' => 1]);
        }
    }

    public function getOrder()
    {
        $api = new DreamKas($this->host, $this->token);

        $orders = $api->getOrders(Carbon::now()->subHours(4)->toDateTimeString());

        foreach ($orders->data as $order) {
            $payment = array_shift($order->payments);

            $device = Device::where('export_id', $order->deviceId)->get()->first();

            if ($device && $order->type == 'SALE') {
                foreach ($order->positions as $i => $position) {
                    $product = Product::where('uuid', $position->id)->get()->first();
                    $quantity = $position->quantity / 1000;
                    $shop_id = $device->shop_id;

                    if ($product && $shop_id) {
                        $date = new Carbon($order->localDate);

                        ProductShopOrder::updateOrCreate(
                            [
                                'device_id'  => $device->id,
                                'shop_id'    => $shop_id,
                                'product_id' => $product->id,
                                'date'       => $date->toDateTimeString(),
                                'position'   => $i
                            ],
                            [
                                'price'    => $position->price,
                                'quantity' => $quantity,
                                'amount'   => $position->amount,
                                'payment'  => $payment->type,
                                'discount' => isset($position->discount) ? $position->discount : 0,
                            ]
                        );

                        // Ищем такую же продажу на другом магазине, если есть удаляем
                        // Такое возможно когда то кассу привязали не к тому магазину

                        $productShopOrder = ProductShopOrder::where([
                            ['device_id', $device->id],
                            ['shop_id', '!=', $shop_id],
                            ['product_id', $product->id],
                            ['date', $order->localDate]
                        ])->get();

                        $productShopOrder->each(function ($item) {
                            $item->delete();
                        });
                    }
                }
            }
        }

        $products = ProductShopOrder::select('*', DB::raw('COUNT(*) as count'))
            ->whereDate('date', '>=', Carbon::now()->subDays(7))
            ->groupBy('product_id')
            ->orderBy('count', 'desc')
            ->limit(20)
            ->get();

        foreach ($products as $product) {
            $product->product->update(['top' => 1]);
        }
    }

    public function getDevice()
    {
        $api = new DreamKas($this->host, $this->token);

        foreach ($api->getDevices() as $device) {
            Device::updateOrCreate(
                [
                    'uuid' => $device->uuid,
                ],
                [
                    'export_id'  => $device->id,
                    'import_id'  => $this->import_id,
                    'name'       => $device->name,
                    'model_code' => $device->modelCode,
                    'model_name' => $device->modelName,
                ]
            );
        }
    }
}
