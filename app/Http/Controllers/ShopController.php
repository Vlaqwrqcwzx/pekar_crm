<?php

namespace App\Http\Controllers;

use App\Device;
use App\Import;
use App\Franchise;
use App\Shop;
use App\Sh5Corr;
use App\Sh5Depart;
use App\Sh5Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Validator;

class ShopController extends Controller
{
    public function index()
    {
        return view('shop.index', [
            'shops' => Shop::with('devices')->get()
        ]);
    }

    public function create()
    {
        return view('shop.form', [
            'shop'       => [],
            'imports'    => Import::all(),
            'franchises' => Franchise::all(),
            'devices'    => Device::where('shop_id', 0)->get(),
            'corrs'      => Sh5Corr::all(),
            'departs'    => Sh5Depart::all(),
            'sh5_settings' => [],
            'days'       => Carbon::getDays(),
            'action'     => route('shop.store')
        ]);
    }

    public function store(Request $request)
    {
        Validator::make($request->all(), [
            'name' => [
                'required',
            ]
        ])->validate();

        $shop = Shop::create($request->except(['device']));

        if ($request->has('device')) {
            Device::whereIn('id', $request->post('device'))->update([
                'shop_id' => $shop->id
            ]);
        }

        return redirect()
            ->route('shop.index')
            ->with('success', 'Магазин успешно добавлен');
    }

    public function edit(Shop $shop)
    {
        $import = Import::where('id', '=', $shop->import_id)->first();

        return view('shop.form', [
            'shop'       => $shop,
            'imports'    => Import::all(),
            'franchises' => Franchise::all(),
            'devices'    => Device::whereIn('shop_id', [0, $shop->id])->get(),
            'corrs'      => Sh5Corr::all(),
            'departs'    => $import != NULL ? Sh5Depart::where('setting_id', '=', $import->setting_id)->get() : [],
            'sh5_settings' => $import != NULL ? Sh5Setting::where('id', '=', $import->setting_id)->get() : [],
            'days'       => Carbon::getDays(),
            'action'     => route('shop.update', ['id' => $shop->id])
        ]);
    }

    public function update(Request $request, Shop $shop)
    {
        // echo '<pre>';
        // var_dump($shop, $request->all());die;
        Validator::make($request->all(), [
            'name' => [
                'required',
            ]
        ])->validate();

        $shop->update($request->except(['device']));

        Device::where('shop_id', $shop->id)->update(['shop_id' => 0]);

        if ($request->has('device')) {
            Device::whereIn('id', $request->post('device'))->update([
                'shop_id' => $shop->id
            ]);
        }

        return redirect()
            ->route('shop.index')
            ->with('success', 'Магазин успешно обновлен');
    }

    public function json()
    {
        $json = [
            'shops' => []
        ];

        if (request()->has('visible')) {
            $shops = Auth::user()->shopsVisible()->get();
        } elseif (request()->has('stock')) {
            $shops = Auth::user()->shopsStock()->get();
        } else {
            $shops = Shop::all();
        }

        foreach ($shops as $shop) {
            $json['shops'][] = [
                'id'   => $shop->id,
                'name' => $shop->name
            ];
        }

        return response()->json($json);
    }
}
