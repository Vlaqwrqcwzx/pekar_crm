<?php

namespace App\Http\Controllers;

use App\Import;
use App\Sh5Setting;
use App\Sh5SettingGroup;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ImportController extends Controller
{
    public function index()
    {
        return view('import.index', ['imports' => Import::all()]);
    }

    public function create()
    {
        return view('import.form', [
            'settings' => Sh5Setting::all(),
            'setting_groups' => Sh5SettingGroup::all(),
            'action'    => route('import.store')
        ]);
    }

    public function store(Request $request)
    {
        Validator::make($request->all(), [
            'name' => [
                'required',
                'max:100'
            ]
        ])->validate();

        Import::create([
            'name'   => $request->post('name'),
            'token'  => $request->post('token'),
            'status' => $request->post('status'),
            'setting_group_id' => $request->post('setting_group_id')
        ]);

        return redirect()
            ->route('import.index')
            ->with('success', 'Импорт успешно добавлен');
    }

    public function edit(Import $import)
    {
        return view('import.form', [
            'import' => $import,
            'settings' => Sh5Setting::all(),
            'setting_groups' => Sh5SettingGroup::all(),
            'action' => route('import.update', ['id' => $import->id])
        ]);
    }

    public function update(Request $request, Import $import)
    {
        Validator::make($request->all(), [
            'name' => [
                'required',
                'max:100'
            ]
        ])->validate();

        $import->update([
            'name'   => $request->post('name'),
            'token'  => $request->post('token'),
            'status' => $request->post('status'),
            'setting_group_id' => $request->post('setting_group_id'),
        ]);

        return redirect()
            ->route('import.index')
            ->with('success', 'Импорт успешно обновлен');
    }
}
