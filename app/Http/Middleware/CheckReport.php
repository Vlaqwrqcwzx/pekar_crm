<?php

namespace App\Http\Middleware;

use Closure;
use App\UserGroup;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

class CheckReport
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $currentRouteName = Route::currentRouteName();
        
        $user_group = Auth::user()->group;
        $group_menu = (array)unserialize($user_group->menu);

        if (in_array($currentRouteName, $group_menu))
            return $next($request);
         else
            return redirect()->route('home');
    }
}
