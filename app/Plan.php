<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{
    public const TYPES = [
        'baker'        => 'Пекарь',
        'confectioner' => 'Кондитер'
    ];

    protected $guarded = [];

    public function getTypeNameAttribute()
    {
        return isset(self::TYPES[$this->type]) ? self::TYPES[$this->type] : '';
    }

    public function shop()
    {
        return $this->belongsTo('App\Shop');
    }

    public function products()
    {
        return $this->hasMany('App\PlanProduct');
    }
}
