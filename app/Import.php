<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Import extends Model
{
    protected $guarded = [];

    public function devices()
    {
        return $this->hasMany('App\Device');
    }

    public function sh5_settings()
    {
        return $this->hasMany('App\Sh5Setting');
    }

}
