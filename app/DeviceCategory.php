<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeviceCategory extends Model
{
    protected $table = 'device_categories';
    protected $fillable = [
        'name',
        'category_id',
        'import_id',
        'device_category_id'
    ];
}
