<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InventoryProduct extends Model
{
    protected $guarded = [];

    public function inventory()
    {
        return $this->belongsTo('App\Inventory');
    }

    public function product()
    {
        return $this->belongsTo('App\Product');
    }
}
