<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Device extends Model
{
    public $timestamps = false;
    protected $guarded = [];

    public function imports()
    {
        return $this->belongsTo('App\Import');
    }
}
