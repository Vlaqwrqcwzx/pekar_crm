<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductShopOrder extends Model
{
    public $timestamps = false;
    public $dates = ['date'];

    protected $guarded = [];

    public function product()
    {
        return $this->belongsTo('App\Product');
    }
}
