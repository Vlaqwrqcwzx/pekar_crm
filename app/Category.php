<?php

namespace App;

class Category extends ModelWithFilter
{
    protected $guarded = [];

    public function products()
    {
        return $this->hasMany('App\Product');
    }
}
