<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sh5WaybillProduct extends Model
{

    public $timestamps = true;

    protected $fillable = [
        'id',
        'name',
        'guid',
        'rid',
        'price',
        'amount',
        'quantity',
        'base_unit',
        'waybill_id'
    ];
}
