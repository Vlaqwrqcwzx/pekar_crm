<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inventory extends Model
{
    protected $guarded = [];

    const OPEN = 'open';
    const CLOSE = 'close';

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function shop()
    {
        return $this->belongsTo('App\Shop');
    }

    public function shift()
    {
        return $this->belongsTo('App\Shift');
    }

    public function inventoryProducts()
    {
        return $this->hasMany('App\InventoryProduct');
    }

    public function scopeOpen($query, $shift_id)
    {
        return $query->where('shift_id', $shift_id)
            ->where('type', self::OPEN)
            ->get();
    }

    public function scopeClose($query, $shift_id)
    {
        return $query->where('shift_id', $shift_id)
            ->where('type', self::CLOSE)
            ->get();
    }

    public function scopeLast($query, $shop_id)
    {
        return $query->where('shop_id', $shop_id)
            ->orderBy('created_at', 'desc')
            ->limit(1)
            ->get();
    }

    public function getIsOpenAttribute()
    {
        return $this->type == self::OPEN;
    }

    public function getIsCloseAttribute()
    {
        return $this->type == self::CLOSE;
    }

    public function getName()
    {
        return sprintf(
            'Сличительная ведомасть #%s %s - %s (%s)',
            $this->id, $this->shop ? $this->shop->name : '',
            $this->isOpen ? 'При открытии' : 'При закрытии',
            $this->user ? $this->user->name : 'Автоматическая'
        );
    }
}
