<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $guarded = [];

    public function getName()
    {
        return sprintf('%s, %s', $this->name, $this->getType() == 'countable' ? 'шт.' : 'кг.');
    }

    public function getPrice()
    {
        return ($this->shopPrice ? $this->shopPrice->price : $this->price) / 100;
    }

    public function getQuantity()
    {
        return (float)($this->shopQuantity ? (float)$this->shopQuantity->quantity : 0);
    }

    public function getCategory()
    {
        return isset($this->category) ? $this->category->name : '';
    }

    public function getColor()
    {
        return isset($this->category) ? $this->category->color : '#e0e0e0';
    }

    public function getSort()
    {
        return isset($this->category) ? $this->category->sort : 0;
    }

    public function getType()
    {
        return ($this->type == 'COUNTABLE') ? 'countable' : 'scalable';
    }

    public function isCountable()
    {
        return $this->getType() === 'countable';
    }

    public function isScalable()
    {
        return $this->getType() === 'scalable';
    }

    public function children()
    {
        return $this->belongsTo('App\Product', 'parent_id', 'id');
    }

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function planProducts()
    {
        return $this->hasMany('App\PlanProduct');
    }

    public function shiftProducts()
    {
        return $this->hasMany('App\ShiftProduct');
    }

    public function rejectProducts()
    {
        return $this->hasMany('App\RejectProduct');
    }

    public function movementProducts()
    {
        return $this->hasMany('App\MovementProduct');
    }

    public function movementProductsOut()
    {
        return $this->hasMany('App\MovementProduct');
    }

    public function movementProductsIn()
    {
        return $this->hasMany('App\MovementProduct');
    }

    public function inventoryProducts()
    {
        return $this->hasMany('App\InventoryProduct');
    }

    public function lastInventoryProducts()
    {
        return $this->hasOne('App\InventoryProduct');
    }

    public function shopOrders()
    {
        return $this->hasMany('App\ProductShopOrder');
    }

    public function shopPrice()
    {
        return $this->hasOne('App\ProductShopPrice');
    }

    public function shopPrices()
    {
        return $this->hasMany('App\ProductShopPrice');
    }

    public function shopQuantity()
    {
        return $this->hasOne('App\ProductShopQuantity');
    }

    public function shopQuantityToday()
    {
        return $this->hasMany('App\ProductShopQuantity');
    }

    public function shopQuantityYesterday()
    {
        return $this->hasMany('App\ProductShopQuantity');
    }

    public static function getAction($date, $shop_id = 0)
    {
        return self::visibleShop()->with([
            'category',
            'shopQuantityToday' => function($query) use ($date, $shop_id) {
                $query->whereDate('date', $date);

                if ($shop_id) {
                    $query->where('shop_id', $shop_id);
                }
            },
            'shopQuantityYesterday' => function($query) use ($date, $shop_id) {
                $query->whereDate('date', $date->copy()->subDay());

                if ($shop_id) {
                    $query->where('shop_id', $shop_id);
                }
            },
            'shopOrders' => function($query) use ($date, $shop_id) {
                $query->whereDate('date', $date)
                    ->orderBy('date');

                if ($shop_id) {
                    $query->where('shop_id', $shop_id);
                }
            },
            'shiftProducts' => function($query) use ($date, $shop_id) {
                $query->whereDate('updated_at', $date)
                    ->orderBy('updated_at');

                if ($shop_id) {
                    $query->where('shop_id', $shop_id);
                }
            },
            'rejectProducts' => function($query) use ($date, $shop_id) {
                $query->whereDate('created_at', $date)
                    ->orderBy('created_at')
                    ->with('reject');

                if ($shop_id) {
                    $query->where('shop_id', $shop_id);
                }
            },
            'movementProducts' => function($query) use ($date) {
                $query->whereDate('updated_at', $date)
                    ->orderBy('updated_at')
                    ->with('movement');
            },
            'inventoryProducts' => function($query) use ($date, $shop_id) {
                $query->whereDate('created_at', $date)
                    ->with('inventory')
                    ->orderBy('created_at');

                if ($shop_id) {
                    $query->where('shop_id', $shop_id);
                }
            }
        ])->orderBy('name')->get();
    }

    public static function getShiftAction($shift)
    {
        $products = self::visibleShop()->with([
            'category',
            'shopPrice'             => function ($query) use ($shift) {
                $query->where('shop_id', $shift->shop_id)
                    ->whereDate('date', $shift->date_start);
            },
            'shopQuantity'          => function ($query) use ($shift) {
                $query->whereDate('created_at', '<=', $shift->date_start)
                    ->where('shop_id', $shift->shop_id)
                    ->orderBy('created_at')
                    ->limit(1);
            },
            'shiftProducts'         => function ($query) use ($shift) {
                $query->whereBetween('updated_at', [$shift->date_start->toDateTimeString(), $shift->getDateEnd()->toDateTimeString()])
                    ->where('shop_id', $shift->shop_id)
                    ->whereIn('status', [2, 3]);
            },
            'shopOrders'            => function ($query) use ($shift) {
                $query->whereBetween('date', [$shift->date_start->toDateTimeString(), $shift->getDateEnd()->toDateTimeString()])
                    ->where('shop_id', $shift->shop_id);
            },
            'movementProducts'   => function ($query) use ($shift) {
                $query->whereBetween('updated_at', [$shift->date_start->toDateTimeString(), $shift->getDateEnd()->toDateTimeString()])
                    ->where(function ($query) use ($shift) {
                        $query->where('from_shop_id', $shift->shop_id)->orWhere('to_shop_id', $shift->shop_id);
                    })
                    ->orderBy('updated_at')
                    ->with('movement');
            },
            'rejectProducts'        => function ($query) use ($shift) {
                $query->whereBetween('created_at', [$shift->date_start->toDateTimeString(), $shift->getDateEnd()->toDateTimeString()])
                    ->where('shop_id', $shift->shop_id)
                    ->with('reject');
            },
            'inventoryProducts'      => function ($query) use ($shift) {
                $query->whereIn('inventory_id', $shift->inventory->pluck('id'))
                    ->with('inventory');
            }
        ])->orderBy('name');

        if ($shift->shop) {
            $products->where('import_id', $shift->shop->import_id);
        }

        return $products->get();
    }

    public function scopeVisibleShop($query)
    {
        return $query->whereRaw('category_id IN (SELECT id FROM categories WHERE shop = 1)');
    }
}
