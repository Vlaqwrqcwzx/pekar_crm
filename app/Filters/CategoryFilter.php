<?php

namespace App\Filters;

class CategoryFilter extends AbstractFilter
{
    protected $sorts = ['name'];
    protected $sort = 'name';

    public function filter_name($value)
    {
        return $this->builder->where('name', 'like', '%' . $value . '%');
    }
}