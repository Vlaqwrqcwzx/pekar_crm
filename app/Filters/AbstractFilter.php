<?php

namespace App\Filters;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;

abstract class AbstractFilter
{
    protected $builder;
    protected $request;
    protected $filters = [];
    protected $sorts = [];
    protected $sort = '';
    protected $order = 'desc';

    public function __construct(Builder $builder, Request $request)
    {
        $this->builder = $builder;
        $this->request = $request;
    }

    public function applyFilter()
    {
        foreach($this->request->all() as $filter => $value) {
            if (method_exists($this, $filter)) {
                $this->{$filter}($value);
                $this->filters[$filter] = $value;
            }
        }
    }

    public function getFilters()
    {
        return $this->filters;
    }

    public function getSorts()
    {
        $sorts = [];

        foreach ($this->sorts as $sort) {
            $sorts[$sort] = array_merge(
                $this->filters,
                ['sort'  => $sort],
                ['order' => $this->order == 'desc' ? 'asc' : 'desc']
            );
        }

        return $sorts;
    }

    public function getBuilder()
    {
        return $this->builder;
    }

    public function sort($value) {
        if (in_array($value, $this->sorts)) {
            $this->sort = $value;
            $this->order = $this->request->get('order') == 'desc' ? 'desc' : 'asc';

            $this->builder->orderBy($this->sort, $this->order);
        }
    }

    public function order($value) {
        $this->order = $value;
    }

    public function page($value) {}
}