<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Reject extends Model
{
    protected $guarded = [];

    public const TYPES = [
        1 => 'Брак',
        2 => 'Бракераж',
        3 => 'Списание'
    ];

    public const PLACES = [
        'kitchen' => 'Кухня',
        'shop'    => 'Касса',
        'accept'  => 'Приемка'
    ];

    public function getPlace()
    {
        return isset(self::PLACES[$this->place]) ? self::PLACES[$this->place] : '-';
    }

    public static function addReject($shift, $type, $products, $place = 'shop')
    {
        $reject = Reject::create([
            'shift_id' => $shift->id,
            'shop_id'  => $shift->shop_id,
            'user_id'  => Auth::user()->id,
            'type'     => $type,
            'place'    => $place
        ]);

        $reject->code = sprintf('%s-%s', mb_substr($reject->id . date('His'), 0, 6), rand(111111, 999999));
        $reject->save();

        foreach ($products as $product) {
            // Если есть этот параметр, то значит брак был с какого то выпуска
            if (isset($product['id'])) {
                $shift_product_id = $product['id'];

                $shiftProduct = ShiftProduct::findOrFail($shift_product_id);
                $shiftProduct->update([
                    'reject' . $type   => $shiftProduct->{'reject' . $type} + $product['quantity']
                ]);
            } else {
                $shift_product_id = 0;
            }

            RejectProduct::create([
                'reject_id'        => $reject->id,
                'product_id'       => $product['product_id'],
                'shift_product_id' => $shift_product_id,
                'shop_id'          => $shift->shop_id,
                'place'            => $place,
                'quantity'         => $product['quantity']
            ]);
        }
    }

    public function getTypeNameAttribute()
    {
        return isset(self::TYPES[$this->type]) ? self::TYPES[$this->type] : '';
    }

    public function rejectProducts()
    {
        return $this->hasMany('App\RejectProduct');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function shop()
    {
        return $this->belongsTo('App\Shop');
    }

    public function shift()
    {
        return $this->belongsTo('App\Shift');
    }
}
