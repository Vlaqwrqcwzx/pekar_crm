<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Franchise extends Model
{
    protected $guarded = [];

    public function shops()
    {
        return $this->hasMany('App\Shop');
    }
}
