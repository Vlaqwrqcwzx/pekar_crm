<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RejectProduct extends Model
{
    protected $guarded = [];

    public function product()
    {
        return $this->belongsTo('App\Product');
    }

    public function reject()
    {
        return $this->belongsTo('App\Reject');
    }

    public function getIsShopAttribute()
    {
        return $this->place == 'shop';
    }

    public function getIsKitchenAttribute()
    {
        return $this->place == 'kitchen';
    }

    public function getIsAcceptAttribute()
    {
        return $this->place == 'accept';
    }

    /**
     * Какое списание мы учитываем в расчете кол-ва
     * Если было у кассира
     * Или бракераж на приемке товара
     */
    public function getIsCalcAttribute()
    {
        return $this->isShop || ($this->isAccept && $this->reject->type == 2);
    }
}
