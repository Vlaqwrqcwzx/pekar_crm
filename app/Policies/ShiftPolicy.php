<?php

namespace App\Policies;

use App\Inventory;
use App\User;
use App\UserShift;
use App\Shift;
use Illuminate\Auth\Access\HandlesAuthorization;

class ShiftPolicy
{
    use HandlesAuthorization;

    public function view(User $user, Shift $shift)
    {
        // Если пользователь не может открывать смены, то и просматривать тоже не может

        if (!$user->group->canOpenShift) {
            return false;
        }

        // Если открытая смена совпадает с текущей, то разрешаем

        $userShift = UserShift::open()->first();

        if ($userShift && $userShift->shift_id == $shift->id) {
            return true;
        }

        return false;
    }

    public function close(User $user, Shift $shift)
    {
        // Если пользователь не может открывать смены, то и закрывать тоже не может

        if (!$user->group->canOpenShift) {
            return false;
        }

        // Если смена уже закрыта, то больше ее не нужно закрывать

        if (!in_array($shift->date_end, ['0000-00-00 00:00:00', ''])) {
            return false;
        }

        // Если открытая смена совпадает с текущей, то разрешаем

        $userShift = UserShift::open()->first();

        if ($userShift && $userShift->shift_id == $shift->id) {
            return true;
        }

        return false;
    }

    public function inventory(User $user, Shift $shift)
    {
        // Только кассир может проводить инвентаризацию

        if ($user->group->isCash) {
            // Если за смену уже были 2 инвентаризации то, не даем больше

            if (Inventory::where('shift_id', $shift->id)->count() == 2) {
                return false;
            }

            return true;
        } else {
            return false;
        }
    }
}
