<?php

namespace App\Policies;

use App\User;
use App\Shop;
use Illuminate\Auth\Access\HandlesAuthorization;

class ShopPolicy
{
    use HandlesAuthorization;

    public function view(User $user, Shop $shop)
    {
        foreach ($user->shopsVisible as $s) {
            if ($s->id == $shop->id) {
                return true;
            }
        }

        return false;
    }

    public function open(User $user, Shop $shop)
    {
        return $user->group->canOpenShift;
    }

    public function enter(User $user, Shop $shop)
    {
        return true;
    }
}
