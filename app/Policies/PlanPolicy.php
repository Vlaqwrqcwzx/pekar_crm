<?php

namespace App\Policies;

use App\User;
use App\Plan;
use Illuminate\Auth\Access\HandlesAuthorization;

class PlanPolicy
{
    use HandlesAuthorization;

    public function view(User $user, Plan $plan)
    {
        foreach ($user->shopsVisible as $s) {
            if ($s->id == $plan->shop->id) {
                return true;
            }
        }
    }

    public function update(User $user, Plan $plan)
    {
        //
    }

    public function delete(User $user, Plan $plan)
    {
        //
    }
}
