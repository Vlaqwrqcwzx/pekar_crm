<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shop extends Model
{
    protected $guarded = [];

    public function devices()
    {
        return $this->hasMany('App\Device');
    }

    public function plans()
    {
        return $this->hasMany('App\Plan');
    }

    public function shifts()
    {
        return $this->hasMany('App\Shift');
    }

    public function shiftProducts()
    {
        return $this->hasMany('App\ShiftProduct');
    }

    public function shopOrders()
    {
        return $this->hasMany('App\ProductShopOrder');
    }

    public function from_depart_one()
    {
        return $this->hasOne('App\Sh5Depart', 'rid', 'depart_rid_1');
    }

    public function from_depart_two()
    {
        return $this->hasOne('App\Sh5Depart', 'rid', 'depart_rid_2');
    }

    public function to_depart()
    {
        return $this->hasOne('App\Sh5Depart', 'rid', 'depart_rid_2');
    }

    public function shopQuantity()
    {
        return $this->hasMany('App\ProductShopQuantity');
    }

    public function rejects()
    {
        return $this->hasMany('App\Reject');
    }

    public function rejectProducts()
    {
        return $this->hasMany('App\RejectProduct');
    }

    public function movements()
    {
        return $this->hasMany('App\Movement');
    }

    public function movementProductsMinus()
    {
        return $this->hasMany('App\MovementProduct', 'from_shop_id');
    }

    public function movementProductsPlus()
    {
        return $this->hasMany('App\MovementProduct', 'to_shop_id');
    }

    public function inventories()
    {
        return $this->hasMany('App\Inventory');
    }

    public function inventoryProducts()
    {
        return $this->hasMany('App\InventoryProduct');
    }
}
