<?php

namespace App;

class DreamKas
{
    private $host;
    private $token;

    public function __construct($host, $token)
    {
        $this->host = $host;
        $this->token = $token;
    }

    public function getProducts()
    {
        return $this->request('v2/products?limit=1000');
    }

    public function getOrders($from = '', $to = '')
    {
        return $this->request(sprintf('receipts?limit=1000&from=%s&to=%s', urlencode($from), urlencode($to)));
    }

    public function getShops()
    {
        return $this->request('shops?limit=1000');
    }

    public function getDevices()
    {
        return $this->request('devices?limit=1000');
    }

    private function request($method, $data = array())
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Authorization: Bearer ' . $this->token
        ));

        curl_setopt($ch, CURLOPT_URL, $this->host . $method);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        if ($data) {
            curl_setopt($ch, CURLOPT_POST, TRUE);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        }

        $response = curl_exec($ch);
        curl_close($ch);

        return json_decode($response);
    }
}
