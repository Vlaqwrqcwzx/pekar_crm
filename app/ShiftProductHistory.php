<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShiftProductHistory extends Model
{
    protected $guarded = [];

    public function shiftProduct()
    {
        return $this->belongsTo('App\ShiftProduct');
    }

    public function isAccepted()
    {
        return $this->status == 3;
    }

    public function isCompleted()
    {
        return $this->status == 2;
    }
}
