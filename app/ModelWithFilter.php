<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

abstract class ModelWithFilter extends Model
{
    protected $filter = '';
    private $filters = [];
    private $sorts = [];

    public function scopeFilter(Builder $builder)
    {
        if ($this->filter) {
            $class = sprintf('App\Filters\%s', $this->filter);

            if (class_exists($class)) {
                $filter = new $class($builder, request());
                $filter->applyFilter();
                $this->filters = $filter->getFilters();
                $this->sorts = $filter->getSorts();
            }
        }

        return $builder;
    }

    public function scopeGetFilters()
    {
        return $this->filters;
    }

    public function scopeGetSorts()
    {
        return $this->sorts;
    }
}