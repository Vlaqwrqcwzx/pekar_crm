<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sh5Setting extends Model
{
    protected $guarded = [];

    public function imports()
    {
        return $this->belongsTo('App\Import');
    }
}
