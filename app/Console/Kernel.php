<?php

namespace App\Console;

use App\Http\Controllers\Cron\ImportController;
use App\Http\Controllers\Cron\QuantityController;
use App\Http\Controllers\Cron\ShiftController;

use App\Product;
use App\Import;
use App\Sh5Setting;

use Carbon\Carbon;

use App\Http\Controllers\Api\StoreHouse5\ApiProductGroupController;
use App\Http\Controllers\Api\StoreHouse5\ApiProductController;
use App\Http\Controllers\Api\StoreHouse5\ApiInvoicesController;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {

        // STOREHOUSE5
        // Подгрузка папок номенклатур
        $schedule->call(function () {

            $settings = Sh5Setting::all();

            $api_product_group = new ApiProductGroupController();

            foreach($settings as $setting) {

                $api_product_group->getGroups($setting->ip, $setting->port, $setting->username, $setting->password);

            }

        })->dailyAt('06:00');
        // });

        // Отгрузка товаров в StoreHouse5
        $schedule->call(function () {

            $api_products = new ApiProductController();

            $imports = Import::all();

            foreach($imports as $import) {

                $products = Product::select('products.name', 'products.type', 'products.id',
                'products.sh5_guid', 'products.base_unit', 'imports.setting_group_id',
                'imports.setting_id', 'sh5_setting_groups.rid')
                    ->join('imports', function($join) use($import) {
                        $join->on('imports.id', '=', 'products.import_id')
                        ->where('imports.id', '=', $import->id);
                    })
                    ->join('sh5_setting_groups', function($join) {
                        $join->on('sh5_setting_groups.id', '=', 'imports.setting_group_id');
                    })
                ->get();

                $setting = Sh5Setting::find($products[0]->setting_id);

                if ($api_products->Goods($products, $setting, $import->id) == "NOTHING") {

                    continue;

                } else {

                    $products = $api_products->Goods($products, $setting, $import->id);

                    return $api_products->postProducts($products, $setting);
                }

            }

        })->dailyAt('06:05');

        // Отгрузка накладных в StoreHouse5

        // Накладные по Кухне

        // Комплектация (Кухни)
        $schedule->call(function () {

            $date_interval = [
                Carbon::yesterday(),
                Carbon::today()
                // Carbon::create("2020-07-21 00:00:00"),
                // Carbon::create("2020-07-22 00:00:00"),
            ];

            $params = array(
                "proc_name"           => "InsGDoc12",
                "harakter"            => "А",
                "type"                => "comp",
                "in_shift"            => true,
                "worker"              => "kitchen",
                "payment"             => NULL,
                "date_interval"       => $date_interval,
                "created_at"          => NULL,
                "shop_id"             => 0,
                "shift_id"            => 0,
                "reject_id"           => 0,
                "movement_id"         => 0,
                "to_shop_id"          => 0,
                "inventory_id"        => 0,
                "shift_products_id"   => 0,
                "depart_rid"          => NULL,
                "corr_rid"            => NULL
            );

            $api_invoices = new ApiInvoicesController();

            $api_invoices->complectation($params);

        })->dailyAt('06:10');
        // });

        // Перемещения

        // Перемещение кухни из точки на склад другой точки
        $schedule->call(function () {

            $date_interval = [
                Carbon::yesterday(),
                Carbon::today()
                // Carbon::create("2020-07-21 00:00:00"),
                // Carbon::create("2020-07-22 00:00:00"),
            ];

            $params = array(
                "proc_name"           => "InsGDoc11",
                "harakter"            => "А",
                "type"                => "move_other",
                "in_shift"            => true,
                "worker"              => "kitchen",
                "payment"             => NULL,
                "date_interval"       => $date_interval,
                "created_at"          => NULL,
                "shop_id"             => 0,
                "shift_id"            => 0,
                "reject_id"           => 0,
                "movement_id"         => 0,
                "to_shop_id"          => 0,
                "inventory_id"        => 0,
                "shift_products_id"   => 0,
                "depart_rid"          => NULL,
                "corr_rid"            => NULL
            );

            $api_invoices = new ApiInvoicesController();

            $api_invoices->computed_movements_other_shop($params);

        })->dailyAt('06:15');
        // });

        // Внутреннее перемещение кухни (то что принял кассир)
        $schedule->call(function () {

            $date_interval = [
                Carbon::yesterday(),
                Carbon::today()
                // Carbon::create("2020-07-21 00:00:00"),
                // Carbon::create("2020-07-22 00:00:00"),
            ];

            $params = array(
                "proc_name"           => "InsGDoc11",
                "harakter"            => "А",
                "type"                => "move",
                "in_shift"            => true,
                "worker"              => "kitchen",
                "payment"             => NULL,
                "date_interval"       => $date_interval,
                "created_at"          => NULL,
                "shop_id"             => 0,
                "shift_id"            => 0,
                "reject_id"           => 0,
                "movement_id"         => 0,
                "to_shop_id"          => 0,
                "inventory_id"        => 0,
                "shift_products_id"   => 0,
                "depart_rid"          => NULL,
                "corr_rid"            => NULL
            );

            $api_invoices = new ApiInvoicesController();

            $api_invoices->computed_movements($params);

        })->dailyAt('06:20');
        // });

        // Брак / Бракераж / Списание (Кухни)

        // Брак кухни
        $schedule->call(function () {

            $date_interval = [
                Carbon::yesterday(),
                Carbon::today()
                // Carbon::create("2020-07-21 00:00:00"),
                // Carbon::create("2020-07-22 00:00:00"),
            ];

            $params = array(
                "proc_name"           => "InsGDoc4",
                "harakter"            => "А",
                "type"                => "1",
                "in_shift"            => true,
                "worker"              => "kitchen",
                "payment"             => NULL,
                "date_interval"       => $date_interval,
                "created_at"          => NULL,
                "shop_id"             => 0,
                "shift_id"            => 0,
                "reject_id"           => 0,
                "movement_id"         => 0,
                "to_shop_id"          => 0,
                "inventory_id"        => 0,
                "shift_products_id"   => 0,
                "depart_rid"          => NULL,
                "corr_rid"            => NULL
            );

            $api_invoices = new ApiInvoicesController();

            $api_invoices->computed_rejects($params);

        })->dailyAt('06:25');
        // });

        // Бракераж кухни
        $schedule->call(function () {

            $date_interval = [
                Carbon::yesterday(),
                Carbon::today()
                // Carbon::create("2020-07-21 00:00:00"),
                // Carbon::create("2020-07-22 00:00:00"),
            ];

            $params = array(
                "proc_name"           => "InsGDoc4",
                "harakter"            => "А",
                "type"                => "2",
                "in_shift"            => true,
                "worker"              => "kitchen",
                "payment"             => NULL,
                "date_interval"       => $date_interval,
                "created_at"          => NULL,
                "shop_id"             => 0,
                "shift_id"            => 0,
                "reject_id"           => 0,
                "movement_id"         => 0,
                "to_shop_id"          => 0,
                "inventory_id"        => 0,
                "shift_products_id"   => 0,
                "depart_rid"          => NULL,
                "corr_rid"            => NULL
            );

            $api_invoices = new ApiInvoicesController();

            $api_invoices->computed_rejects($params);

        })->dailyAt('06:30');
        // });

        // Накладные по Кассе

        // Сличительная ведомость
        // При открытии смены
        $schedule->call(function () {

            $date_interval = [
                Carbon::yesterday(),
                Carbon::today()
                // Carbon::create("2020-07-21 00:00:00"),
                // Carbon::create("2020-07-22 00:00:00"),
            ];

            $params = array(
                "proc_name"           => "InsGDoc8",
                "harakter"            => "А",
                "type"                => "open",
                "in_shift"            => true,
                "worker"              => "cash",
                "payment"             => NULL,
                "date_interval"       => $date_interval,
                "created_at"          => NULL,
                "shop_id"             => 0,
                "shift_id"            => 0,
                "reject_id"           => 0,
                "movement_id"         => 0,
                "to_shop_id"          => 0,
                "inventory_id"        => 0,
                "shift_products_id"   => 0,
                "depart_rid"          => NULL,
                "corr_rid"            => NULL
            );

            $api_invoices = new ApiInvoicesController();

            $api_invoices->inventories($params);

        })->dailyAt('06:35');
        // });

        // Перемещение кассиром из точки на склад другой точки
        $schedule->call(function () {

            $date_interval = [
                Carbon::yesterday(),
                Carbon::today()
                // Carbon::create("2020-07-21 00:00:00"),
                // Carbon::create("2020-07-22 00:00:00"),
            ];

            $params = array(
                "proc_name"           => "InsGDoc11",
                "harakter"            => "А",
                "type"                => "move_other",
                "in_shift"            => true,
                "worker"              => "cash",
                "payment"             => NULL,
                "date_interval"       => $date_interval,
                "created_at"          => NULL,
                "shop_id"             => 0,
                "shift_id"            => 0,
                "reject_id"           => 0,
                "movement_id"         => 0,
                "to_shop_id"          => 0,
                "inventory_id"        => 0,
                "shift_products_id"   => 0,
                "depart_rid"          => NULL,
                "corr_rid"            => NULL
            );

            $api_invoices = new ApiInvoicesController();

            $api_invoices->computed_movements_other_shop($params);

        })->dailyAt('06:40');
        // });


        // Брак / Бракераж / Списание магазина

        // Списание магазина
        $schedule->call(function () {

            $date_interval = [
                Carbon::yesterday(),
                Carbon::today()
                // Carbon::create("2020-07-21 00:00:00"),
                // Carbon::create("2020-07-22 00:00:00"),
            ];

            $params = array(
                "proc_name"           => "InsGDoc4",
                "harakter"            => "А",
                "type"                => "3",
                "in_shift"            => true,
                "worker"              => "shop",
                "payment"             => NULL,
                "date_interval"       => $date_interval,
                "created_at"          => NULL,
                "shop_id"             => 0,
                "shift_id"            => 0,
                "reject_id"           => 0,
                "movement_id"         => 0,
                "to_shop_id"          => 0,
                "inventory_id"        => 0,
                "shift_products_id"   => 0,
                "depart_rid"          => NULL,
                "corr_rid"            => NULL
            );

            $api_invoices = new ApiInvoicesController();

            $api_invoices->computed_rejects($params);

        })->dailyAt('06:45');
        // });

        // Брак магазина
        $schedule->call(function () {

            $date_interval = [
                Carbon::yesterday(),
                Carbon::today()
                // Carbon::create("2020-07-21 00:00:00"),
                // Carbon::create("2020-07-22 00:00:00"),
            ];

            $params = array(
                "proc_name"           => "InsGDoc4",
                "harakter"            => "А",
                "type"                => "1",
                "in_shift"            => true,
                "worker"              => "shop",
                "payment"             => NULL,
                "date_interval"       => $date_interval,
                "created_at"          => NULL,
                "shop_id"             => 0,
                "shift_id"            => 0,
                "reject_id"           => 0,
                "movement_id"         => 0,
                "to_shop_id"          => 0,
                "inventory_id"        => 0,
                "shift_products_id"   => 0,
                "depart_rid"          => NULL,
                "corr_rid"            => NULL
            );

            $api_invoices = new ApiInvoicesController();

            $api_invoices->computed_rejects($params);

        })->dailyAt('06:55');
        // });

        // Бракераж магазина
        $schedule->call(function () {

            $date_interval = [
                Carbon::yesterday(),
                Carbon::today()
                // Carbon::create("2020-07-21 00:00:00"),
                // Carbon::create("2020-07-22 00:00:00"),
            ];

            $params = array(
                "proc_name"           => "InsGDoc4",
                "harakter"            => "А",
                "type"                => "2",
                "in_shift"            => true,
                "worker"              => "shop",
                "payment"             => NULL,
                "date_interval"       => $date_interval,
                "created_at"          => NULL,
                "shop_id"             => 0,
                "shift_id"            => 0,
                "reject_id"           => 0,
                "movement_id"         => 0,
                "to_shop_id"          => 0,
                "inventory_id"        => 0,
                "shift_products_id"   => 0,
                "depart_rid"          => NULL,
                "corr_rid"            => NULL
            );

            $api_invoices = new ApiInvoicesController();

            $api_invoices->computed_rejects($params);

        })->dailyAt('07:00');
        // });

        // Расходные накладные
        // Наличные в смену
        $schedule->call(function () {

            $date_interval = [
                Carbon::yesterday(),
                Carbon::today()
                // Carbon::create("2020-07-21 00:00:00"),
                // Carbon::create("2020-07-22 00:00:00"),
            ];

            $api_invoices = new ApiInvoicesController();

            $params = array(
                "proc_name"           => "InsGDoc4",
                "harakter"            => "А",
                "type"                => "р/н",
                "in_shift"            => true,
                "worker"              => "cash",
                "payment"             => "CASH",
                "date_interval"       => $date_interval,
                "created_at"          => NULL,
                "shop_id"             => 0,
                "shift_id"            => 0,
                "reject_id"           => 0,
                "movement_id"         => 0,
                "to_shop_id"          => 0,
                "inventory_id"        => 0,
                "shift_products_id"   => 0,
                "depart_rid"          => NULL,
                "corr_rid"            => NULL
            );

            $api_invoices->computed_shifts($params);

        })->dailyAt('07:05');
        // });

        // Эквайринг в смену
        $schedule->call(function () {

            $date_interval = [
                Carbon::yesterday(),
                Carbon::today()
                // Carbon::create("2020-07-21 00:00:00"),
                // Carbon::create("2020-07-22 00:00:00"),
            ];

            $api_invoices = new ApiInvoicesController();

            $params = array(
                "proc_name"           => "InsGDoc4",
                "harakter"            => "А",
                "type"                => "р/н",
                "in_shift"            => true,
                "worker"              => "cash",
                "payment"             => "CASHLESS",
                "date_interval"       => $date_interval,
                "created_at"          => NULL,
                "shop_id"             => 0,
                "shift_id"            => 0,
                "reject_id"           => 0,
                "movement_id"         => 0,
                "to_shop_id"          => 0,
                "inventory_id"        => 0,
                "shift_products_id"   => 0,
                "depart_rid"          => NULL,
                "corr_rid"            => NULL
            );

            $api_invoices->computed_shifts($params);

        })->dailyAt('07:10');
        // });

        // Сличительная ведомость
        // При закрытии смены
        $schedule->call(function () {

            $date_interval = [
                Carbon::yesterday(),
                Carbon::today()
                // Carbon::create("2020-07-21 00:00:00"),
                // Carbon::create("2020-07-22 00:00:00"),
            ];

            $params = array(
                "proc_name"           => "InsGDoc8",
                "harakter"            => "А",
                "type"                => "close",
                "in_shift"            => true,
                "worker"              => "cash",
                "payment"             => NULL,
                "date_interval"       => $date_interval,
                "created_at"          => NULL,
                "shop_id"             => 0,
                "shift_id"            => 0,
                "reject_id"           => 0,
                "movement_id"         => 0,
                "to_shop_id"          => 0,
                "inventory_id"        => 0,
                "shift_products_id"   => 0,
                "depart_rid"          => NULL,
                "corr_rid"            => NULL
            );

            $api_invoices = new ApiInvoicesController();

            $api_invoices->inventories($params);

        })->dailyAt('07:15');
        // });

        // Расходные накладные
        // Наличные вне смен за сутки
        $schedule->call(function () {

            $date_interval = [
                Carbon::yesterday(),
                Carbon::today()
                // Carbon::create("2020-07-21 00:00:00"),
                // Carbon::create("2020-07-22 00:00:00"),
            ];

            $params = array(
                "proc_name"           => "InsGDoc4",
                "harakter"            => "А",
                "type"                => "р/н",
                "in_shift"            => false,
                "worker"              => "cash",
                "payment"             => "CASH",
                "date_interval"       => $date_interval,
                "created_at"          => NULL,
                "shop_id"             => 0,
                "shift_id"            => 0,
                "reject_id"           => 0,
                "movement_id"         => 0,
                "to_shop_id"          => 0,
                "inventory_id"        => 0,
                "shift_products_id"   => 0,
                "depart_rid"          => NULL,
                "corr_rid"            => NULL
            );

            $api_invoices = new ApiInvoicesController();

            $api_invoices->computed_day($params);

        })->dailyAt('07:20');
        // });

        // Эквайринг вне смен за сутки
        $schedule->call(function () {

            $date_interval = [
                Carbon::yesterday(),
                Carbon::today()
                // Carbon::create("2020-07-21 00:00:00"),
                // Carbon::create("2020-07-22 00:00:00"),
            ];

            $params = array(
                "proc_name"           => "InsGDoc4",
                "harakter"            => "А",
                "type"                => "р/н",
                "in_shift"            => false,
                "worker"              => "cash",
                "payment"             => "CASHLESS",
                "date_interval"       => $date_interval,
                "created_at"          => NULL,
                "shop_id"             => 0,
                "shift_id"            => 0,
                "reject_id"           => 0,
                "movement_id"         => 0,
                "to_shop_id"          => 0,
                "inventory_id"        => 0,
                "shift_products_id"   => 0,
                "depart_rid"          => NULL,
                "corr_rid"            => NULL
            );

            $api_invoices = new ApiInvoicesController();

            $api_invoices->computed_day($params);

        })->dailyAt('07:25');
        // });

        // Импорт данных по заказам, товарам и кассам
        $schedule->call(function () {
            ImportController::index();
        })->everyMinute();

        // Обновение наличия товаров за сегодня
        $schedule->call(function () {
            $qty = new QuantityController();
            $qty->index();
        })->everyMinute();

        // Автоматическое закрытие смен
        $schedule->call(function () {
            ShiftController::index();
        })->dailyAt('03:00');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
