<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class UserShift extends Model
{
    public $timestamps = false;
    public $dates = ['date_start', 'date_end'];

    protected $guarded = [];

    public function getName()
    {
        return sprintf('%s - %s (%s - %s)', $this->user->name, $this->user->group->name, $this->date_start, $this->date_end);
    }

    /**
     * Получить открытые смены у пользователя
     */
    public function scopeOpen($query, $shop_id = false, $user_id = false)
    {
        $query = $query->where('user_id', $user_id ? $user_id : Auth::user()->id)
            ->whereNull('date_end')
            ->orderBy('date_start', 'desc');

        if ($shop_id) {
            $query->where('shop_id', $shop_id);
        }

        return $query;
    }

    public function shift()
    {
        return $this->belongsTo('App\Shift');
    }

    public function shop()
    {
        return $this->belongsTo('App\Shop');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
