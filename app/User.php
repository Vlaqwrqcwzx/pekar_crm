<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $guarded = [];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function group()
    {
        return $this->belongsTo('App\UserGroup');
    }

    public function getGroupMenuItems()
    {
        return $this->group ? (array)unserialize($this->group->menu) : [];
    }

    public function shopsVisible()
    {
        return $this->belongsToMany('App\Shop', 'user_shop')->wherePivot('type', 'visible')->withPivot('time');
    }

    public function shopsStock()
    {
        return $this->belongsToMany('App\Shop', 'user_shop')->wherePivot('type', 'stock');
    }

    public function reportSettings($report)
    {
        return $this->hasOne('App\ReportSetting')->where('report', $report)->get()->first();
    }
}
