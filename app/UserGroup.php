<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserGroup extends Model
{
    public const TYPES = [
        'baker'        => 'Пекарь',
        'confectioner' => 'Кондитер',
        'worker'       => 'Рабочий кухни',
        'cash'         => 'Касса'
    ];

    /**
     * Группа кухни?
     */
    public function getIsKitchenAttribute()
    {
        return in_array($this->type, ['baker', 'confectioner', 'worker']);
    }

    /**
     * Пекарь?
     */
    public function getIsBakerAttribute()
    {
        return in_array($this->type, ['baker']);
    }

    /**
     * Группа кассы?
     */
    public function getIsCashAttribute()
    {
        return in_array($this->type, ['cash']);
    }

    /**
     * Может ли открывать смену?
     */
    public function getCanOpenShiftAttribute()
    {
        return in_array($this->type, ['baker', 'confectioner', 'cash']);
    }

    /**
     *  Ключ смены
     */
    public function getShiftTypeAttribute()
    {
        switch ($this->type) {
            case 'baker':
            case 'worker':
                return 'baker';
            break;

            case 'confectioner':
                return 'confectioner';
            break;

            case 'cash':
                return 'cash';
            break;

            default:
                return false;
            break;
        }
    }
}
