@extends('layouts.list')

@section('title', 'Пользователи')

@section('button')
    <div class="btn-group" role="group">
        <a href="{{route('user.create')}}" class="btn btn-primary"><i class="fa fa-plus"></i></a>
    </div>
@endsection

@section('list')
    <table class="table">
        <thead>
        <tr>
            <th width="1">#</th>
            <th>Пользователь</th>
            <th>Группа</th>
            <th>Пин-код</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @forelse ($users as $user)
            <tr>
                <td>{{$user->id}}</td>
                <td>{{$user->name}}</td>
                <td>{{$user->group ? $user->group->name : '-' }}</td>
                <td>{{$user->code }}</td>
                <td class="text-right">
                    <a class="btn btn-primary btn-sm" href="{{route('user.edit', ['id' => $user->id])}}">
                        <i class="fa fa-pen"></i>
                    </a>
                </td>
            </tr>
        @empty
            <tr>
                <td colspan="5">Пусто</td>
            </tr>
        @endforelse
        </tbody>
    </table>
@endsection
