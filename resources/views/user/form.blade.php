@extends('layouts.form')

@section('title', $user->name ?? 'Добавление пользователя')
@section('action', $action)

@section('button')
    <div class="btn-group" role="group">
        <a class="btn btn-primary" href="#" onclick="$('#form').submit(); return false;"><i class="fa fa-save"></i></a>
        <a class="btn btn-secondary" href="{{route('user.index')}}"><i class="fa fa-reply"></i></a>
    </div>
@endsection

@section('form')
    @if (isset($user->id))
        {{method_field('PUT')}}
    @endif
    <div class="form-group row">
        <label for="name" class="col-sm-2 col-form-label text-right">Имя:</label>
        <div class="col-sm-10">
            <input type="text" class="form-control {{$errors->has('name') ? 'is-invalid' : ''}}" name="name" id="name"
                   value="{{old('name', $user->name ?? '')}}">
            @if ($errors->has('name'))
                <div class="invalid-feedback">{{$errors->first('name')}}</div>
            @endif
        </div>
    </div>
    <div class="form-group row">
        <label for="code" class="col-sm-2 col-form-label text-right">Пин-код:</label>
        <div class="col-sm-10">
            <input type="text" class="form-control {{$errors->has('code') ? 'is-invalid' : ''}}" name="code" id="code"
                   value="{{old('code', $user->code ?? '')}}">
            @if ($errors->has('code'))
                <div class="invalid-feedback">{{$errors->first('code')}}</div>
            @endif
        </div>
    </div>
    <div class="form-group row">
        <label for="group" class="col-sm-2 col-form-label text-right">Группа:</label>
        <div class="col-sm-10">
            <select class="form-control" name="group_id" id="group">
                @foreach($groups as $group)
                    <option value="{{$group->id}}" {{($group->id == old('group_id', $user->group_id ?? '')) ? 'selected' : ''}}>{{$group->name}}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-2 col-form-label text-right">Рабочие магазины:</label>
        <div class="col-sm-10">
            <div class="list-group">
                @foreach($shops as $s)
                    <div class="list-group-item">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox"
                                   class="custom-control-input"
                                   id="visible-{{$s->id}}"
                                   name="shops_visible[]"
                                   value="{{$s->id}}"
                                   {{in_array($s->id, $shopsVisible) ? 'checked' : ''}}>
                            <label class="custom-control-label" for="visible-{{$s->id}}">{{$s->name}}</label>
                        </div>
                        <div class="form-group row mt-2">
                            <label for="time-{{$s->id}}" class="col-sm-2 col-form-label">Время работы</label>
                            <div class="col-sm-10">
                                <input type="text"
                                       class="form-control"
                                       name="shops_time[{{$s->id}}]"
                                       id="time-{{$s->id}}"
                                       value="{{old('shops_time[' . $s->id . ']', $shopsTime[$s->id] ?? '')}}">
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-2 col-form-label text-right">Магазины с наличием:</label>
        <div class="col-sm-10">
            <div class="list-group">
                @foreach($shops as $s)
                    <div class="list-group-item">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox"
                                   class="custom-control-input"
                                   id="stock-{{$s->id}}"
                                   name="shops_stock[]"
                                   value="{{$s->id}}"
                                   {{in_array($s->id, $shopsStock) ? 'checked' : ''}}
                            >
                            <label class="custom-control-label" for="stock-{{$s->id}}">{{$s->name}}</label>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection
