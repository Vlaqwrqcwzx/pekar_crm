@extends('layouts.form')

@section('title', 'Настройки отчета')
@section('action', route('user.saveReportSettings'))

@section('button')
    <div class="btn-group" role="group">
        <a class="btn btn-primary" href="#" onclick="$('#form').submit(); return false;"><i class="fa fa-save"></i></a>
    </div>
@endsection

@section('form')
    <div class="form-group row">
        <label class="col-sm-2 col-form-label text-right">Категории товаров:</label>
        <div class="col-sm-10">
            <div class="list-group">
                @foreach($categories as $c)
                    <div class="list-group-item">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox"
                                   class="custom-control-input"
                                   id="category-{{$c->id}}"
                                   name="categories[]"
                                   value="{{$c->id}}"
                                   {{in_array($c->id, $userCategories) ? 'checked' : ''}}
                            >
                            <label class="custom-control-label" for="category-{{$c->id}}">{{$c->name}}</label>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <input type="hidden" name="report" value="{{$report}}">
@endsection
