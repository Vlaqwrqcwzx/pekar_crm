@extends('layouts.app')

@section('content')
<div class="container">
    <div class="card">
        <div class="card-header">
            <div class="float-left">
                <div class="h3">{{$reject->typeName}} - {{$reject->shop->name}} ({{$reject->created_at->format('Y-m-d H:i')}})</div>
            </div>
            <div class="float-right">
                <a class="btn btn-primary" href="#" onclick="window.print();"><i class="fa fa-print"></i></a>
            </div>
        </div>
        <table class="table">
            <thead>
            <tr>
                <th class="font-weight-normal w-25">Тип:</th>
                <th class="w-25">{{$reject->typeName}}</th>
                <th class="font-weight-normal w-25">Магазин:</th>
                <th class="w-25">{{$reject->shop->name}}</th>
            </tr>
            <tr>
                <th class="font-weight-normal">Дата:</th>
                <th>{{$reject->created_at->format('Y-m-d H:i')}}</th>
                <th class="font-weight-normal">Составил:</th>
                <th>{{$reject->user->name}}</th>
            </tr>
            <tr>
                <th colspan="2">Наимнование:</th>
                <th>Количество:</th>
                <th>Сумма</th>
            </tr>
            </thead>
            <tbody>
            @forelse ($reject->rejectProducts as $product)
                <tr>
                    <td colspan="2">{{$product->product->name}}</td>
                    <td>{{$product->quantity}}</td>
                    <td>@format($product->quantity * ($product->product->shopPrice ? $product->product->shopPrice->price : $product->product->price))</td>
                </tr>
            @empty
                <tr>
                    <td colspan="4">Пусто</td>
                </tr>
            @endforelse
            <tr>
                <th colspan="2">Итого, шт / кг:</th>
                <th>{{$totals['count']['countable']}} / {{$totals['count']['scalable']}}</th>
                <th>@format($totals['price']['countable']) / @format($totals['price']['scalable'])</th>
            </tr>
            </tbody>
        </table>
    </div>
</div>
@endsection
