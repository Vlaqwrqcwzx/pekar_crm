@extends('layouts.report')

@section('title', 'Брак / Бракераж / Списание')
@section('action', route('reject.index'))

@section('list')
    <div class="table-freeze">
        <table class="table">
            <thead>
            <tr>
                <th>Пользователь</th>
                <th>Магазин</th>
                <th>Действие</th>
                <th>Место</th>
                <th>Дата</th>
            </tr>
            </thead>
            <tbody>
            @forelse ($rejects as $reject)
                <tr>
                    <td><a href="{{route('reject.view', ['reject' => $reject->id])}}">{{$reject->user->name}}</a></td>
                    <td>{{$reject->shop->name}}</td>
                    <td>{{$types[$reject->type]}}</td>
                    <td>{{$reject->getPlace()}}</td>
                    <td>{{$reject->created_at}}</td>
                </tr>
            @empty
                <tr>
                    <td colspan="5">Пусто</td>
                </tr>
            @endforelse
            </tbody>
        </table>
    </div>
@endsection
