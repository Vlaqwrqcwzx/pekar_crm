@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <report-component></report-component>
        </div>
    </div>
@endsection
