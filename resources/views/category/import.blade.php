@extends('layouts.list')

@section('title', 'Категории')
@section('action', route('category.destroy', 0))

@section('button')
    <div class="btn-group" role="group">
        <a href="{{route('category.create', ['import' => $import_id])}}" class="btn btn-primary"><i class="fa fa-plus"></i></a>
    </div>
@endsection

@section('list')
    <table class="table">
        <thead>
        <tr>
            <th>Название</th>
            <th>Цвет</th>
            <th>Сортировка</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @forelse ($categories as $category)
            <tr>
                <td>{{$category->name}}</td>
                <td><input type="color" id="color" name="color" value="{{ $category->color }}" disabled></td>
                <td>{{$category->sort}}</td>
                <td class="text-right">
                    <a class="btn btn-primary btn-sm" href="{{route('category.edit', ['id' => $category->id])}}">
                        <i class="fa fa-pen"></i>
                    </a>
                </td>
            </tr>
        @empty
            <tr>
                <td colspan="4">Пусто</td>
            </tr>
        @endforelse
        </tbody>
    </table>
@endsection
