@extends('layouts.form')

@section('title', $category->name ?? 'Добавление категории')
@section('action', $action)

@section('button')
    <div class="btn-group" role="group">
        <a class="btn btn-primary" href="#" onclick="$('#form').submit(); return false;"><i class="fa fa-save"></i></a>
        <a class="btn btn-secondary" href="{{route('category.index')}}"><i class="fa fa-reply"></i></a>
    </div>
@endsection

@section('form')
    @if (isset($category->id))
        {{method_field('PUT')}}
    @endif
    <div class="form-group row">
        <label for="name" class="col-sm-3 col-form-label text-right">Название категории:</label>
        <div class="col-sm-9">
            <input type="text" class="form-control {{$errors->has('name') ? 'is-invalid' : ''}}" name="name" id="name" value="{{old('name', $category->name ?? '')}}">
            @if ($errors->has('name'))
            <div class="invalid-feedback">{{$errors->first('name')}}</div>
            @endif
        </div>
    <input name="import_id" type="hidden" value="{{ $import->id }}" />
    </div>
    <div class="form-group row">
        <label for="color" class="col-sm-3 col-form-label text-right">Цвет категории:</label>
        <div class="col-sm-9">
            <input type="color" class="form-control" id="color" name="color" value="{{ $category ? $category->color : '#FFFFFF' }}">
        </div>
    </div>
    <div class="form-group row">
        <label for="color" class="col-sm-3 col-form-label text-right">Сортировка:</label>
        <div class="col-sm-9">
            <input type="number" class="form-control" id="sort" name="sort" value="{{ $category ? $category->sort : 0 }}">
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-3 col-form-label text-right">Категории касс ({{ $import->name }}):</label>
        <div class="col-sm-9">
            <div class="list-group">
                @foreach($device_categories as $device_category)
                    @if($device_category->import_id == $import->id && !$device_category->category_id || ($category && $device_category->category_id == $category->id) && $device_category->import_id == $import->id)
                        <div class="list-group-item">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox"
                                    class="custom-control-input"
                                    id="device_category-{{$device_category->device_category_id}}"
                                    name="device_category[]"
                                    value="{{$device_category->device_category_id}}"
                                    {{($category && $device_category->category_id == $category->id) || in_array($device_category->device_category_id, (array)old('device_category')) ? 'checked' : ''}}
                                >
                                <label class="custom-control-label" for="device_category-{{$device_category->device_category_id}}">{{$device_category->name}}</label>
                            </div>
                        </div>
                    @endif
                @endforeach
            </div>
        </div>
    </div>
    <div class="form-group row">
        <label for="shop" class="col-sm-3 col-form-label text-right">Участвует в витрине:</label>
        <div class="col-sm-9">
            <div class="custom-control custom-checkbox mt-2">
                <input type="checkbox"
                       class="custom-control-input"
                       name="shop"
                       id="shop"
                       value="1"
                    {{(old('shop', $category->shop ?? 0)) ? 'checked' : ''}}
                >
                <label for="shop" class="custom-control-label">Да</label>
            </div>
        </div>
    </div>
    <div class="form-group row">
        <label for="kitchen" class="col-sm-3 col-form-label text-right">Может производиться:</label>
        <div class="col-sm-9">
            <div class="custom-control custom-checkbox mt-2">
                <input type="checkbox"
                       class="custom-control-input"
                       name="kitchen"
                       id="kitchen"
                       value="1"
                    {{(old('shop', $category->kitchen ?? 0)) ? 'checked' : ''}}
                >
                <label for="kitchen" class="custom-control-label">Да</label>
            </div>
        </div>
    </div>
@endsection
