@extends('layouts.list')

@section('title', 'Категории')

@section('list')
    <div class="table-freeze">
        <table class="table">
            <thead>
            <tr>
                <th>Интеграция</th>
            </tr>
            </thead>
            <tbody>
            @forelse ($imports as $import)
                <tr>
                    <td><a href="{{route('category.import', ['import' => $import->id])}}">{{$import->name}}</a></td>
                </tr>
            @empty
                <tr>
                    <td>Пусто</td>
                </tr>
            @endforelse
            </tbody>
        </table>
    </div>
@endsection
