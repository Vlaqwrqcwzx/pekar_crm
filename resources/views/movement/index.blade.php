@extends('layouts.report')

@section('title', 'Перемещения')
@section('action', route('movement.index'))

@section('list')
    <div class="table-freeze">
        <table class="table">
            <thead>
            <tr>
                <th>Магазин</th>
                <th>№ Смены</th>
                <th>Склад отправителя</th>
                <th>Отправил</th>
                <th>Склад получателя</th>
                <th>Принял</th>
                <th>Сумма шт/кг</th>
                <th>Дата</th>
            </tr>
            </thead>
            <tbody>
            @forelse ($movements as $key => $movement)
                <tr>
                    <td>
                        <a href="{{route('movement.view', ['inventory' => $movement->id])}}">
                            {{$movement->getName()}}
                        </a>
                    </td>
                    <td>{{$movement->shift_id}}</td>
                    @if($movement->place === "kitchen")
                        <td>{{isset($movement->fromShop->from_depart_one->name) ? $movement->fromShop->from_depart_one->name : '-'}}</td>
                    @else
                        <td>{{isset($movement->fromShop->from_depart_two->name) ? $movement->fromShop->from_depart_two->name : '-'}}</td>
                    @endif
                    <td>{{$movement->fromUser->name}}</td>
                    <td>{{isset($movement->toShop->to_depart->name) ? $movement->toShop->to_depart->name : '-'}}</td>
                    <td>{{$movement->toUser ? $movement->toUser->name : '-'}}</td>
                    <td>@moneyFormat( $summ_countable[$key] )</td>
                    {{-- <td>@moneyFormat( $summ_countable[$key] ) / @moneyFormat( $summ_scalable[$key] )</td> --}}
                    <td>{{$movement->created_at}}</td>
                </tr>
            @empty
                <tr>
                    <td colspan="3">Пусто</td>
                </tr>
            @endforelse
            </tbody>
        </table>
    </div>
@endsection
