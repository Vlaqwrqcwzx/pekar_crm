@extends('layouts.app')

@section('content')
<div class="container">
    <div class="card">
        <div class="card-header">
            <div class="float-left">
                <div class="h3">{{$movement->getName()}} ({{$movement->created_at->format('Y-m-d H:i')}})</div>
            </div>
            <div class="float-right">
                <a class="btn btn-primary" href="#" onclick="window.print();"><i class="fa fa-print"></i></a>
            </div>
        </div>
        <table class="table">
            <thead>
            <tr>
                <th class="font-weight-normal w-25">Отправитель:</th>
                <th class="w-25">{{$movement->fromUser->name}}</th>
                <th class="font-weight-normal w-25">Получатель:</th>
                <th class="w-25">{{$movement->toUser ? $movement->toUser->name : '-'}}</th>
            </tr>
            <tr>
                <th class="font-weight-normal">Создан:</th>
                <th>{{$movement->created_at->format('Y-m-d H:i')}}</th>
                <th class="font-weight-normal">Принят:</th>
                <th>{{$movement->toUser ? $movement->updated_at->format('Y-m-d H:i') : '-'}}</th>
            </tr>
            <tr>
                <th colspan="2">Наимнование:</th>
                <th>Количество:</th>
                <th>Сумма:</th>
            </tr>
            </thead>
            <tbody>
            @forelse ($movement->movementProducts as $product)
                <tr>
                    <td colspan="2">{{$product->product->getName()}}</td>
                    <td>{{$product->complete}}</td>
                    <td>@moneyFormat($product->complete * $product->product->getPrice())</td>
                </tr>
            @empty
                <tr>
                    <td colspan="3">Пусто</td>
                </tr>
            @endforelse
            <tr>
                <th colspan="2">Итого, шт / кг:</th>
                <th>{{$totals['count']['countable']}} / {{$totals['count']['scalable']}}</th>
                <th>@moneyFormat($totals['price']['countable']) / @moneyFormat($totals['price']['scalable'])</th>
            </tr>
            </tbody>
        </table>
    </div>
</div>
@endsection
