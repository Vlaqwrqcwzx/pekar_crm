@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">
                <div class="float-left">
                    <div class="h3">Сличительная ведомость "{{$shift->shop->name}}"</div>
                </div>
            </div>
            <div class="card-body">
                @if (session()->has('success'))
                    <div class="alert alert-success">
                        {{session('success')}}
                    </div>
                @endif
                @if (session()->has('warning'))
                    <div class="alert alert-danger">
                        {{session('warning')}}
                    </div>
                @endif
            </div>

            <inventory-component shift_id="{{$shift->id}}"></inventory-component>
        </div>
    </div>
@endsection
