@extends('layouts.app')

@section('navbar', '')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8 mt-5">
                <div class="card">
                    <div class="card-header">
                        <div class="h3"><span class="badge badge-success">{{ Auth::user()->name }}</span> {{$shop ? $shop->name : 'Выберете магазин'}}</div>
                    </div>
                    <div class="card-body">
                        @if (session()->has('success'))
                            <div class="alert alert-success">
                                {{session('success')}}
                            </div>
                        @endif
                        @if (session()->has('warning'))
                            <div class="alert alert-danger">
                                {{session('warning')}}
                            </div>
                        @endif
                        @if($shop)
                            <div class="d-flex flex-column">
                                @if($canOpen)
                                    @if($shift)
                                        <a href="{{route('shift.view', ['shift' => $shift->shift_id])}}" class="btn btn-lg mb-3 btn-success">Продолжить смену</a>
                                        <a href="{{route('shift.exit', ['shop' => $shift->shop_id])}}" class="btn btn-lg mb-3 btn-danger" @click.prevent="confirm('Вы уверены?')">Отметить уход</a>
                                        <a href="{{route('shift.close', ['shift' => $shift->shift_id])}}" class="btn btn-lg mb-3 btn-danger" @click.prevent="confirm('Вы уверены?')">Закрыть смену</a>
                                    @else
                                        @if($openShift)
                                        <a href="{{route('shift.enter', ['shop' => $openShift->shop_id])}}" class="btn btn-lg mb-3 btn-primary">Перейти в смену</a>
                                        @else
                                        <a href="{{route('shift.open', ['shop' => $shop->id])}}" class="btn btn-lg mb-3 btn-primary">Открыть смену</a>
                                        @endif
                                    @endif
                                @else
                                    @if($shift)
                                        <a href="{{route('shift.exit', ['shop' => $shop->id])}}" class="btn btn-lg mb-3 btn-danger" @click.prevent="confirm('Вы уверены?')">Отметить уход</a>
                                    @else
                                        <a href="{{route('shift.enter', ['shop' => $shop->id])}}" class="btn btn-lg mb-3 btn-primary">Отметить приход</a>
                                    @endif
                                @endif
                            </div>
                        @else
                            <select name="shop" class="form-control" @change="redirect($event.target.value)">
                                <option value="">-</option>
                                @foreach($shops as $s)
                                    <option value="{{route('shift.shop', ['id' => $s->id])}}">{{$s->name}}</option>
                                @endforeach
                            </select>
                        @endif
                        <form action="{{ route('logout') }}" method="POST" class="text-center mt-5">
                            @csrf
                            <button type="submit" class="btn btn-lg mb-3 btn-danger">Сменить пользователя</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
