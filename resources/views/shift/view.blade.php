@extends('layouts.app')

@section('navbar', '')

@section('content')
    <div class="container-fluid">
        <div style="margin-top: 70px">
            @if (session()->has('warning'))
                <div class="alert alert-danger">
                    {{session('warning')}}
                </div>
            @endif
            <shift-component shift_id="{{$shift->id}}"></shift-component>
        </div>
    </div>
@endsection
