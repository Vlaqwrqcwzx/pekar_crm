@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">Вход</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}" id="form-login">
                        @csrf
                        <div class="form-group">
                            <label for="code">Пин-код</label>
                            <input id="code"
                                   type="text"
                                   name="code"
                                   class="form-control @error('code') is-invalid @enderror"
                                   required
                                   readonly
                                   autocomplete="name"
                                   autofocus>
                            @error('code')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <input type="hidden" name="password" value="password">
                        <input type="hidden" name="remember" value="1">

                        <input-keyboard-component :elements="['#code']" submit="#form-login"></input-keyboard-component>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
