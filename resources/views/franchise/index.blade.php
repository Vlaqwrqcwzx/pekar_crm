@extends('layouts.list')

@section('title', 'Франшизы')

@section('button')
    <div class="btn-group" role="group">
        <a href="{{route('franchise.create')}}" class="btn btn-primary"><i class="fa fa-plus"></i></a>
    </div>
@endsection

@section('list')
    <table class="table">
        <thead>
        <tr>
            <th>Название</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @forelse ($franchises as $franchise)
            <tr>
                <td>{{$franchise->name}}</td>
                <td class="text-right">
                    <a class="btn btn-primary btn-sm" href="{{route('franchise.edit', ['id' => $franchise->id])}}">
                        <i class="fa fa-pen"></i>
                    </a>
                </td>
            </tr>
        @empty
            <tr>
                <td colspan="2">Пусто</td>
            </tr>
        @endforelse
        </tbody>
    </table>
@endsection
