@extends('layouts.form')

@section('title', 'Настройки')
@section('action', route('setting.update'))

@section('button')
    <div class="btn-group" role="group">
        <a class="btn btn-primary" href="#" onclick="$('#form').submit(); return false;"><i class="fa fa-save"></i></a>
    </div>
@endsection

@section('form')
    <h5>Округление</h5>
    <div class="form-group row">
        <label for="decimal_place" class="col-sm-4 col-form-label text-right">Количество знаков после запятой:</label>
        <div class="col-sm-8">
            <input type="text" class="form-control" name="decimal_place" id="decimal_place">
        </div>
    </div>
@endsection
