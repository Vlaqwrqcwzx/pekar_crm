@extends('layouts.list')

@section('title', 'Кассы')

@section('button')
    <!-- <div class="btn-group" role="group">
        <a href="" class="btn btn-primary"><i class="fa fa-plus"></i></a>
    </div> -->
@endsection

@section('list')
    <table class="table">
        <thead>
        <tr>
            <th>#</th>
            <th>Название кассы</th>
            <th>Сервис</th>
            <th>Интеграция</th>
            <!-- <th>#</th> -->
        </tr>
        </thead>
        <tbody>
        @forelse ($devices as $device)
            <tr>
                <td>{{$device->id}}</td>
                <td>
                    {{$device->name}}
                    <div class="small text-muted">{{$device->uuid}}</div>
                </td>
                <td>{{ $device->model_name }}</td>
                @foreach ($imports as $import)
                    @if ($device->import_id === $import->id)
                        <td>{{ $import->name }}</td>
                    @endif
                @endforeach
                <!-- <td class="text-right">
                    <a class="btn btn-primary btn-sm" href="{{route('device.edit', ['id' => $device->id])}}">
                        <i class="fa fa-pen"></i>
                    </a>
                </td> -->
            </tr>
        @empty
            <tr>
                <td colspan="2">Пусто</td>
            </tr>
        @endforelse
        </tbody>
    </table>
@endsection
