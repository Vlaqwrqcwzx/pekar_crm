@extends('layouts.form')

@section('title', $device->name ?? 'Добавление кассы')
@section('action', $action)

@section('button')
    <div class="btn-group" role="group">
        <a class="btn btn-primary" href="#" onclick="$('#form').submit(); return false;"><i class="fa fa-save"></i></a>
        <a class="btn btn-secondary" href="{{route('device.index')}}"><i class="fa fa-reply"></i></a>
    </div>
@endsection

@section('form')
    @if (isset($device->id))
        {{method_field('PUT')}}
    @endif
    <div class="form-group row">
        <label for="name" class="col-sm-3 col-form-label text-right">Название:</label>
        <div class="col-sm-9">
            <input type="text"
                   class="form-control {{$errors->has('name') ? 'is-invalid' : ''}}"
                   name="name"
                   id="name"
                   disabled
                   value="{{old('name', $device->name ?? '')}}">
            @if ($errors->has('name'))
                <div class="invalid-feedback">{{$errors->first('name')}}</div>
            @endif
        </div>
    </div>
    <div class="form-group row">
        <label for="shop_id" class="col-sm-3 col-form-label text-right">Магазин:</label>
        <div class="col-sm-9">
            <select name="shop_id" class="form-control" id="shop_id">
                <option value="0">-</option>
                @foreach($shops as $shop)
                    <option value="{{$device->shop_id}}" {{($device->shop_id === $shop->id) ? 'selected' : ''}}>
                        {{ $shop->name }}
                    </option>
                @endforeach
            </select>
        </div>
    </div>
@endsection
