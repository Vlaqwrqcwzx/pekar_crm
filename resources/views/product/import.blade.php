@extends('layouts.list')

@section('title', 'Товары')

@section('button')
    <div class="btn-group" role="group">
        <push-sh5-products-component></push-sh5-products-component>
    </div>
@endsection

@section('list')
    <table class="table">
        <thead>
        <tr>
            <th>#</th>
            <th>Название</th>
            <th>Время приготовление</th>
            <th>SH5_GUID</th>
            <th>Цена</th>
            <th>#</th>
        </tr>
        </thead>
        <tbody>
        @forelse ($products as $product)
            <tr>
                <td>{{$product->id}}</td>
                <td>
                    {{$product->name}}
                    <div class="small text-muted">{{$product->uuid}}</div>
                </td>
                <td>{{!empty($product->time) ? $product->time : '--:--'}}</td>
                <td>{{$product->sh5_guid}}</td>
                <td>@format($product->price)</td>
                <td class="text-right">
                    <a class="btn btn-primary btn-sm" href="{{route('product.edit', ['id' => $product->id, 'import_id' => $import_id])}}">
                        <i class="fa fa-pen"></i>
                    </a>
                </td>
            </tr>
        @empty
            <tr>
                <td colspan="3">Пусто</td>
            </tr>
        @endforelse
        </tbody>
    </table>
@endsection