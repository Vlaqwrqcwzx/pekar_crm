@extends('layouts.form')

@section('title', $product->name)
@section('action', $action)

@section('button')
    <div class="btn-group" role="group">
        <a class="btn btn-primary" href="#" onclick="$('#form').submit(); return false;"><i class="fa fa-save"></i></a>
        <a class="btn btn-secondary" href="{{route('product.import', ['import_id' => $import_id])}}"><i class="fa fa-reply"></i></a>
    </div>
@endsection

@section('form')
    @if (isset($product->id))
        {{method_field('PUT')}}
    @endif
    <div class="form-group row">
        <input type="hidden" name="import_id" id="import_id" value="{{$import_id}}" />
        <label for="name" class="col-sm-2 col-form-label text-right">Название:</label>
        <div class="col-sm-10">
            <input type="text"
                   class="form-control {{$errors->has('name') ? 'is-invalid' : ''}}"
                   name="name"
                   id="name"
                   value="{{old('name', $product->name ?? '')}}"
                   {{$product->children ? $product->children->count() ? '' : 'readonly' : ''}}
            >
            @if ($errors->has('name'))
                <div class="invalid-feedback">{{$errors->first('name')}}</div>
            @endif
        </div>
    </div>
    <div class="form-group row">
        <label for="time" class="col-sm-2 col-form-label text-right">Время приготовление:</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="time" id="time" value="{{old('time', $product->time ?? '')}}">
        </div>
    </div>
    <div class="form-group row">
        <label for="parent" class="col-sm-2 col-form-label text-right">Родительский товар:</label>
        <div class="col-sm-10">
            <select name="parent_id" class="form-control" id="parent">
                <option value="0">-</option>
                @foreach($products as $p)
                <option value="{{$p->id}}" {{(old('parent_id', $product->parent_id ?? 0) == $p->id) ? 'selected' : ''}}>{{$p->name}}</option>
                @endforeach
            </select>
        </div>
    </div>
@endsection
