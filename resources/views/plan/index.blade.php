@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="card">
            <div class="card-header">
                <div class="float-left">
                    <div class="h3">План выпуска</div>
                </div>
                <div class="float-right">
                    <div class="input-group">
                        <select class="form-control" @change="redirect($event.target.value)">
                            @foreach($shops as $s)
                                <option value="{{route('plan.shop', ['shop_id' => $s->id])}}" {{($shop->id == $s->id) ? 'selected' : ''}}>{{$s->name}}</option>
                            @endforeach
                        </select>
                        <div class="input-group-append">
                            <button class="btn btn-primary" form="form">Сохранить</button>
                        </div>
                    </div>
                </div>
            </div>
            @if (session()->has('success'))
                <div class="alert alert-success">
                    {{session('success')}}
                </div>
            @endif
            @if (session()->has('warning'))
                <div class="alert alert-danger">
                    {{session('warning')}}
                </div>
            @endif
            @if ($shop)
                <form action="{{route('plan.updatePlanDays', ['shop_id' => $shop->id])}}" method="post" id="form">
                    {{csrf_field()}}
                    {{method_field('PUT')}}
                    @if ($plan)
                    <input type="hidden" name="plan_id" value="{{$plan->id}}">
                    @endif
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Название</th>
                            <th>Тип</th>
                            <th>Сумма</th>
                            <th>Пн</th>
                            <th>Вт</th>
                            <th>Ср</th>
                            <th>Чт</th>
                            <th>Пт</th>
                            <th>Сб</th>
                            <th>Вс</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse ($plans as $p)
                            <tr class="{{($plan->id ?? 0) == $p->id ? 'bg-light text-whit' : ''}}">
                                <td><a href="{{route('plan.plan', ['plan_id' => $p->id])}}">{{$p->name}}</a></td>
                                <td>{{$p->typeName}}</td>
                                <td>{{$p->total}}</td>
                                @foreach($days as $day)
                                    <td>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox"
                                                   name="plan[{{$p->id}}][{{$day}}]"
                                                   id="plan{{$p->id}}{{$day}}"
                                                   value="1"
                                                   class="custom-control-input"
                                                   {{$p->{$day} ? 'checked' : ''}}
                                            >
                                            <label for="plan{{$p->id}}{{$day}}" class="custom-control-label"></label>
                                        </div>
                                    </td>
                                @endforeach
                            </tr>
                        @empty
                            <tr>
                                <td colspan="10">Пусто</td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </form>
            @endif
            @if ($shop)
                <div class="card-footer">
                    <form action="{{route('plan.store')}}" method="post" id="form">
                        {{csrf_field()}}
                        <input type="hidden" name="shop_id" value="{{$shop->id}}">
                        <div class="input-group">
                            <input type="text" name="name" class="form-control {{$errors->has('name') ? 'is-invalid' : ''}}" placeholder="Добавить план" required>
                            <select name="type" class="form-control">
                                <option value="">- Тип смены -</option>
                                @foreach($types as $id => $type)
                                    <option value="{{$id}}" {{($id == old('type', '')) ? 'selected' : ''}}>{{$type}}</option>
                                @endforeach
                            </select>
                            <div class="input-group-append">
                                <button class="btn btn-primary">Добавить</button>
                            </div>
                        </div>
                        @if ($errors->has('name'))
                            <div class="invalid-feedback">{{$errors->first('name')}}</div>
                        @endif
                    </form>
                </div>
            @endif
        </div>

        @if($plan)
            <div class="card mt-3">
                <div class="card-header">
                    <div class="float-left">
                        <div class="h3">{{$plan->name}}</div>
                    </div>
                    <div class="float-right d-flex">
                        <form action="{{route('plan.update', $plan)}}" method="post" class="input-group mr-3">
                            <input type="text" name="name" class="form-control" placeholder="Переименовать" required value="{{$plan->name}}">
                            {{csrf_field()}}
                            <div class="input-group-append">
                                <button class="btn btn-primary" type="submit">Переименовать</button>
                            </div>
                        </form>
                        <div class="btn-group">
                            <a href="{{route('plan.copy', $plan)}}" class="btn btn-primary">Копировать</a>
                            <div class="btn-group">
                                <button class="btn btn-success dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown">
                                    Копировать в
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    @foreach($shops as $s)
                                        @if ($s->id != $shop->id)
                                            <a class="dropdown-item" href="{{route('plan.copy', ['id'=> $plan->id, 'shop_id' => $s->id])}}">{{$s->name}}</a>
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                            <button type="submit" form="delete" class="btn btn-danger" @click.prevent="confirm('Удалить план?')">Удалить</button>
                        </div>
                    </div>
                    <form action="{{route('plan.delete', $plan)}}" method="post" id="delete">
                        {{csrf_field()}}
                        {{method_field('DELETE')}}
                    </form>
                </div>
                <div class="table-freeze">
                    <plan-component :plan_id="{{$plan->id}}"></plan-component>
                </div>
            </div>
        @endif
    </div>
@endsection
