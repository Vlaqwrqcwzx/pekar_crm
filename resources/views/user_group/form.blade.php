@extends('layouts.form')

@section('title', $user->name ?? 'Добавление группы пользователей')
@section('action', $action)

@section('button')
    <div class="btn-group" role="group">
        <a class="btn btn-primary" href="#" onclick="$('#form').submit(); return false;"><i class="fa fa-save"></i></a>
        <a class="btn btn-secondary" href="{{route('user_group.index')}}"><i class="fa fa-reply"></i></a>
    </div>
@endsection

@section('form')
    @if (isset($group->id))
        {{method_field('PUT')}}
    @endif
    <div class="form-group row">
        <label for="name" class="col-sm-2 col-form-label text-right">Названия:</label>
        <div class="col-sm-10">
            <input type="text" class="form-control {{$errors->has('name') ? 'is-invalid' : ''}}" name="name" id="name" value="{{old('name', $group->name ?? '')}}">
            @if ($errors->has('name'))
                <div class="invalid-feedback">{{$errors->first('name')}}</div>
            @endif
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-2 col-form-label text-right">Права:</label>
        <div class="col-sm-10">
            <div class="list-group">
                @foreach($menu as $i => $item)
                <div class="list-group-item">
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox"
                               class="custom-control-input"
                               id="checkbox{{$i}}"
                               name="menu[]"
                               value="{{$item['route']}}"
                               {{in_array($item['route'], $group_menu) ? 'checked' : ''}}
                        >
                        <label class="custom-control-label" for="checkbox{{$i}}">{{$item['title']}}</label>
                    </div>
                    <div class="ml-3">
                        @foreach($item['children'] as $j => $child)
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox"
                                       class="custom-control-input"
                                       id="checkbox{{$i}}{{$j}}"
                                       name="menu[]"
                                       value="{{$child['route']}}"
                                       {{in_array($child['route'], $group_menu) ? 'checked' : ''}}
                                >
                                <label class="custom-control-label" for="checkbox{{$i}}{{$j}}">{{$child['title']}}</label>
                            </div>
                            <div class="ml-4">
                                @if (isset($child['children']))
                                    @foreach($child['children'] as $k => $child_inside)
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox"
                                                class="custom-control-input"
                                                id="checkbox{{$i}}{{$j}}{{$k}}"
                                                name="menu[]"
                                                value="{{$child_inside['route']}}"
                                                {{in_array($child_inside['route'], $group_menu) ? 'checked' : ''}}
                                            >
                                            <label class="custom-control-label" for="checkbox{{$i}}{{$j}}{{$k}}">{{$child_inside['title']}}</label>
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                        @endforeach
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="form-group row">
        <label for="type" class="col-sm-2 col-form-label text-right">Тип:</label>
        <div class="col-sm-10">
            <select class="form-control" name="type" id="type">
                <option value="">-</option>
                @foreach($types as $id => $type)
                    <option value="{{$id}}" {{($id == old('type', $group->type ?? '')) ? 'selected' : ''}}>{{$type}}</option>
                @endforeach
            </select>
        </div>
    </div>
@endsection
