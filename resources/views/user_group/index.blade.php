@extends('layouts.list')

@section('title', 'Группы пользователей')

@section('button')
    <div class="btn-group" role="group">
        <a href="{{route('user_group.create')}}" class="btn btn-primary"><i class="fa fa-plus"></i></a>
    </div>
@endsection

@section('list')
    <table class="table">
        <thead>
        <tr>
            <th>Название</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @forelse ($groups as $group)
            <tr>
                <td>{{$group->name}}</td>
                <td class="text-right">
                    <a class="btn btn-primary btn-sm" href="{{route('user_group.edit', ['id' => $group->id])}}">
                        <i class="fa fa-pen"></i>
                    </a>
                </td>
            </tr>
        @empty
            <tr>
                <td colspan="2">Пусто</td>
            </tr>
        @endforelse
        </tbody>
    </table>
@endsection
