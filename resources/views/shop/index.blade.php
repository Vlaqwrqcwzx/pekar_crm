@extends('layouts.list')

@section('title', 'Магазины')

@section('button')
    <div class="btn-group" role="group">
        <a href="{{route('shop.create')}}" class="btn btn-primary"><i class="fa fa-plus"></i></a>
    </div>
@endsection

@section('list')
    <table class="table">
        <thead>
        <tr>
            <th>Название</th>
            <th>#</th>
        </tr>
        </thead>
        <tbody>
        @forelse ($shops as $shop)
            <tr>
                <td>
                    <div>{{$shop->name}}</div>
                    @foreach ($shop->devices as $device)
                        <span class="badge badge-success">{{$device->name}}</span>
                    @endforeach
                </td>
                <td class="text-right">
                    <a class="btn btn-primary btn-sm" href="{{route('shop.edit', ['id' => $shop->id])}}">
                        <i class="fa fa-pen"></i>
                    </a>
                </td>
            </tr>
        @empty
            <tr>
                <td colspan="2">Пусто</td>
            </tr>
        @endforelse
        </tbody>
    </table>
@endsection
