@extends('layouts.form')

@section('title', $shop->name ?? 'Добавление магазина')
@section('action', $action)

@section('button')
    <div class="btn-group" role="group">
        <a class="btn btn-primary" href="#" onclick="$('#form').submit(); return false;"><i class="fa fa-save"></i></a>
        <a class="btn btn-secondary" href="{{route('shop.index')}}"><i class="fa fa-reply"></i></a>
    </div>
@endsection

@section('form')
    @if (isset($shop->id))
        {{method_field('PUT')}}
    @endif
    <div class="form-group row">
        <label for="name" class="col-sm-3 col-form-label text-right">Название:</label>
        <div class="col-sm-9">
            <input type="text"
                   class="form-control {{$errors->has('name') ? 'is-invalid' : ''}}"
                   name="name"
                   id="name"
                   value="{{old('name', $shop->name ?? '')}}">
            @if ($errors->has('name'))
                <div class="invalid-feedback">{{$errors->first('name')}}</div>
            @endif
        </div>
    </div>
    <div class="form-group row">
        <label for="franchise" class="col-sm-3 col-form-label text-right">Франшиза:</label>
        <div class="col-sm-9">
            <select class="form-control" name="franchise_id" id="franchise">
                <option value="0">-</option>
                @foreach($franchises as $franchise)
                    <option value="{{$franchise->id}}" {{($franchise->id == old('franchise_id', $shop->franchise_id ?? '')) ? 'selected' : ''}}>{{$franchise->name}}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="form-group row">
        <label for="import_id" class="col-sm-3 col-form-label text-right">Импорт:</label>
        <div class="col-sm-9">
            <select class="form-control" name="import_id" id="import_id">
                <option value="0">-</option>
                @foreach($imports as $import)
                    <option value="{{$import->id}}" {{($import->id == old('import_id', $shop->import_id ?? '')) ? 'selected' : ''}}>{{$import->name}}</option>
                @endforeach
            </select>
        </div>
    </div>
    @if(isset($shop->import_id))
        <div class="form-group row">
            <label class="col-sm-3 col-form-label text-right">Кассы:</label>
            <div class="col-sm-9">
                <div class="list-group">
                    @foreach($devices as $d)
                        @if($shop->import_id == $d->import_id)
                            <div class="list-group-item">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox"
                                        class="custom-control-input"
                                        id="device-{{$d->id}}"
                                        name="device[]"
                                        value="{{$d->id}}"
                                        {{($shop && $d->shop_id == $shop->id) || in_array($d->id, (array)old('device')) ? 'checked' : ''}}
                                    ><label class="custom-control-label" for="device-{{$d->id}}">{{$d->name}}</label>
                                </div>
                            </div>                        
                        @endif
                    @endforeach
                    @if(!$shop->import_id)
                        <div class="list-group-item">
                            Выберите импорт
                        </div>
                    @endif
                </div>
            </div>
        </div>
        @if($shop->import_id || $shop->import_id != 0)
            <div class="form-group row">
                <label for="depart_rid_1" class="col-sm-3 col-form-label text-right">Склад производства:</label>
                <div class="col-sm-9">
                    <select name="depart_rid_1" class="form-control" id="depart_rid_1">
                        <option value="0" selected >-</option>
                        @foreach($sh5_settings as $setting)
                            @foreach($departs as $depart)
                                @if ($setting->id == $depart->setting_id)
                                    <option value="{{$depart->rid}}" {{(isset($shop->depart_rid_1) && $depart->rid === $shop->depart_rid_1) ? 'selected' : ''}}>{{$depart->name}}</option>
                                @endif
                            @endforeach
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label for="depart_rid_2" class="col-sm-3 col-form-label text-right">Склад магазина:</label>
                <div class="col-sm-9">
                    <select name="depart_rid_2" class="form-control" id="depart_rid_2">
                        <option value="0" selected >-</option>
                        @foreach($sh5_settings as $setting)
                            @foreach($departs as $depart)
                                @if ($setting->id == $depart->setting_id)
                                    <option value="{{$depart->rid}}"
                                        {{(isset($shop->depart_rid_2) && $depart->rid === $shop->depart_rid_2) ? 'selected' : ''}}
                                    >{{$depart->name}}</option>
                                @endif
                            @endforeach
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label for="cash_corr_rid" class="col-sm-3 col-form-label text-right">Корреспондент продаж (Наличка):</label>
                <div class="col-sm-9">
                    <select name="cash_corr_rid" class="form-control" id="cash_corr_rid">
                        <option value="0" selected >-</option>
                        @foreach($sh5_settings as $setting)
                            @foreach($corrs as $corr)
                                @if ($setting->id == $corr->setting_id)
                                    <option value="{{$corr->rid}}"
                                        {{(isset($shop->cashless_corr_rid) && $corr->rid === $shop->cash_corr_rid) ? 'selected' : ''}}
                                    >{{$corr->name}} ({{ $setting->name }})</option>
                                @endif
                            @endforeach
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label for="cashless_corr_rid" class="col-sm-3 col-form-label text-right">Корреспондент продаж (Эквайринг):</label>
                <div class="col-sm-9">
                    <select name="cashless_corr_rid" class="form-control" id="cashless_corr_rid">
                        <option value="0" selected >-</option>
                        @foreach($sh5_settings as $setting)
                            @foreach($corrs as $corr)
                                @if ($setting->id == $corr->setting_id)
                                    <option value="{{$corr->rid}}"
                                        {{(isset($shop->cashless_corr_rid) && $corr->rid === $shop->cashless_corr_rid) ? 'selected' : ''}}
                                    >{{$corr->name}} ({{ $setting->name }})</option>
                                @endif
                            @endforeach
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label for="marriage_corr_rid" class="col-sm-3 col-form-label text-right">Корреспондент брак:</label>
                <div class="col-sm-9">
                    <select name="marriage_corr_rid" class="form-control" id="marriage_corr_rid">
                        <option value="0" selected >-</option>
                        @foreach($sh5_settings as $setting)
                            @foreach($corrs as $corr)
                                @if ($setting->id == $corr->setting_id)
                                    <option value="{{$corr->rid}}"
                                        {{(isset($shop->marriage_corr_rid) && $corr->rid === $shop->marriage_corr_rid) ? 'selected' : ''}}
                                    >{{$corr->name}} ({{ $setting->name }})</option>
                                @endif
                            @endforeach
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label for="brokerage_corr_rid" class="col-sm-3 col-form-label text-right">Корреспондент бракераж:</label>
                <div class="col-sm-9">
                    <select name="brokerage_corr_rid" class="form-control" id="brokerage_corr_rid">
                        <option value="0" selected >-</option>
                        @foreach($sh5_settings as $setting)
                            @foreach($corrs as $corr)
                                @if ($setting->id == $corr->setting_id)
                                    <option value="{{$corr->rid}}"
                                        {{(isset($shop->brokerage_corr_rid) && $corr->rid === $shop->brokerage_corr_rid) ? 'selected' : ''}}
                                    >{{$corr->name}} ({{ $setting->name }})</option>
                                @endif
                            @endforeach
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label for="write_off_corr_rid" class="col-sm-3 col-form-label text-right">Корреспондент списание:</label>
                <div class="col-sm-9">
                    <select name="write_off_corr_rid" class="form-control" id="write_off_corr_rid">
                        <option value="0" selected >-</option>
                        @foreach($sh5_settings as $setting)
                            @foreach($corrs as $corr)
                                @if ($setting->id == $corr->setting_id)
                                    <option value="{{$corr->rid}}"
                                        {{(isset($shop->write_off_corr_rid) && $corr->rid === $shop->write_off_corr_rid) ? 'selected' : ''}}
                                    >{{$corr->name}} ({{ $setting->name }})</option>
                                @endif
                            @endforeach
                        @endforeach
                    </select>
                </div>
            </div>

            <!-- На будущее -->
            <!-- <div class="form-group row">
                <label for="corr_guid_1" class="col-sm-2 col-form-label text-right">Специальный корреспондент №1:</label>
                <div class="col-sm-10">
                    <select name="corr_guid_1" class="form-control" id="corr_guid_1">
                        <option value="0" selected >-</option>
                        @foreach($corrs as $corr)
                        <option value="{{$corr->rid}}" {{(isset($shop->corr_rid_1) && $corr->rid === $shop->corr_rid_1) ? 'selected' : ''}}>{{$corr->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label for="corr_guid_2" class="col-sm-2 col-form-label text-right">Специальный корреспондент №2:</label>
                <div class="col-sm-10">
                    <select name="corr_guid_2" class="form-control" id="corr_guid_2">
                        <option value="0" selected >-</option>
                        @foreach($corrs as $corr)
                        <option value="{{$corr->rid}}" {{(isset($shop->corr_rid_2) && $corr->rid === $shop->corr_rid_2) ? 'selected' : ''}}>{{$corr->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label for="corr_guid_3" class="col-sm-2 col-form-label text-right">Специальный корреспондент №3:</label>
                <div class="col-sm-10">
                    <select name="corr_guid_3" class="form-control" id="corr_guid_3">
                        <option value="0" selected >-</option>
                        @foreach($corrs as $corr)
                        <option value="{{$corr->rid}}" {{(isset($shop->corr_rid_3) && $corr->rid === $shop->corr_rid_3) ? 'selected' : ''}}>{{$corr->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label for="corr_guid_4" class="col-sm-2 col-form-label text-right">Специальный корреспондент №4:</label>
                <div class="col-sm-10">
                    <select name="corr_guid_4" class="form-control" id="corr_guid_4">
                        <option value="0" selected >-</option>
                        @foreach($corrs as $corr)
                        <option value="{{$corr->rid}}" {{(isset($shop->corr_rid_4) && $corr->rid === $shop->corr_rid_4) ? 'selected' : ''}}>{{$corr->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label for="corr_guid_5" class="col-sm-2 col-form-label text-right">Специальный корреспондент №5:</label>
                <div class="col-sm-10">
                    <select name="corr_guid_5" class="form-control" id="corr_guid_5">
                        <option value="0" selected >-</option>
                        @foreach($corrs as $corr)
                        <option value="{{$corr->rid}}" {{(isset($shop->corr_rid_5) && $corr->rid === $shop->corr_rid_5) ? 'selected' : ''}}>{{$corr->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group">
                <h4>Время работы:</h4>
            </div>
            @foreach ($days as $day)
                <div class="form-group row">
                    <label for="time" class="col-sm-3 col-form-label text-right">{{$day}}:</label>
                    <div class="col-sm-9 d-flex align-items-center">
                        <input type="text" class="form-control" name="time" id="time" value="{{old('time', $shop->time ?? '')}}" style="width: 100px" placeholder="09:00">
                        &minus;
                        <input type="text" class="form-control" name="time" id="time" value="{{old('time', $shop->time ?? '')}}" style="width: 100px" placeholder="10:00">
                    </div>
                </div>
            @endforeach
                -->
        @endif
    @endif
@endsection
