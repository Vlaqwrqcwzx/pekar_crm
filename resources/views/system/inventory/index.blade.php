@extends('layouts.form')

@section('title', $inventory->getName())
@section('action', route('system.inventory.update', ['inventory' => $inventory->id]))

@section('list')
    <div class="table-freeze">
        <table class="table">
            <thead>
            <tr>
                <th>Смена</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
           
            </tbody>
        </table>
    </div>
@endsection
