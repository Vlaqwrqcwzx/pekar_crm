@extends('layouts.report')

@section('title', 'Смены')
@section('action', route('system.shift'))

@section('list')
    <div class="table-freeze">
        <table class="table">
            <thead>
            <tr>
                <th>Смена</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @forelse ($shifts as $shift)
                <tr>
                    <td colspan="2">
                        <span class="font-weight-bold">
                            {{$shift->getName()}}
                        </span>
                    </td>
                </tr>
                @forelse ($shift->userShifts as $userShift)
                    <tr>
                        <td class="align-middle">{{$userShift->getName()}}</td>
                        <td>
                            <form class="input-group" action="{{route('system.shift.update')}}" method="post">
                                <input type="text" class="form-control datetimepicker" name="date_start[{{$userShift->id}}]" value="{{$userShift->date_start}}">
                                <input type="text" class="form-control datetimepicker" name="date_end[{{$userShift->id}}]" value="{{$userShift->date_end}}">
                                <div class="input-group-append">
                                    <button class="btn btn-primary" type="submit">Сохранить</button>
                                    <a class="btn btn-danger" href="{{route('system.shift', ['user_shift_id' => $userShift->id])}}" @click.prevent="confirm('Вы уверены?')">Удалить</a>
                                </div>
                            </form>
                        </td>
                    </tr>
                @empty
                @endforelse
            @empty
                <tr>
                    <td>Пусто</td>
                </tr>
            @endforelse
            </tbody>
        </table>
    </div>
@endsection
