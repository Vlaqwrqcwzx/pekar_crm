@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">
                <div class="float-left">
                    <div class="h3">@yield('title')</div>
                </div>
                <div class="float-right">
                    @yield('button')
                </div>
            </div>
            <div class="card-body">
                @if (session()->has('success'))
                    <div class="alert alert-success">
                        {{session('success')}}
                    </div>
                @endif
                @if (session()->has('warning'))
                    <div class="alert alert-danger">
                        {{session('warning')}}
                    </div>
                @endif
                @if ($errors->any())
                    <div class="alert alert-danger">
                        Ошибка
                    </div>
                @endif
                <form action="@yield('action')" method="post" id="form">
                    {{csrf_field()}}
                    @yield('form')
                </form>
            </div>
        </div>
    </div>
@endsection
