@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        @if (session()->has('success'))
            <div class="alert alert-success">
                {{session('success')}}
            </div>
        @endif
        <div class="card">
            <div class="card-header">
                <div class="float-left">
                    <div class="h3">@yield('title')</div>
                </div>
            </div>
            <div class="bg-light p-3">
                <form action="@yield('action')" @submit.prevent="filter">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-row">
                                @if (isset($filter_date))
                                    <div class="col">
                                        <datepicker-component :value="'{{$filter_date}}'" name="filter_date"></datepicker-component>
                                    </div>
                                @else
                                    <div class="col">
                                        <datepicker-component :value="'{{$filter_date_start}}'" name="filter_date_start"></datepicker-component>
                                    </div>
                                    <div class="col">
                                        <datepicker-component :value="'{{$filter_date_end}}'" name="filter_date_end"></datepicker-component>
                                    </div>
                                @endif
                            </div>
                        </div>
                        @if (isset($franchises))
                        <div class="col-sm-2">
                            <select name="filter_franchise" class="form-control">
                                @foreach ($franchises as $franchise)
                                    <option value="{{$franchise->id}}" @if ($franchise->id == $filter_franchise)selected="selected"@endif>{{$franchise->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        @endif
                        @if (isset($shops))
                        <div class="col-sm-2">
                            <select name="filter_shop" class="form-control">
                                <option value="">-</option>
                                @foreach ($shops as $shop)
                                    <option value="{{$shop->id}}" @if ($shop->id == $filter_shop)selected="selected"@endif>{{$shop->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        @endif
                        @if (isset($types))
                        <div class="col-sm-2">
                            <select name="filter_type" class="form-control">
                                <option value="">-</option>
                                @foreach ($types as $id => $type)
                                    <option value="{{$id}}" @if ($id == $filter_type)selected="selected"@endif>{{$type}}</option>
                                @endforeach
                            </select>
                        </div>
                        @endif
                        @if (isset($shifts))
                            <div class="col-sm-2">
                                <select name="filter_shift" class="form-control">
                                    <option value="">-</option>
                                    @foreach ($shifts as $shift)
                                        <option value="{{$shift->id}}" @if ($shift->id == $filter_shift)selected="selected"@endif>
                                            {{sprintf('Смена #%s (%s - %s)', $shift->id, $shift->date_start, $shift->date_end)}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        @endif
                        @if (isset($hours))
                        <div class="col-sm-2">
                            <div class="form-check mt-2">
                                <input class="form-check-input" value="1" name="filter_hidden" type="checkbox" id="filter_hidden" @if ($filter_hidden)checked="checked"@endif>
                                <label class="form-check-label" for="filter_hidden">
                                    Скрывать пустые
                                </label>
                            </div>
                        </div>
                        @endif
                        @if (isset($sorts))
                            <div class="col-sm-2">
                                <select name="filter_sort" class="form-control">
                                    <option value="">-</option>
                                    @foreach ($sorts as $key => $sort)
                                        <option value="{{$key}}" @if ($key == $filter_sort)selected="selected"@endif>
                                            {{$sort}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        @endif
                        <div class="col text-right">
                            @if(isset($settings))
                                <a href="{{$settings}}" class="btn btn-primary">
                                    <i class="fa fa-cog"></i>
                                </a>
                            @endif
                            <button class="btn btn-primary mt-1">Применить</button>
                        </div>
                    </div>
                </form>
            </div>
            @yield('list')
        </div>
    </div>
@endsection
