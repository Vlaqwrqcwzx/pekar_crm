<ul class="navbar-nav mr-auto">
    @auth
        @foreach ($items as $item)
            @if (in_array($item['route'], $groupItems))
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown">
                    {{$item['title']}}
                    <span class="caret"></span>
                </a>
                <div class="dropdown-menu">
                    @foreach ($item['children'] as $child_par)
                        @if(!empty($child_par['children']))
                            <div class="dropdown-header" style="font-size: 14px;">{{$child_par['title']}}</div>
                            @foreach ($child_par['children'] as $child)
                                @if(!empty($child['route']))
                                    @if (in_array($child['route'], $groupItems))
                                        <a class="dropdown-item" href="{{ route($child['route']) }}">{{$child['title']}}</a>
                                    @endif
                                @endif
                            @endforeach
                        <hr style="margin:5px;">
                        @else
                            @if (in_array($child_par['route'], $groupItems))
                                <a class="dropdown-item" href="{{ route($child_par['route']) }}">{{$child_par['title']}}</a>
                            @endif
                        @endif
                    @endforeach
                </div>
            </li>
            @endif
        @endforeach
    @endauth
</ul>
