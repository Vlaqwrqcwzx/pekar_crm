@if ($status)
    <div class="btn-group btn-group-toggle" data-toggle="buttons">
        <label class="btn btn-primary active">
            <input type="radio" name="status" value="1" checked="checked"> Включить
        </label>
        <label class="btn btn-primary">
            <input type="radio" name="status" value="0"> Выключить
        </label>
    </div>
@else
    <div class="btn-group btn-group-toggle" data-toggle="buttons">
        <label class="btn btn-primary">
            <input type="radio" name="status" value="1"> Включить
        </label>
        <label class="btn btn-primary active">
            <input type="radio" name="status" value="0" checked="checked"> Выключить
        </label>
    </div>
@endif