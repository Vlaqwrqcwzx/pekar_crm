@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="card">
            <div class="card-header">
                <div class="float-left">
                    <div class="h3">@yield('title')</div>
                </div>
                <div class="float-right">
                    @yield('button')
                </div>
            </div>
            @yield('filter')
            @if (session()->has('success'))
                <div class="alert alert-success">
                    {{session('success')}}
                </div>
            @endif
            @if (session()->has('warning'))
                <div class="alert alert-danger">
                    {{session('warning')}}
                </div>
            @endif

            <form action="@yield('action')" method="post" id="form">
                {{csrf_field()}}
                {{method_field('DELETE')}}
                @yield('list')
            </form>
        </div>
    </div>
@endsection
