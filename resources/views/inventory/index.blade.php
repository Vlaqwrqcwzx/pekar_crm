@extends('layouts.report')

@section('title', 'Сличительная ведомость')
@section('action', route('inventory.index'))

@section('list')
    <div class="table-freeze">
        <table class="table">
            <thead>
            <tr>
                <th>Пользователь</th>
                <th>Магазин</th>
                <th>Тип</th>
                <th>Код</th>
                <th>Дата</th>
            </tr>
            </thead>
            <tbody>
            @forelse ($inventories as $inventory)
                <tr>
                    <td>
                        <a href="{{route('inventory.view', ['inventory' => $inventory->id])}}">
                            {{$inventory->user ? $inventory->user->name : 'Автоматическая'}}
                        </a>
                    </td>
                    <td>{{$inventory->shop->name}}</td>
                    <td>{{$inventory->isOpen ? 'При открытии' : 'При закрытии'}}</td>
                    <td>{{$inventory->isClose ? $inventory->code : ''}}</td>
                    <td>{{$inventory->created_at}}</td>
                </tr>
            @empty
                <tr>
                    <td colspan="5">Пусто</td>
                </tr>
            @endforelse
            </tbody>
        </table>
    </div>
@endsection
