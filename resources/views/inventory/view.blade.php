@extends('layouts.app')

@section('content')
<div class="container">
    <div class="card">
        <div class="card-header">
            <div class="float-left">
                <div class="h3">{{$inventory->shop->name}} ({{$inventory->created_at->format('Y-m-d H:i')}})</div>
            </div>
            <div class="float-right">
                <a class="btn btn-primary" href="{{route('system.inventory', ['inventory' => $inventory->id])}}"><i class="fa fa-pen"></i></a>
                <a class="btn btn-primary" href="#" onclick="window.print();"><i class="fa fa-print"></i></a>
            </div>
        </div>
        <table class="table">
            <thead>
            <tr>
                <th class="font-weight-normal w-25">Тип:</th>
                <th class="w-25">{{$inventory->isOpen ? 'При открытии' : 'При закрытии'}}</th>
                <th class="font-weight-normal w-25">Магазин:</th>
                <th class="w-25">{{$inventory->shop->name}}</th>
            </tr>
            <tr>
                <th class="font-weight-normal">Дата:</th>
                <th>{{$inventory->created_at->format('Y-m-d H:i')}}</th>
                <th class="font-weight-normal">Составил:</th>
                <th>{{$inventory->user ? $inventory->user->name : 'Автоматическая'}}</th>
            </tr>
            <tr>
                <th colspan="2">Наимнование:</th>
                <th>Количество:</th>
                <th>Сумма:</th>
            </tr>
            </thead>
            <tbody>
            @forelse ($inventory->inventoryProducts as $product)
                <tr>
                    <td colspan="2">{{$product->product->getName()}}</td>
                    <td>{{$product->quantity}}</td>
                    <td>@moneyFormat($product->quantity * $product->product->getPrice())</td>
                </tr>
            @empty
                <tr>
                    <td colspan="3">Пусто</td>
                </tr>
            @endforelse
            <tr>
                <th colspan="2">Итого, шт / кг:</th>
                <th>{{$totals['count']['countable']}} / {{$totals['count']['scalable']}}</th>
                <th>@moneyFormat($totals['price']['countable']) / @moneyFormat($totals['price']['scalable'])</th>
            </tr>
            </tbody>
        </table>
    </div>
</div>
@endsection
