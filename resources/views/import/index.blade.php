@extends('layouts.list')

@section('title', 'Импорт')

@section('button')
    <div class="btn-group" role="group">
        <a href="{{route('import.create')}}" class="btn btn-primary"><i class="fa fa-plus"></i></a>
    </div>
@endsection

@section('list')
    <table class="table">
        <thead>
        <tr>
            <th>Название</th>
            <th>Статус</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @forelse ($imports as $import)
            <tr>
                <td>{{$import->name}}</td>
                <td>{{$import->status ? 'Включен' : 'Выключен'}}</td>
                <td class="text-right">
                    <a class="btn btn-primary btn-sm" href="{{route('import.edit', ['id' => $import->id])}}">
                        <i class="fa fa-pen"></i>
                    </a>
                </td>
            </tr>
        @empty
            <tr>
                <td colspan="3">Пусто</td>
            </tr>
        @endforelse
        </tbody>
    </table>
@endsection
