@extends('layouts.form')

@section('title', $import->name ?? 'Добавление франшизы')
@section('action', $action)

@section('button')
    <div class="btn-group" role="group">
        <a class="btn btn-primary" href="#" onclick="$('#form').submit(); return false;"><i class="fa fa-save"></i></a>
        <a class="btn btn-secondary" href="{{route('import.index')}}"><i class="fa fa-reply"></i></a>
    </div>
@endsection

@section('form')
    @if (isset($import->id))
        {{method_field('PUT')}}
    @endif
    <div class="form-group row">
        <label for="name" class="col-sm-3 col-form-label text-right">Названия:</label>
        <div class="col-sm-9">
            <input type="text" class="form-control {{$errors->has('name') ? 'is-invalid' : ''}}" name="name" id="name" value="{{old('name', $import->name ?? '')}}">
            @if ($errors->has('name'))
                <div class="invalid-feedback">{{$errors->first('name')}}</div>
            @endif
        </div>
    </div>
    <div class="form-group row">
        <label for="token" class="col-sm-3 col-form-label text-right">Token:</label>
        <div class="col-sm-9">
            <input type="text" class="form-control" name="token" id="token" value="{{old('name', $import->token ?? '')}}">
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-3 col-form-label text-right">Статус:</label>
        <div class="col-sm-9">
            @include('layouts.components.form.status', ['status' => old('status', $import->status ?? 1)])
        </div>
    </div>
    @if (isset($import))
        <div class="form-group row">
            <label for="setting_group_id" class="col-sm-3 col-form-label text-right">Группа номенклатуры SH5:</label>
            <div class="col-sm-9">
                <select name="setting_group_id" class="form-control" id="setting_group_id">
                    <option value="0">-</option>
                    @foreach($setting_groups as $setting_group)
                        @foreach ($settings as $setting)
                            @if ($setting_group->setting_id == $setting->id && $import->setting_id == $setting->id)
                                <option value="{{$setting_group->id}}" {{($import->setting_group_id === $setting_group->id) ? 'selected' : ''}}>
                                    {{$setting_group->name}} ({{ $setting->name }})
                                </option>
                            @endif
                        @endforeach
                    @endforeach
                </select>
            </div>
        </div>
    @endif
@endsection
