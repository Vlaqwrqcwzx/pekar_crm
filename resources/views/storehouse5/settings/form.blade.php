@extends('layouts.form')

@section('title', $setting->name ?? 'Добавление настройки')
@section('action', $action)

@section('button')
    <div class="btn-group" role="group">
        <div class="btn-group mr-4" role="group">
            <check-connect-sh5-component></check-connect-sh5-component>
        </div>
        <a class="btn btn-primary" href="#" onclick="$('#form').submit(); return false;"><i class="fa fa-save"></i></a>
        <a class="btn btn-secondary" href="{{route('settings.index')}}"><i class="fa fa-reply"></i></a>
    </div>
@endsection

@section('form')
    @if (isset($setting->id))
        {{method_field('PUT')}}
    @endif
    <div class="form-group row">
        <input type="hidden" name="connect" id="connect" value="">
        <label for="name" class="col-sm-2 col-form-label text-right">Название:</label>
        <div class="col-sm-10">
            <input type="text" class="form-control {{$errors->has('name') ? 'is-invalid' : ''}}" name="name" id="name"
                   value="{{old('name', $setting->name ?? '')}}">
            @if ($errors->has('name'))
                <div class="invalid-feedback">{{$errors->first('name')}}</div>
            @endif
        </div>
    </div>
    <div class="form-group row">
        <label for="ip" class="col-sm-2 col-form-label text-right">ip:</label>
        <div class="col-sm-10">
            <input type="text" class="form-control {{$errors->has('ip') ? 'is-invalid' : ''}}" name="ip" id="ip"
                   value="{{old('ip', $setting->ip ?? '')}}">
            @if ($errors->has('ip'))
                <div class="invalid-feedback">{{$errors->first('ip')}}</div>
            @endif
        </div>
    </div>
    <div class="form-group row">
        <label for="port" class="col-sm-2 col-form-label text-right">port:</label>
        <div class="col-sm-10">
            <input type="text" class="form-control {{$errors->has('port') ? 'is-invalid' : ''}}" name="port" id="port"
                   value="{{old('port', $setting->port ?? '')}}">
            @if ($errors->has('port'))
                <div class="invalid-feedback">{{$errors->first('port')}}</div>
            @endif
        </div>
    </div>
    <div class="form-group row">
        <label for="username" class="col-sm-2 col-form-label text-right">UserName:</label>
        <div class="col-sm-10">
            <input type="text" class="form-control {{$errors->has('username') ? 'is-invalid' : ''}}" name="username" id="username"
                   value="{{old('username', $setting->username ?? '')}}">
            @if ($errors->has('username'))
                <div class="invalid-feedback">{{$errors->first('username')}}</div>
            @endif
        </div>
    </div>
    <div class="form-group row">
        <label for="password" class="col-sm-2 col-form-label text-right">Password:</label>
        <div class="col-sm-10">
            <input type="text" class="form-control {{$errors->has('password') ? 'is-invalid' : ''}}" name="password" id="password"
                   value="{{old('password', $setting->password ?? '')}}">
            @if ($errors->has('password'))
                <div class="invalid-feedback">{{$errors->first('password')}}</div>
            @endif
        </div>
    </div>
    @if (isset($setting->id))
        <div class="form-group row">
            <label class="col-sm-3 col-form-label text-right">Интеграция касс:</label>
            <div class="col-sm-9">
                <div class="list-group">
                    @foreach($imports as $import)
                        @if($import->setting_id == $setting->id || $import->setting_id == 0)
                            <div class="list-group-item">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox"
                                        class="custom-control-input"
                                        id="import-{{$import->id}}"
                                        name="import[]"
                                        value="{{$import->id}}"
                                        {{($setting && $import->setting_id == $setting->id) || in_array($import->id, (array)old('import')) ? 'checked' : ''}}
                                    >
                                    <label class="custom-control-label" for="import-{{$import->id}}">{{$import->name}}</label>
                                </div>
                            </div>
                        @endif
                    @endforeach
                </div>
            </div>
        </div>
    @endif
    @if (isset($setting->id))
        <table class="table">
            <thead>
            <tr>
                <th>#</th>
                <th>Название</th>
                <th>Rid</th>
            </tr>
            </thead>
            <tbody>
            @forelse ($groups as $group)
                @if ($group->setting_id = $setting->id)
                    <tr>
                        <td>{{$group->id}}</td>
                        <td>
                            {{$group->name}}
                            <div class="small text-muted">{{$group->guid}}</div>
                        </td>
                        <td>{{ $group->rid }}</td>
                    </tr>
                @endif
            @empty
                <tr>
                    <td colspan="3">Пусто</td>
                </tr>
            @endforelse
            </tbody>
        </table>    
    @endif
@endsection
