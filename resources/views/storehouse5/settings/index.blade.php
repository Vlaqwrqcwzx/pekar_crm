@extends('layouts.list')

@section('title', 'Настройки StoreHouse5')

@section('button')
    <div class="btn-group" role="group">
        <a href="{{route('settings.create')}}" class="btn btn-primary"><i class="fa fa-plus"></i></a>
    </div>
@endsection

@section('list')
    <table class="table">
        <thead>
        <tr>
            <th width="1">#</th>
            <th>Название найтройки</th>
            <th>ip</th>
            <th>port</th>
            <th>Дата</th>
        </tr>
        </thead>
        <tbody>
        @forelse ($settings as $setting)
            <tr>
                <td>{{ $setting->id }}</td>
                <td>{{ $setting->name }}</td>
                <td>{{ $setting->ip }}</td>
                <td>{{ $setting->port }}</td>
                <td>{{ $setting->updated_at }}</td>
                <td class="text-right">
                    <a class="btn btn-primary btn-sm" href="{{route('settings.edit', ['id' => $setting->id])}}">
                        <i class="fa fa-pen"></i>
                    </a>
                </td>
            </tr>
        @empty
            <tr>
                <td colspan="5">Пусто</td>
            </tr>
        @endforelse
        </tbody>
    </table>
@endsection
