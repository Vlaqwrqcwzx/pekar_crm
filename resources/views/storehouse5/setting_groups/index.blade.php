@extends('layouts.list')

@section('title', 'Поставщики')

@section('button')
    <!-- <div class="btn-group" role="group">
        <a href="{{route('setting_groups.create')}}" class="btn btn-primary"><i class="fa fa-plus"></i></a>
    </div> -->
@endsection

@section('list')
    <table class="table">
        <thead>
        <tr>
            <th>#</th>
            <th>Название</th>
            <th>RID</th>
            <th>GUID</th>
            <th>Сервер</th>
        </tr>
        </thead>
        <tbody>
        @forelse ($groups as $group)
            <tr>
                <td>{{$group->id}}</td>
                <td>
                    {{$group->name}}
                </td>
                <td>{{$group->rid}}</td>
                <td>{{$group->guid}}</td>
                @foreach($settings as $setting)
                    @if ($setting->id === $group->setting_id)
                        <td>{{ $setting->name }}</td>
                    @endif
                @endforeach
            </tr>
        @empty
            <tr>
                <td colspan="3">Пусто</td>
            </tr>
        @endforelse
        </tbody>
    </table>
@endsection
