@extends('layouts.form')

@section('title', $waybill->number ?? 'Редактирование товарной накладной')
@section('action', $action)

@section('button')
    <div class="btn-group" role="group">
        <form action="{{route('waybills.unload')}}" method="post" id="form">
            {{csrf_field()}}
            <input type="hidden" name="id" value="{{$waybill->id}}">
            <input type="hidden" id="double_corr" name="corr_rid" value="{{$waybill->corr_rid}}">
            <input type="hidden" id="double_depart" name="depart_rid" value="{{$waybill->depart_rid}}">
            <input type="submit" class="btn btn-primary mr-4" value="Выгрузить накладную повторно">
        </form>
        <a class="btn btn-primary" href="#" onclick="$('#form').submit(); return false;"><i class="fa fa-save"></i></a>
        <a class="btn btn-secondary" href="{{route('waybills.index')}}"><i class="fa fa-reply"></i></a>
    </div>
@endsection

@section('form')
    @if (isset($group->id))
        {{method_field('PUT')}}
    @endif
    <div class="form-group row">
        <label for="name" class="col-sm-2 col-form-label text-right">Название:</label>
        <div class="col-sm-10">
            <input type="text"
                   class="form-control {{$errors->has('number') ? 'is-invalid' : ''}}"
                   name="name"
                   id="name"
                   disabled
                   value="{{old('number', $waybill->number ?? '')}}">
            @if ($errors->has('number'))
                <div class="invalid-feedback">{{$errors->first('number')}}</div>
            @endif
        </div>
    </div>
    <div class="form-group row">
        <label for="shop_name" class="col-sm-2 col-form-label text-right">Смена:</label>
        <div class="col-sm-10">
            <input type="text"
                   class="form-control {{$errors->has('shop_name') ? 'is-invalid' : ''}}"
                   name="shop_name"
                   id="shop_name"
                   disabled
                   value="{{sprintf('%s (%s - %s)', $waybill->shift_id, $waybill->date_start, $waybill->date_end)}}">
            @if ($errors->has('shop_name'))
                <div class="invalid-feedback">{{$errors->first('shop_name')}}</div>
            @endif
        </div>
    </div>
    <div class="form-group row">
        <label for="corr_guid_cash" class="col-sm-2 col-form-label text-right">Корреспондент / Склад:</label>
        <div class="col-sm-10">
            <select onfocus="double_corr.value = this.value" onblur="double_corr.value = this.value" class="form-control" id="corr_rid">
                <option value="0" selected >-</option>
                @if($waybill->corr_rid < 10000)
                    @foreach($corrs as $corr)
                        @if($corr->setting_id === $waybill->setting_id)
                            <option value="{{$corr->rid}}" {{($waybill->corr_rid === $corr->rid && $waybill->corr_rid != 0) ? 'selected' : ''}}>{{$corr->name}}</option>
                        @endif
                    @endforeach
                @else
                    @foreach($departs as $depart)
                        @if($depart->setting_id === $waybill->setting_id)
                            <option value="{{$depart->rid}}" {{($waybill->corr_rid === $depart->rid) ? 'selected' : ''}}>{{$depart->name}}</option>
                        @endif
                    @endforeach
                @endif
            </select>
        </div>
    </div>
    <div class="form-group row">
        <label for="depart_guid" class="col-sm-2 col-form-label text-right">Склад:</label>
        <div class="col-sm-10">
            <select onfocus="double_depart.value = this.value" onblur="double_depart.value = this.value"  name="depart_rid" class="form-control" id="depart_rid">
                <option value="0" selected >-</option>
                @foreach($departs as $depart)
                    @if($depart->setting_id === $waybill->setting_id)
                        <option value="{{$depart->rid}}" {{($waybill->depart_rid === $depart->rid) ? 'selected' : ''}}>{{$depart->name}}</option>
                    @endif
                @endforeach
            </select>
        </div>
    </div>
    <div class="card-header">
        <div class="float-left">
            <div class="h3">Товары</div>
        </div>
    </div>
    <table class="table">
        <thead>
        <tr>
            <th>#</th>
            <th>Название</th>
            <th>Цена</th>
            <th>Сумма</th>
            <th>Количество / КГ</th>
            <th class="text-right">Редактировать</th>
        </tr>
        </thead>
        <tbody>
        @forelse ($products as $product)
            <tr>
                <td>{{$product->product_id}}</td>
                <td>
                    {{$product->name}}
                    <div class="small text-muted">{{$product->guid}}</div>
                </td>
                <td>@format($product->price)</td>
                <td>{{ $product->amount }} ₽</td>
                <td>{{ number_format($product->quantity, 2) }}</td>
                <td class="text-right">
                    <a class="btn btn-primary btn-sm" href="{{route('product.edit', ['id' => $product->product_id])}}">
                        <i class="fa fa-pen"></i>
                    </a>
                </td>
            </tr>
        @empty
            <tr>
                <td colspan="3">Пусто</td>
            </tr>
        @endforelse
        </tbody>
    </table>
@endsection
