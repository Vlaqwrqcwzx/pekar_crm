@extends('layouts.list')

@section('title', 'Товарные накладные')

@section('button')
    <push-sh5-waybills-component></push-sh5-waybills-component>
@endsection
@section('list')
    <table class="table">
        <thead>
        <tr>
            <th>#</th>
            <th>Номер</th>
            <th>Склад</th>
            <th>Контрагент / Склад</th>
            <th>Дата</th>
            <th>#</th>
        </tr>
        </thead>
        <tbody>
            @forelse ($waybills as $waybill)
                <tr>
                    <td>{{$waybill->id}}</td>
                    <td>
                        {{$waybill->number}}
                        <div class="small text-muted">{{$waybill->guid}}</div>
                    </td>
                    @foreach($departs as $depart)
                        @if($depart->rid === $waybill->depart_rid && $depart->setting_id === $waybill->setting_id)
                            <td>{{ $depart->name }}</td>
                        @endif
                    @endforeach
                    @if($waybill->corr_rid === 0)
                            <td>-</td>
                    @else
                        @foreach($corrs as $corr)
                            @if($corr->rid === $waybill->corr_rid && $corr->setting_id === $waybill->setting_id)
                                <td>{{ $corr->name }}</td>
                            @endif
                        @endforeach
                    @endif
                    @foreach($departs as $depart)
                        @if($depart->rid === $waybill->corr_rid && $depart->setting_id === $waybill->setting_id)
                            <td>{{ $depart->name }}</td>
                        @endif
                    @endforeach
                    <td>{{$waybill->created_at}}</td>
                    <td>
                        <a class="btn btn-primary btn-sm" href="{{route('waybills.edit', ['id' => $waybill->id])}}">
                            <i class="fa fa-pen"></i>
                        </a>
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="3">Пусто</td>
                </tr>
            @endforelse
        </tbody>
    </table>
@endsection
