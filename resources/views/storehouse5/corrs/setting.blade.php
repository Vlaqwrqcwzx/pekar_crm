@extends('layouts.list')

@section('title', 'Корреспонденты')

@section('button')
    <div class="btn-group" role="group">
        <get-sh5-corrs-component class="mr-4"></get-sh5-corrs-component>
        <a class="btn btn-secondary" href="{{route('corrs.index')}}"><i class="fa fa-reply"></i></a>
    </div>
@endsection

@section('list')
    <table class="table">
        <thead>
        <tr>
            <th>#</th>
            <th>Название</th>
            <th>RID</th>
            <th>GUID</th>
            <th>Дата обновления</th>
            <th>#</th>
        </tr>
        </thead>
        <tbody>
        @forelse ($corrs as $corr)
            <tr>
                <td>{{$corr->id}}</td>
                <td>
                    {{$corr->name}}
                </td>
                <td>{{$corr->rid}}</td>
                <td>{{$corr->guid}}</td>
                <td>{{$corr->updated_at}}</td>
            </tr>
        @empty
            <tr>
                <td colspan="3">Пусто</td>
            </tr>
        @endforelse
        </tbody>
    </table>
@endsection

