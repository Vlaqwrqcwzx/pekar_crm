@extends('layouts.list')

@section('title', 'Склады')

@section('button')
    <div class="btn-group" role="group">
        <get-sh5-departs-component class="mr-4"></get-sh5-departs-component>
        <a class="btn btn-secondary" href="{{route('departs.index')}}"><i class="fa fa-reply"></i></a>
    </div>
@endsection

@section('list')
    <table class="table">
        <thead>
        <tr>
            <th>#</th>
            <th>Название</th>
            <th>RID</th>
            <th>GUID</th>
            <th>Дата обновления</th>
        </tr>
        </thead>
        <tbody>
        @forelse ($departs as $depart)
            <tr>
                <td>{{$depart->id}}</td>
                <td>
                    {{$depart->name}}
                </td>
                <td>{{$depart->rid}}</td>
                <td>{{$depart->guid}}</td>
                <td>{{$depart->updated_at}}</td>
            </tr>
        @empty
            <tr>
                <td colspan="3">Пусто</td>
            </tr>
        @endforelse
        </tbody>
    </table>
@endsection
