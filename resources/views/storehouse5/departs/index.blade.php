@extends('layouts.list')

@section('title', 'Склады')

@section('list')
    <div class="table-freeze">
        <table class="table">
            <thead>
            <tr>
                <th>Настройки StoreHouse5</th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @forelse ($settings as $setting)
                <tr>
                    <td><a href="{{route('departs.setting', ['setting' => $setting->id])}}">{{$setting->name}}</a></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td class="text-right">

                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="5">Пусто</td>
                </tr>
            @endforelse
            </tbody>
        </table>
    </div>
@endsection