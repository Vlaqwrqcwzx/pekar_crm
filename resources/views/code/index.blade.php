@extends('layouts.form')

@section('title', 'Коды')
@section('action', route('code.update'))

@section('button')
    <div class="btn-group" role="group">
        <a class="btn btn-primary" href="#" onclick="$('#form').submit(); return false;"><i class="fa fa-save"></i></a>
    </div>
@endsection

@section('form')
    <div class="form-group row">
        <label for="code" class="col-sm-2 col-form-label text-right">Код:</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="code" id="code" value="{{old('code')}}">
        </div>
    </div>
@endsection
