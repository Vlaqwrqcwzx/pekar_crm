@extends('layouts.report')

@section('title', 'TOP 20')
@section('action', route('report.top'))

@section('list')
    <div class="table-freeze">
        <table class="table">
            <thead>
            <tr>
                <th width="1%">#</th>
                <th>Товар</th>
                <th>Категория</th>
                <th>Кол-во</th>
                <th>Сумма</th>
            </tr>
            </thead>
            <tbody>
            @foreach($products as $product)
                <tr style="border-left: 3px solid {{$product['color']}};" class="{{$loop->iteration <= 20 ? 'bg-warning' : ''}}">
                    <td>{{$loop->iteration}}</td>
                    <td>{{$product['name']}}</td>
                    <td>{{$product['category']}}</td>
                    <td>{{$product['count']}}</td>
                    <td>@moneyFormat($product['total'])</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
