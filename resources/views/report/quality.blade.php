@extends('layouts.report')

@section('title', 'Качество работы пекаря')
@section('action', route('report.quality'))

@section('list')
    <div class="table-freeze">
        <table class="table">
            <thead>
            <tr style="font-size: 12px">
                <th class="w-25">Товар</th>
                <th>Время выпуска по плану от</th>
                <th>Время выпуска по плану до</th>
                <th>Попадание в час начала выпуска</th>
                <th>Время начала по плану от</th>
                <th>Время начала по плану до</th>
                <th>Фактическая время начала</th>
                <th>Расхождение время начала</th>
                <th>Минимальное время выпуска</th>
                <th>Максимально время выпуска</th>
                <th>Фактическое время выпуска</th>
                <th>Расхождение время выпуска</th>
                <th>Время готовки</th>
                <th>Фактическое время готовки</th>
                <th>Моментальный выпуск</th>
            </tr>
            </thead>
            <tbody>
            @forelse($products as $product)
                <tr style="border-left: 3px solid {{$product['color']}};">
                    @foreach($product['rows'] as $row)
                    <td>{{$row}}</td>
                    @endforeach
                </tr>
                @if ($loop->last)
                <tr>
                    <td colspan="4"></td>
                    <td colspan="3" class="text-right">Итого:</td>
                    <td>{{$totals['time_start_diff']}}</td>
                    <td colspan="3" class="text-right">Итого:</td>
                    <td>{{$totals['time_complete_diff']}}</td>
                    <td colspan="2" class="text-right">Итого:</td>
                    <td>{{$totals['time_moment']}}</td>
                </tr>
                @endif
            @empty
                <tr>
                    <td colspan="2">Пусто</td>
                </tr>
            @endforelse
            </tbody>
        </table>
    </div>
@endsection
