@extends('layouts.report')

@section('title', 'План/Факт/Продажи')
@section('action', route('report.action'))

@section('list')
    <div class="table-freeze">
        <table class="table">
            <thead>
            <tr>
                <th>Товар</th>
                <th>Остатки</th>
                @foreach (array_keys($hours) as $hour)
                    <th class="fix-width">{{$hour}}</th>
                @endforeach
                <th>Остатки</th>
            </tr>
            </thead>
            <tbody>
            @forelse ($products as $product)
                <tr style="border-left: 3px solid {{$product['color']}};">
                    <td>{{$product['name']}}</td>
                    <td class="fix-width">{{$product['start']}}</td>
                    @foreach ($product['rows'] as $row)
                        <td style="padding: 5px 1px;width: 90px">
                            @if ($row['plan'])
                                <span class="badge badge-warning" title="План">{{$row['plan']}}</span>
                            @else
                                <span class="badge badge-light text-light">{{$row['plan']}}</span>
                            @endif
                            @if ($row['order'])
                            <span class="badge badge-danger" title="Продажи">{{$row['order']}}</span>
                            @else
                            <span class="badge badge-light text-light">{{$row['order']}}</span>
                            @endif
                            @if ($row['accept'])
                                <span class="badge badge-success" title="Факт">{{$row['accept']}}</span>
                            @else
                                <span class="badge badge-light text-light">{{$row['accept']}}</span>
                            @endif
                            <br>
                            @if ($row['reject'])
                            <span class="badge badge-pink" title="Списание">{{$row['reject']}}</span>
                            @else
                            <span class="badge badge-light text-light">{{$row['reject']}}</span>
                            @endif
                            @if ($row['movementOut'])
                            <span class="badge badge-primary" title="Перемещение-">{{$row['movementOut']}}</span>
                            @else
                            <span class="badge badge-light text-light">{{$row['movementOut']}}</span>
                            @endif
                            @if ($row['movementIn'])
                                <span class="badge badge-secondary" title="Перемещение+">{{$row['movementIn']}}</span>
                            @else
                                <span class="badge badge-light text-light">{{$row['movementIn']}}</span>
                            @endif
                            <span class="d-none">
                            @if ($row['inventory'])
                                <span class="badge badge-dark" title="Инвентаризация">{{$row['inventory']}}</span>
                            @else
                                <span class="badge badge-light text-light">{{$row['inventory']}}</span>
                            @endif
                            </span>
                        </td>
                    @endforeach
                    <td class="fix-width">{{$product['end']}}</td>
                </tr>
            @empty
                <tr>
                    <td colspan="{{ 3 + count($hours) }}">Пусто</td>
                </tr>
            @endforelse
            </tbody>
        </table>
    </div>
@endsection
