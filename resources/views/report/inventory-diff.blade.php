@extends('layouts.report')

@section('title', 'Сравнение сличительных ведомостей')
@section('action', route('report.inventory-diff'))

@section('list')
    <div class="table-freeze">
        <table class="table">
            <thead>
            <tr>
                <th rowspan="2" class="align-middle">Наименование</th>
                <th colspan="2">Пред. слич. вед</th>
                <th colspan="2">Слич. вед</th>
                <th colspan="2">Недостача</th>
                <th colspan="2">Излишки</th>
            </tr>
            <tr>
                <th>Кол-во</th>
                <th>Сумма</th>
                <th>Кол-во</th>
                <th>Сумма</th>
                <th>Кол-во</th>
                <th>Сумма</th>
                <th>Кол-во</th>
                <th>Сумма</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($products as $product)
                <tr style="border-left: 3px solid {{$product['color']}};">
                    <td>{{$product['name']}}</td>
                    <td>{{$product['end']['quantity']}}</td>
                    <td>@moneyFormat($product['end']['total'])</td>
                    <td>{{$product['start']['quantity']}}</td>
                    <td>@moneyFormat($product['start']['total'])</td>
                    @if ($product['result']['quantity'] != 0)
                        @if ($product['result']['quantity'] < 0)
                            <td class="text-danger font-weight-bold">{{$product['result']['quantity']}}</td>
                            <td class="text-danger font-weight-bold">@moneyFormat($product['result']['total'])</td>
                            <td></td>
                            <td></td>
                        @else
                            <td></td>
                            <td></td>
                            <td class="text-primary font-weight-bold">{{$product['result']['quantity']}}</td>
                            <td class="text-primary font-weight-bold">@moneyFormat($product['result']['total'])</td>
                        @endif
                    @else
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    @endif
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
