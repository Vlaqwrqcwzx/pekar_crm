@extends('layouts.report')

@section('title', 'Сводка')
@section('action', route('report.shop'))

@section('list')
@if($mobileView)
    <div class="table-freeze-all">
        <table class="table">
            <thead>
            <tr>
                <th rowspan="2">Магазин</th>
                <th rowspan="2">План</th>
                <th rowspan="2">Выпуск</th>
                <th rowspan="2">Принятие</th>
                <th rowspan="2">Перемещения</th>
                <th rowspan="2">Продажи</th>
                <th rowspan="2">Витрина</th>
                <th rowspan="2">SKU</th>
                <th rowspan="2">Минусы на витрине</th>
            </thead>
            <tbody>
            @foreach($reportShops as $shop)
                <tr>
                    <td>{{$shop['name']}}</td>
                    <td>@moneyFormat($shop['shift'])</td>
                    <td>@moneyFormat($shop['complete'])</td>
                    <td>@moneyFormat($shop['accept'])</td>
                    <td>@moneyFormat($shop['movementsMinus'] + $shop['movementsPlus'])</td>
                    <td>@moneyFormat($shop['order'])</td>
                    <td>@moneyFormat($shop['quantity'])</td>
                    <td>{{$shop['sku']}}</td>
                    <td>{{$shop['quantityMinus']}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <table class="table">
            <thead>
            <tr>
                <th rowspan="2">Магазин</th>
                <th colspan="3" class="text-center">Качество</th>
                <th rowspan="2">Скидка</th>
                <th rowspan="2">Списание</th>
                <th rowspan="2">Излишки</th>
                <th rowspan="2">Недостача</th>
            </tr>
            <tr>
                <th>Последний<br>выпуск</th>
                <th>Отклонения<br>от выпуска</th>
                <th>Моментальные<br>выпуски</th>
            </tr>
            </thead>
            <tbody>
            @foreach($reportShops as $shop)
                <tr>
                    <td>{{$shop['name']}}</td>
                    <td>{{$shop['lastComplete']}}</td>
                    <td>{{$shop['timeComplete']}}</td>
                    <td>{{$shop['timeMoment']}}</td>
                    <td>@moneyFormat($shop['discount'])</td>
                    <td>@moneyFormat($shop['reject'])</td>
                    <td>@moneyFormat($shop['plus'])</td>
                    <td>@moneyFormat($shop['minus'])</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@else
    <div class="table-freeze-all">
        <table class="table">
            <thead>
            <tr>
                <th rowspan="2">Магазин</th>
                <th rowspan="2">Продажи</th>
                <th rowspan="2">План</th>
                <th rowspan="2">Витрина</th>
                <th rowspan="2">SKU</th>
                <th rowspan="2">Выпуск</th>
                <th rowspan="2">Принятие</th>
                <th colspan="3" class="text-center">Качество</th>
                <th rowspan="2">Скидка</th>
                <th rowspan="2">Списание</th>
                <th rowspan="2">Перемещения</th>
                <th rowspan="2">Минусы на витрине</th>
                <th rowspan="2">Излишки</th>
                <th rowspan="2">Недостача</th>
            </tr>
            <tr>
                <th>Последний<br>выпуск</th>
                <th>Отклонения<br>от выпуска</th>
                <th>Моментальные<br>выпуски</th>
            </tr>
            </thead>
            <tbody>
            @foreach($reportShops as $shop)
                <tr>
                    <td>{{$shop['name']}}</td>
                    <td>@moneyFormat($shop['order'])</td>
                    <td>@moneyFormat($shop['shift'])</td>
                    <td>@moneyFormat($shop['quantity'])</td>
                    <td>{{$shop['sku']}}</td>
                    <td>@moneyFormat($shop['complete'])</td>
                    <td>@moneyFormat($shop['accept'])</td>
                    <td>{{$shop['lastComplete']}}</td>
                    <td>{{$shop['timeComplete']}}</td>
                    <td>{{$shop['timeMoment']}}</td>
                    <td>@moneyFormat($shop['discount'])</td>
                    <td>@moneyFormat($shop['reject'])</td>
                    <td>@moneyFormat($shop['movementsMinus'] + $shop['movementsPlus'])</td>
                    <td>{{$shop['quantityMinus']}}</td>
                    <td>@moneyFormat($shop['plus'])</td>
                    <td>@moneyFormat($shop['minus'])</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endif
@endsection
