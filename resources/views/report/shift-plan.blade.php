@extends('layouts.report')

@section('title', 'План/Факт')
@section('action', route('report.shift-plan'))

@section('list')
    <div class="table-freeze">
        <table class="table">
            <thead>
            <tr>
                <th>Товар</th>
                <th>Время</th>
                <th>Итого</th>
                @foreach (array_keys($hours) as $hour)
                    <th class="fix-width">{{$hour}}</th>
                @endforeach
            </tr>
            <tr>
                <th class="font-weight-bold">Итого, шт</th>
                <th class="font-weight-bold"></th>
                <th class="font-weight-bold"></th>
                @foreach ($counts as $count)
                    <th class="font-weight-bold fix-width">{{$count}}</th>
                @endforeach
            </tr>
            <tr>
                <th class="font-weight-bold">Итого, т.р.</th>
                <th class="font-weight-bold"></th>
                <th class="font-weight-bold"></th>
                @foreach ($totals as $total)
                    <th class="font-weight-bold fix-width">{{$total / 1000}}</th>
                @endforeach
            </tr>
            </thead>
            <tbody>
            @forelse ($products as $product)
                <tr style="border-left: 3px solid <?= $product["category_color"] ?>;">
                    <td>{{$product['name']}}</td>
                    <td class="fix-width">{{$product['time']}}</td>
                    <td class="fix-width">{{$product['total']}}</td>
                    @foreach ($product['rows'] as $row)
                        <td class="fix-width {{($row['plan'] || $row['shift']) ? '' : 'null' }}">
                            @if (($row['plan'] || $row['shift']))
                                <span class="badge badge-warning">{{$row['plan']}}</span>
                                <div>&minus;</div>
                                <span class="badge badge-primary">{{$row['shift']}}</span>
                            @else
                                <span>{{$row['plan']}}</span>
                                <div>&minus;</div>
                                <span>{{$row['shift']}}</span>
                            @endif
                        </td>
                    @endforeach
                </tr>
            @empty
                <tr>
                    <td colspan="{{ 3 + count($totals) }}">Пусто</td>
                </tr>
            @endforelse
            </tbody>
        </table>
    </div>
@endsection
