@extends('layouts.report')

@section('title', 'Контроль списаний')
@section('action', route('report.control'))

@section('list')
    <div class="table-freeze">
        <table class="table table-striped">
            <thead>
            <tr>
                <th rowspan="2" class="align-middle">Наименование</th>
                <th colspan="2">Остаток</th>
                <th colspan="2">Продажи</th>
                <th colspan="2">Списание общее</th>
                <th colspan="2">Списание 12:00 - 14:00</th>
                <th colspan="2">Списание прочее</th>
                <th colspan="2">Излишки</th>
                <th colspan="2">Недостачи</th>
            </tr>
            <tr>
                <th>Кол-во</th>
                <th>Сумма</th>
                <th>Кол-во</th>
                <th>Сумма</th>
                <th>Кол-во</th>
                <th>Сумма</th>
                <th>Кол-во</th>
                <th>Сумма</th>
                <th>Кол-во</th>
                <th>Сумма</th>
                <th>Кол-во</th>
                <th>Сумма</th>
                <th>Кол-во</th>
                <th>Сумма</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($products as $product)
                <tr style="border-left: 3px solid {{$product['color']}};">
                    <td>{{$product['name']}}</td>
                    <td>{{$product['start']['quantity']}}</td>
                    <td>{{$product['start']['total']}}</td>
                    <td>{{$product['order']['quantity']}}</td>
                    <td>{{$product['order']['total']}}</td>
                    <td>{{$product['reject']['all']['quantity']}}</td>
                    <td>{{$product['reject']['all']['total']}}</td>
                    <td>{{$product['reject']['time']['quantity']}}</td>
                    <td>{{$product['reject']['time']['total']}}</td>
                    <td>{{$product['reject']['other']['quantity']}}</td>
                    <td>{{$product['reject']['other']['total']}}</td>
                    @if ($product['result']['quantity'] != 0)
                        @if ($product['result']['quantity'] > 0)
                            <td class="text-danger font-weight-bold">{{(float)abs($product['result']['quantity'])}}</td>
                            <td class="text-danger font-weight-bold">{{(float)abs($product['result']['total'])}}</td>
                            <td></td>
                            <td></td>
                        @else
                            <td></td>
                            <td></td>
                            <td class="text-primary font-weight-bold">{{(float)abs($product['result']['quantity'])}}</td>
                            <td class="text-primary font-weight-bold">{{(float)abs($product['result']['total'])}}</td>
                        @endif
                    @else
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    @endif
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
