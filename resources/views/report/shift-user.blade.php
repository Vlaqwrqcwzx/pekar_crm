@extends('layouts.report')

@section('title', 'Смены')
@section('action', route('report.shift-user'))

@section('list')
    <div class="table-freeze">
        <table class="table">
            <thead>
            <tr>
                <th>Имя</th>
                <th>Группа</th>
                <th>Код</th>
                <th>Магазин</th>
                <th>Смена</th>
                <th>Дата начала</th>
                <th>Дата конца</th>
            </tr>
            </thead>
            <tbody>
            @forelse ($user_shifts as $user_shift)
                <tr>
                    <td>{{$user_shift->user->name}}</td>
                    <td>{{$user_shift->user->group->name}}</td>
                    <td>{{$user_shift->user->code}}</td>
                    <td>{{$user_shift->shop->name}}</td>
                    <td>{{$user_shift->shift ? $user_shift->shift->getName() : 'Смены нет'}}</td>
                    <td>{{$user_shift->date_start->format('d.m.Y H:i:s')}}</td>
                    <td>{{$user_shift->date_end ? $user_shift->date_end->format('d.m.Y H:i:s') : '-'}}</td>
                </tr>
            @empty
                <tr>
                    <td colspan="6">Пусто</td>
                </tr>
            @endforelse
            </tbody>
        </table>
    </div>
@endsection
