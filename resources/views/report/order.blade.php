@extends('layouts.report')

@section('title', 'Продажи')
@section('action', route('report.order'))

@section('list')
    <div class="table-freeze">
        <table class="table">
            <thead>
            <tr>
                <th>Товар</th>
                <th>Время</th>
                <th>Итого, шт</th>
                @foreach (array_keys($hours) as $hour)
                    <th class="fix-width">{{$hour}}</th>
                @endforeach
            </tr>
            <tr>
                <th class="font-weight-bold">Итого, шт</th>
                <th class="font-weight-bold"></th>
                <th class="font-weight-bold"></th>
                @foreach ($hourCounts as $count)
                    <th class="font-weight-bold fix-width">{{$count['countable']}}</th>
                @endforeach
            </tr>
            <tr>
                <th class="font-weight-bold">Итого, кг</th>
                <th class="font-weight-bold"></th>
                <th class="font-weight-bold"></th>
                @foreach ($hourCounts as $count)
                    <th class="font-weight-bold fix-width">{{$count['scalable']}}</th>
                @endforeach
            </tr>
            <tr>
                <th class="font-weight-bold">Итого, т.р.</th>
                <th class="font-weight-bold"></th>
                <th class="font-weight-bold"></th>
                @foreach ($hourTotals as $total)
                    <th class="font-weight-bold fix-width">{{$total}}</th>
                @endforeach
            </tr>
            </thead>
            <tbody>
            @forelse ($products as $product)
                <tr style="border-left: 3px solid <?= $product["category_color"] ?>;">
                    <td>{{$product['name']}}</td>
                    <td class="fix-width">{{$product['time']}}</td>
                    <td class="fix-width">{{$product['count']}}</td>
                    @foreach ($product['orders'] as $order)
                        <td class="fix-width {{$order ? '' : 'null' }}">{{$order}}</td>
                    @endforeach
                </tr>
            @empty
                <tr>
                    <td colspan="{{ 3 + count($hourTotals) }}">Пусто</td>
                </tr>
            @endforelse
            </tbody>
        </table>
    </div>
@endsection
