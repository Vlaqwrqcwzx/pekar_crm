@extends('layouts.report')

@section('title', 'Сличительная ведомость')
@section('action', route('report.inventory'))

@section('list')
    <div class="table-freeze">
        <table class="table">
            <thead>
            <tr>
                <th rowspan="2" class="align-middle">Наименование</th>
                <th colspan="2">Остаток</th>
                <th colspan="2">Принято</th>
                <th colspan="2">Продажи</th>
                <th colspan="2">Перемещение -</th>
                <th colspan="2">Перемещение +</th>
                <th colspan="2">Брак</th>
                <th colspan="2">Бракераж</th>
                <th colspan="2">Списание</th>
                <th colspan="2">Слич. вед</th>
                <th colspan="2">Недостача</th>
                <th colspan="2">Излишки</th>
            </tr>
            <tr>
                <th>Кол-во</th>
                <th>Сумма</th>
                <th>Кол-во</th>
                <th>Сумма</th>
                <th>Кол-во</th>
                <th>Сумма</th>
                <th>Кол-во</th>
                <th>Сумма</th>
                <th>Кол-во</th>
                <th>Сумма</th>
                <th>Кол-во</th>
                <th>Сумма</th>
                <th>Кол-во</th>
                <th>Сумма</th>
                <th>Кол-во</th>
                <th>Сумма</th>
                <th>Кол-во</th>
                <th>Сумма</th>
                <th>Кол-во</th>
                <th>Сумма</th>
                <th>Кол-во</th>
                <th>Сумма</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($products as $product)
                <tr style="border-left: 3px solid {{$product['color']}};">
                    <td>{{$product['name']}}</td>
                    <td>{{$product['start']['quantity']}}</td>
                    <td>{{$product['start']['total']}}</td>
                    <td>{{$product['shift']['quantity']}}</td>
                    <td>{{$product['shift']['total']}}</td>
                    <td>{{$product['order']['quantity']}}</td>
                    <td>{{$product['order']['total']}}</td>
                    <td>{{$product['movement_out']['quantity']}}</td>
                    <td>{{$product['movement_out']['total']}}</td>
                    <td>{{$product['movement_in']['quantity']}}</td>
                    <td>{{$product['movement_in']['total']}}</td>
                    <td>{{$product['reject-1']['quantity']}}</td>
                    <td>{{$product['reject-1']['total']}}</td>
                    <td>{{$product['reject-2']['quantity']}}</td>
                    <td>{{$product['reject-2']['total']}}</td>
                    <td>{{$product['reject-3']['quantity']}}</td>
                    <td>{{$product['reject-3']['total']}}</td>
                    <td>{{$product['end']['quantity']}}</td>
                    <td>{{$product['end']['total']}}</td>
                    @if ($product['result']['quantity'] != 0 && $product['end']['quantity'] != '-')
                        @if ($product['result']['quantity'] < 0)
                            <td class="text-danger font-weight-bold">{{(float)$product['result']['quantity']}}</td>
                            <td class="text-danger font-weight-bold">@moneyFormat($product['result']['total'])</td>
                            <td></td>
                            <td></td>
                        @else
                            <td></td>
                            <td></td>
                            <td class="text-primary font-weight-bold">{{(float)$product['result']['quantity']}}</td>
                            <td class="text-primary font-weight-bold">@moneyFormat($product['result']['total'])</td>
                        @endif
                    @else
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    @endif
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
