@extends('layouts.report')

@section('title', 'Остатки по часам')
@section('action', route('report.quantity'))

@section('list')
    <div class="table-freeze">
        <table class="table">
            <thead>
            <tr>
                <th>Товар</th>
                <th class="text-center">Вчер. остатки</th>
                @foreach (array_keys($hours) as $hour)
                    <th class="fix-width text-center">{{$hour}}</th>
                @endforeach
                <th class="text-center">Расч. остатки</th>
            </tr>
            </thead>
            <tbody>
            @forelse ($products as $product)
                <tr style="border-left: 3px solid {{$product['color']}};">
                    <td class="w-25">{{$product['name']}}</td>
                    <td class="fix-width text-center">{{$product['start']}}</td>
                    @foreach ($product['rows'] as $row)
                        <td class="fix-width text-center {{$row > 0 ? 'bg-success text-white' : ''}}{{$row < 0 ? 'bg-danger text-white' : ''}}">
                            {{$row}}
                        </td>
                    @endforeach
                    <td class="fix-width text-center">{{$product['end']}}</td>
                </tr>
            @empty
                <tr>
                    <td colspan="{{ 3 + count($hours) }}">Пусто</td>
                </tr>
            @endforelse
            </tbody>
        </table>
    </div>
@endsection
