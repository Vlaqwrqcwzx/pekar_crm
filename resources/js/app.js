/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

Vue.component('datepicker-component', require('./components/DatepickerComponent').default);
Vue.component('input-keyboard-component', require('./components/InputKeyboardComponent').default);
Vue.component('plan-component', require('./components/PlanComponent').default);
Vue.component('shift-component', require('./components/ShiftComponent').default);
Vue.component('complete-product-component', require('./components/CompleteProductComponent').default);
Vue.component('shift-products-component', require('./components/ShiftProductsComponent').default);
Vue.component('reject-component', require('./components/RejectComponent').default);
Vue.component('products-component', require('./components/ProductsComponent').default);
Vue.component('stock-component', require('./components/StockComponent').default);
Vue.component('movement-component', require('./components/MovementComponent').default);
Vue.component('inventory-component', require('./components/InventoryComponent').default);
Vue.component('accept-component', require('./components/AcceptComponent').default);
Vue.component('get-sh5-corrs-component', require('./components/GetSh5CorrsComponent').default);
Vue.component('get-sh5-departs-component', require('./components/GetSh5DepartsComponent').default);
Vue.component('push-sh5-products-component', require('./components/PushSh5ProductsComponent').default);
Vue.component('check-connect-sh5-component', require('./components/CheckConnectSh5Component').default);

// Keyboard
const app = new Vue({
    el: '#app',
    methods: {
        filter: function (event) {
            params = {};
            url = $(event.target).attr('action');

            $(event.target).find('input, select').each(function () {
                if ($(this).is(':checkbox')) {
                    if ($(this).is(':checked')) {
                        params[$(this).attr('name')] = 1;
                    } else {
                        params[$(this).attr('name')] = 0;
                    }
                } else if ($(this).val()) {
                    params[$(this).attr('name')] = $(this).val();
                }
            });

            params = $.param(params);

            if (params) {
                location.href = $(event.target).attr('action') + '?' + params;
            }
        },
        redirect: function (href) {
            location.href = href;
        },
        confirm: function (message) {
            if (confirm(message)) {
                if ($(event.target).attr('href')) {
                    location.href = $(event.target).attr('href');
                }

                if ($(event.target).attr('form')) {
                    $('#' + $(event.target).attr('form')).submit();
                }
            }
        },
        formatPrice: function (value) {
            let val = (value / 1).toFixed(2).replace('.', ',')
            return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ' ')
        },
        formatCount: function (value) {
            return value.toFixed(3);
        }
    }
});

Vue.directive('focus', {
    // Когда привязанный элемент вставлен в DOM...
    inserted: function (el) {
        // Переключаем фокус на элемент
        el.focus()
    }
})

// jQuery
$(function() {
    $('.table-freeze').freezeTable({
        freezeColumn: false
    });

    $('.table-freeze-all').freezeTable();

    $('[data-toggle="tooltip"]').tooltip();

    jQuery('.datetimepicker').datetimepicker({
        format: 'Y-m-d H:i:s'
    });
    jQuery.datetimepicker.setLocale('ru');
});
