<?php

return [
    [
        'title'    => 'Отчеты',
        'route'    => 'report',
        'children' => [
            [
                'title'    => 'Продажи',
                'route'    => 'report.order',
                'children' => [],
            ],
            [
                'title'    => 'Выпуски',
                'route'    => 'report.shift',
                'children' => [],
            ],
            [
                'title'    => 'Списание',
                'route'    => 'report.reject',
                'children' => [],
            ],
            [
                'title'    => 'Качество работы',
                'route'    => 'report.quality',
                'children' => [],
            ],
            [
                'title'    => 'Смены',
                'route'    => 'report.shift-user',
                'children' => [],
            ],
            [
                'title'    => 'Сличительная ведомость (Касса)',
                'route'    => 'report.inventory',
                'children' => [],
            ],
            [
                'title'    => 'Сличительная ведомость (Кухня)',
                'route'    => 'report.inventory-kitchen',
                'children' => [],
            ],
            [
                'title'    => 'Сравнение сличительных ведомостей',
                'route'    => 'report.inventory-diff',
                'children' => [],
            ],
            [
                'title'    => 'План / Факт',
                'route'    => 'report.shift-plan',
                'children' => [],
            ],
            [
                'title'    => 'План / Факт / Продажи',
                'route'    => 'report.action',
                'children' => [],
            ],
            [
                'title'    => 'Сводка',
                'route'    => 'report.shop',
                'children' => [],
            ],
            [
                'title'    => 'TOP 20',
                'route'    => 'report.top',
                'children' => [],
            ],
            [
                'title'    => 'Остатки по часам',
                'route'    => 'report.quantity',
                'children' => [],
            ],
            [
                'title'    => 'Контроль списаний',
                'route'    => 'report.control',
                'children' => [],
            ]
        ]
    ],
    [
        'title'    => 'Система',
        'route'    => 'system',
        'children' => [
            [
                'title'    => 'План выпуска',
                'route'    => 'plan.index',
                'children' => [],
            ],
            [
                'title'    => 'Коды',
                'route'    => 'code.index',
                'children' => [],
            ],
            [
                'title'    => 'Смены',
                'route'    => 'system.shift',
                'children' => [],
            ]
        ]
    ],
    [
        'title'    => 'Документы',
        'route'    => 'doc',
        'children' => [
            [
                'title'    => 'Брак / Бракераж / Списание',
                'route'    => 'reject.index',
                'children' => [],
            ],
            [
                'title'    => 'Сличительная ведомость',
                'route'    => 'inventory.index',
                'children' => [],
            ],
            [
                'title'    => 'Перемещения',
                'route'    => 'movement.index',
                'children' => [],
            ]
        ]
    ],
    [
        'title'    => 'Справочник',
        'route'    => 'product.index',
        'children' => [
            [
                'title'    => 'Франшизы',
                'route'    => 'franchise.index',
                'children' => [],
            ],
            [
                'title'    => 'Магазины',
                'route'    => 'shop.index',
                'children' => [],
            ],
            [
                'title'    => 'Кассы',
                'route'    => 'device.index',
                'children' => [],
            ],
            [
                'title'    => 'Импорт',
                'route'    => 'import.index',
                'children' => [],
            ],
            [
                'title'    => 'Категории',
                'route'    => 'category.index',
                'children' => [],
            ],
            [
                'title'    => 'Товары',
                'route'    => 'product.index',
                'children' => [],
            ],
            [
                'title'    => 'Пользователи',
                'route'    => 'user.index',
                'children' => [],
            ],
            [
                'title'    => 'Группы пользователей',
                'route'    => 'user_group.index',
                'children' => [],
            ]
        ]
    ],
    [
        'title'    => 'StoreHouse5',
        'route'    => 'corrs.index',
        'children' => [
            [
                'title'    => 'Контрагенты',
                'route'    => 'corrs.index',
                'children' => [
                    [
                        'title'    => 'Корреспонденты',
                        'route'    => 'corrs.index',
                        'children' => [],
                    ],
                    [
                        'title'    => 'Склады',
                        'route'    => 'departs.index',
                        'children' => [],
                    ]
                ],

            ],
            [
                'title'    => 'Накладные',
                'route'    => 'waybills.index',
                'children' => [],
            ],
            [
                'title' => 'Настройки',
                'route' => 'settings.index',
                'children' => [
                    // [
                    //     'title' => 'Сервера',
                    //     'route' => 'settings.index',
                    //     'children' => [],
                    // ],
                    // [
                    //     'title' => 'Группы номенклатур',
                    //     'route' => 'setting_groups.index',
                    //     'children' => [],
                    // ]
                ],
            ]
        ]
    ],
    [
        'title'    => 'Настройки',
        'route'    => 'setting',
        'children' => [
            [
                'title'    => 'Настроки',
                'route'    => 'setting.index',
                'children' => [],
            ]
        ]
    ],

];
