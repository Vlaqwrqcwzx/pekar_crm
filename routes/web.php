<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');

Auth::routes([
    'register' => false,
    'reset'    => false,
    'verify'   => false
]);

Route::group(['middleware' => ['auth']], function() {
    // Product
    Route::get('product/json', 'ProductController@json');
    Route::get('product/import-{import}', 'ProductController@import')->name('product.import');
    Route::resource('product', 'ProductController');

    // Franchise
    Route::resource('franchise', 'FranchiseController');

    // Import
    Route::resource('import', 'ImportController');

    // Shop
    Route::get('shop', 'ShopController@index')->name('shop.index');
    Route::get('shop/json', 'ShopController@json')->name('product.json');
    Route::resource('shop', 'ShopController');

    // Device
    Route::get('device', 'DeviceController@index')->name('device.index');
    Route::resource('device', 'DeviceController');



    Route::group(['middleware' => ['checkReport']], function() {
        // Report
        Route::get('report/order', 'ReportController@order')->name('report.order');
        Route::get('report/shift', 'ReportController@shift')->name('report.shift');
        Route::get('report/shift-user', 'ReportController@shiftUser')->name('report.shift-user');
        Route::get('report/shift-plan', 'ReportController@shiftPlan')->name('report.shift-plan');
        Route::get('report/reject', 'ReportController@reject')->name('report.reject');
        Route::get('report/quality', 'ReportController@quality')->name('report.quality');
        Route::get('report/inventory', 'ReportController@inventory')->name('report.inventory');
        Route::get('report/inventory-kitchen', 'ReportController@inventoryKitchen')->name('report.inventory-kitchen');
        Route::get('report/inventory-diff', 'ReportController@inventoryDiff')->name('report.inventory-diff');
        Route::get('report/action', 'ReportController@action')->name('report.action');
        Route::get('report/shop', 'ReportController@shop')->name('report.shop');
        Route::get('report/top', 'ReportController@top')->name('report.top');
        Route::get('report/quantity', 'ReportController@quantity')->name('report.quantity');
        Route::get('report/control', 'ReportController@control')->name('report.control');
    });

    // Category
    Route::get('category/import-{import}', 'CategoryController@import')->name('category.import');
    Route::resource('category', 'CategoryController');

    // User
    Route::get('user/report-settings/{report}', 'UserController@reportSettings')->name('user.reportSettings');
    Route::post('user/save-report-settings', 'UserController@saveReportSettings')->name('user.saveReportSettings');
    Route::resource('user', 'UserController');
    Route::resource('user_group', 'UserGroupController');

    // Plan
    Route::get('plan', 'PlanController@index')->name('plan.index');
    Route::get('plan/shop-{shop}', 'PlanController@shop')->name('plan.shop');
    Route::get('plan/plan-{plan}', 'PlanController@plan')->name('plan.plan');
    Route::post('plan/store', 'PlanController@store')->name('plan.store');
    Route::put('plan/update-plan-days/{shop_id}', 'PlanController@updatePlanDays')->name('plan.updatePlanDays');
    Route::put('plan/update-plan-products/{plan}', 'PlanController@updatePlanProducts')->name('plan.updatePlanProducts');
    Route::post('plan/update/{plan}', 'PlanController@update')->name('plan.update');
    Route::get('plan/copy/{plan}', 'PlanController@copy')->name('plan.copy');
    Route::delete('plan/delete/{plan}', 'PlanController@delete')->name('plan.delete');
    Route::get('plan-api/get-{plan}', 'PlanController@getPlan');

    // Shift
    Route::get('shift', 'ShiftController@index')->name('shift.index');
    Route::get('shift/shop-{shop}', 'ShiftController@shop')->name('shift.shop')->middleware('can:view,shop');
    Route::get('shift/open-{shop}', 'ShiftController@open')->name('shift.open')->middleware(['can:view,shop', 'can:open,shop']);
    Route::get('shift/close-{shift}', 'ShiftController@close')->name('shift.close')->middleware(['can:close,shift']);
    Route::get('shift/enter-{shop}', 'ShiftController@enter')->name('shift.enter')->middleware(['can:view,shop', 'can:enter,shop']);
    Route::get('shift/exit-{shop}', 'ShiftController@exit')->name('shift.exit')->middleware(['can:view,shop', 'can:enter,shop']);
    Route::get('shift/view-{shift}', 'ShiftController@view')->name('shift.view')->middleware('can:view,shift');
    Route::get('shift/inventory-{shift}', 'ShiftController@inventory')->name('shift-inventory.view')->middleware(['can:view,shift', 'can:inventory,shift']);

    // StoreHouse5
    Route::resource('waybills', 'ViewWaybillsController');
    Route::post('waybills/unload/', 'ViewWaybillsController@unload')->name('waybills.unload');
    Route::get('corrs/setting-{setting}', 'ViewCorrController@setting')->name('corrs.setting');
    Route::resource('corrs', 'ViewCorrController');
    Route::get('departs/setting-{setting}', 'ViewDepartController@setting')->name('departs.setting');
    Route::resource('departs', 'ViewDepartController');
    Route::resource('settings', 'ViewSettingsController');
    Route::get('settings/connect/{setting_id}', 'ViewSettingsController@connect')->name('settings.connect');
    Route::resource('setting_groups', 'ViewSettingGroupController');

    // Api
    Route::group([
        'middleware' => 'can:view,shift',
        'namespace'  => 'Api'
    ], function () {
        // Shift
        Route::get('shift-api/get-{shift}', 'ShiftController@getShift');
        Route::put('shift-api/set-status-{shift}', 'ShiftController@setStatus');
        Route::put('shift-api/set-quantity-{shift}', 'ShiftController@setQuantity');
        Route::put('shift-api/add-product-{shift}', 'ShiftController@addProduct');
        Route::put('shift-api/accept-product-{shift}', 'ShiftController@acceptProduct');
        Route::put('shift-api/add-wrong-product-{shift}', 'ShiftController@addWrongProduct');
        Route::put('shift-api/remove-wrong-product-{shift}', 'ShiftController@removeWrongProduct');

        // Movement
        Route::put('movement-api/add-{shift}', 'MovementController@addMovement');
        Route::put('movement-api/accept-{shift}', 'MovementController@acceptMovement');

        // Reject
        Route::get('reject-api/get-{shift}', 'RejectController@getRejects');
        Route::put('reject-api/add-{shift}', 'RejectController@addReject');

        // Inventory
        Route::get('inventory-api/get-{shift}', 'InventoryController@getInventory');
        Route::put('inventory-api/add-{shift}', 'InventoryController@addInventory');
    });

    // System
    Route::group([
        'namespace'  => 'System'
    ], function () {
        // Shift
        Route::get('system/shift', 'ShiftController@index')->name('system.shift');
        Route::post('system/shift/update', 'ShiftController@update')->name('system.shift.update');

        // Inventory
        Route::get('system/inventory/view-{inventory}', 'InventoryController@index')->name('system.inventory');
        Route::post('system/inventory/update', 'ShiftController@update')->name('system.inventory.update');
    });

    // StoreHouse5
    Route::group([
        'middleware' => ['auth'],
        'namespace'  => 'Api'
    ], function () {
         Route::post('sh5/get-corrs', 'StoreHouse5\\ApiCorrController@apiCorrs');
         Route::post('sh5/get-departs', 'StoreHouse5\\ApiDepartController@apiDeparts');
         Route::post('sh5/post-products', 'StoreHouse5\\ApiProductController@apiProduct');
         Route::get('sh5/get-products-group', 'StoreHouse5\\ApiProductGroupController@getGroups');
         Route::post('sh5/check-connect', 'StoreHouse5\\ApiCheckConnectController@checkConnect');
    });

    // Code
    Route::get('code', 'CodeController@index')->name('code.index');
    Route::post('code/update', 'CodeController@update')->name('code.update');

    // Reject
    Route::get('reject', 'RejectController@index')->name('reject.index');
    Route::get('reject/view-{reject}', 'RejectController@view')->name('reject.view');

    // Inventory
    Route::get('inventory', 'InventoryController@index')->name('inventory.index');
    Route::get('inventory/view-{inventory}', 'InventoryController@view')->name('inventory.view');

    // Movement
    Route::get('movement', 'MovementController@index')->name('movement.index');
    Route::get('movement/view-{movement}', 'MovementController@view')->name('movement.view');

    // Setting
    Route::get('setting', 'SettingController@index')->name('setting.index');
    Route::post('setting/update', 'SettingController@update')->name('setting.update');
});

Route::group(['namespace' => 'Cron'], function () {
    Route::get('shift-cron', 'ShiftController@index');
    Route::get('qty-cron', 'QuantityController@index');
    Route::get('import-cron', 'ImportController@index');
});
